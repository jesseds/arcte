/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "modelexportdialog.h"
#include "ui_modelexportdialog.h"
#include <QFileDialog>
#include <io.h>
#include <QMessageBox>
#include <ciso646>
#include <QProgressDialog>

ModelExportDialog::ModelExportDialog(QWidget *parent, Document* doc) :
    QDialog(parent), doc{doc},
    ui(new Ui::ModelExportDialog) {
    ui->setupUi(this);
    ui->atom_polys_edit->setValue(20);
    ui->bond_polys_edit->setValue(15);
}

ModelExportDialog::~ModelExportDialog() {
delete ui;
}

void ModelExportDialog::on_union_radio_clicked() {
    bool checked = ui->union_radio->isChecked();
    ui->separate_mesh_radio->setChecked(! checked);
    ui->plainTextEdit->setPlainText("3D printable meshes may take a while to make with large numbers of atoms or bonds. Time may be reduced by reducing the number of polygons of atoms and/or bonds.");
    ui->polys_widget->setVisible(true);
}

void ModelExportDialog::on_separate_mesh_radio_clicked() {
    bool checked = ui->separate_mesh_radio->isChecked();
    ui->union_radio->setChecked(! checked);
    ui->plainTextEdit->setPlainText("Each bond/atom/object has its own mesh, suitable for manipulation in other 3D software.");
    ui->polys_widget->setVisible(false);
}


void ModelExportDialog::on_file_btn_clicked() {
    QString filepath = QFileDialog::getSaveFileName(this, "Export path", "", "Stereo Lithgraphy (*.stl);;");
    ui->file_edit->setText(QFileInfo{filepath}.absoluteFilePath());
}

QFileInfo ModelExportDialog::filepath() {
    return QFileInfo(ui->file_edit->text());
}

void ModelExportDialog::accept() {
    QFileInfo filepath = QFileInfo(ui->file_edit->text());
    if (doc->basisPositions().size() == 0) {
        return;
    }

    if (filepath.absoluteFilePath() == "") {
        return;
    }

    if (not filepath.absoluteDir().exists()) {
        QMessageBox::critical(this, "Invalid file path", "File destination is not valid or does not exist.");
        return;
    }

    if (ui->union_radio->isChecked()) {
        kind = "union";
    }
    else if (ui->separate_mesh_radio->isChecked()) {
        kind = "separate";
    }
    QDialog::accept();
}

void ModelExportDialog::on_atom_polys_edit_valueChanged(int val) {
    doc->setAtomPolygons(val);
}

void ModelExportDialog::on_bond_polys_edit_valueChanged(int val) {
    doc->setBondPolygons(val);
}
