/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "vieworientationdialog.h"
#include "ui_vieworientationdialog.h"
#include <QPushButton>
#include <ciso646>

ViewOrientationDialog::ViewOrientationDialog(QWidget *parent, SceneWidget* scene) :
    QDialog(parent), ui(new Ui::ViewOrientationDialog){
    ui->setupUi(this);
    this->scene = scene;
    connect(ui->buttonBox, &QDialogButtonBox::clicked, this, &ViewOrientationDialog::btnBoxClicked);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &ViewOrientationDialog::reject);
}

ViewOrientationDialog::~ViewOrientationDialog(){
    delete ui;
}

void ViewOrientationDialog::btnBoxClicked(QAbstractButton* btn) {
    if (btn != ui->buttonBox->button(QDialogButtonBox::Apply)) {
        return;
    }
    double x = ui->x_edit->value();
    double y = ui->y_edit->value();
    double z = ui->z_edit->value();
    if (x == 0 and y == 0 and z == 0) {
        return;
    }
    scene->setUvw(x, y, z);
}

void ViewOrientationDialog::reject() {
    hide();
    QDialog::reject();
}
