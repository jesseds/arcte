/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <QDialog>
#include <document.h>

namespace Ui {
class CalculatorDialog;
}

class CalculatorDialog : public QDialog {
    Q_OBJECT

public:
    explicit CalculatorDialog(QWidget *parent, Document const*);
    ~CalculatorDialog();

    void showWithDocumentParams(Document const* doc);

private slots:
    void on_copy_from_doc_btn_clicked();
    void on_angle_spacing_lbl_clicked();
    void on_angle_freq_lbl_clicked();
    void on_wl_spacing_lbl_clicked();
    void on_wl_freq_lbl_clicked();

private:
    Ui::CalculatorDialog *ui;
    void reset();

    Document const* doc;
    void compute();
    double u1, u2, v1, v2, w1, w2;
    double h1, k1, l1, h2, k2, l2;
    int n;
    double dhkl, ghkl, angle, wl;
    double mass, volts, gamma;

protected:
//    void keyPressEvent(QKeyEvent *) override;
};

