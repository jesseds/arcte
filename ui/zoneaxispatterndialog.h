/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <QMainWindow>
#include <document.h>
#include <zoneaxisdiff.h>
#include <scatteringfactors.h>
#include <zoneaxisplotter.h>
#include <qtabulate.h>
#include <QLabel>
#include <FramelessHelper/Widgets/standardtitlebar.h>

using namespace wangwenx190;

namespace Ui {
class ZoneAxisPatternDialog;
}

class ZoneAxisPatternDialog : public QMainWindow
{
    Q_OBJECT

private:
    Ui::ZoneAxisPatternDialog *ui;
    Document const* doc;
    QMenuBar* titlebar_widget;
    FramelessHelper::StandardTitleBar* m_titleBar;
    std::unique_ptr<ediff::ZoneAxisPattern> ediff_sim;
    std::map<std::string, sfs::Param> sfs_map {};
//    double _last_camera_length = 0;
//    int _n_ticks = 100;
    sfs::QScatteringFactorsElectronCombo* _sfs_select;
    ediff::QModelCombo* _model_select;

    void setSfParam(sfs::Param value);
    ZoneAxisPlotter* plotter;
    QTabulate* table;

    QLabel* _hover_label;


public:
    explicit ZoneAxisPatternDialog(QWidget *parent, Document const* doc);
    ~ZoneAxisPatternDialog();

public slots:
    void show();
    void setHoverStatus(QString const&);

private slots:
    void on_calculate_btn_clicked();
    void onCalculationFinished();
    void onSimZoneAxisChanged(int h, int k, int l);
    void on_magn_slider_valueChanged(double cl);
};

