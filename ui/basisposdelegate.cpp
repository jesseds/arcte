/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "basisposdelegate.h"
#include <qlineedit.h>
#include <basiseditor.h>


bool doubles_are_close(double num1, double num2){
    bool close = std::abs(num2 - num1) < 1e-6;
    return close;
}


DoubleDelegate::DoubleDelegate(double min, double max) :
    QStyledItemDelegate() {
    this->min = min;
    this->max = max;
}


QWidget* DoubleDelegate::createEditor(QWidget* parent, QStyleOptionViewItem const& option, QModelIndex const& index) const {
    QLineEdit* widg = new QLineEdit(parent);
    widg->setClearButtonEnabled(true);
    widg->setValidator(new QDoubleValidator(min, max, 4, widg));
    return widg;
}


void DoubleDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const {
    QString value = QString::number(index.data(Qt::EditRole).toDouble());
    QLineEdit* edit = dynamic_cast<QLineEdit*>(editor);
    edit->setText(value);
}

void DoubleDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const {
    QLineEdit* edit = dynamic_cast<QLineEdit*>(editor);
    double newval = edit->text().toDouble();
    double oldval = index.data(Qt::EditRole).toDouble();

    if (doubles_are_close(oldval, newval)){
    }
    else {
        model->setData(index, newval, Qt::EditRole);
    }
}
