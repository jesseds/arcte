/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <spacegroupdialog.h>
#include "ui_spacegroupdialog.h"
#include <map>
#include <string>


SpaceGroupDialog::SpaceGroupDialog(QWidget* parent, gemmi::SpaceGroup const* init_spg):
    QDialog(parent),
    ui(new Ui::SpaceGroupDialog){
    ui->setupUi(this);
    setModal(true);
    centering_map = {{'F', "Face"},
                     {'A', "Base"},
                     {'B', "Base"},
                     {'C', "Base"},
                     {'P', "Primitive"},
                     {'I', "Body"},
                     {'R', "Rhombohedral"}};

    createModel();
    this->init_spg = init_spg;

    ui->spg_widget->header()->resizeSection(0, 160);
    ui->spg_widget->header()->resizeSection(2, 75);
    ui->spg_widget->header()->resizeSection(3, 100);
    ui->spg_widget->header()->resizeSection(4, 100);
    ui->spg_widget->header()->resizeSection(7, 100);
    ui->set_widget->setFocus();
}


SpaceGroupDialog::~SpaceGroupDialog(){
    delete ui;
}


gemmi::SpaceGroup const* SpaceGroupDialog::currentlySelectedSpaceGroup(){
    QTreeWidgetItemIterator it(ui->set_widget);
    while (*it){
        if ((*it)->isSelected()){
            std::string xhm = (*it)->data(0, Qt::UserRole).toString().toStdString();
            return gemmi::find_spacegroup_by_name(xhm);
        }
            ++it;
    }
    return nullptr;
}


gemmi::SpaceGroup const* SpaceGroupDialog::getSpaceGroup(QWidget* parent, gemmi::SpaceGroup const* curr_spg){
    SpaceGroupDialog* dialog = new SpaceGroupDialog(parent, curr_spg);
    dialog->setSelectedSpaceGroup(curr_spg);
    if (dialog->exec()) {
        return dialog->currentlySelectedSpaceGroup();
    }
    else {
        return nullptr;
    }
}


void SpaceGroupDialog::createModel(){
    std::map<std::string, QTreeWidgetItem*> data;
    data["triclinic"] = new QTreeWidgetItem();
    data["triclinic"]->setText(0, "Triclinic");
    data["monoclinic"] = new QTreeWidgetItem();
    data["monoclinic"]->setText(0, "Monoclinic");
    data["orthorhombic"] = new QTreeWidgetItem();
    data["orthorhombic"]->setText(0, "Orthorhombic");
    data["trigonal"] = new QTreeWidgetItem();
    data["trigonal"]->setText(0, "Trigonal");
    data["hexagonal"] = new QTreeWidgetItem();
    data["hexagonal"]->setText(0, "Hexagonal");
    data["cubic"] = new QTreeWidgetItem();
    data["cubic"]->setText(0, "Cubic");
    data["tetragonal"] = new QTreeWidgetItem();
    data["tetragonal"]->setText(0, "Tetragonal");

    for (int i=1; i<=230; ++i){
        auto  spg = gemmi::find_spacegroup_by_number(i);
        QTreeWidgetItem* item = new QTreeWidgetItem();
        std::string system = spg->crystal_system_str();
        system[0] = toupper(system[0]);
        item->setText(0, QString::fromStdString(system));
        auto hm = spg->hm;
        item->setText(1, QString::fromStdString(centering_map[hm[0]]));
        item->setText(2, QString::fromStdString(std::to_string(spg->number)));
        item->setText(3, QString::fromStdString(hm));
        item->setText(4, QString::fromStdString(spg->short_name()));
        item->setText(5, QString::fromStdString(spg->point_group_hm()));
        item->setText(6, QString::fromStdString(spg->laue_str()));
        item->setText(7, QString::fromStdString(spg->hall));
        int nops = spg->operations().sym_ops.size() * spg->operations().cen_ops.size();
        item->setText(8, QString::fromStdString(std::to_string(nops)));
        std::string centric = spg->operations().is_centric() ? "Yes" : "No";
        item->setText(9, QString::fromStdString(centric));

        // We use the user role to retrieve the space group data later
        item->setData(0, Qt::UserRole, hm);
        data[spg->crystal_system_str()]->addChild(item);
    }

    ui->spg_widget->addTopLevelItem(data["triclinic"]);
    ui->spg_widget->addTopLevelItem(data["monoclinic"]);
    ui->spg_widget->addTopLevelItem(data["orthorhombic"]);
    ui->spg_widget->addTopLevelItem(data["tetragonal"]);
    ui->spg_widget->addTopLevelItem(data["trigonal"]);
    ui->spg_widget->addTopLevelItem(data["hexagonal"]);
    ui->spg_widget->addTopLevelItem(data["cubic"]);
}


void SpaceGroupDialog::populateSettings(const gemmi::SpaceGroup* spg){
    ui->set_widget->clear();

    int i=1;
    for (gemmi::SpaceGroup const& spg_item: gemmi::spacegroup_tables::main){
        if (spg_item.number == spg->number){

            QTreeWidgetItem* item = new QTreeWidgetItem();
            item->setText(0, QString::fromStdString(std::to_string(i)));
            item->setText(1, QString::fromStdString(spg_item.xhm()));
            item->setData(0, Qt::UserRole, QString::fromStdString(spg_item.xhm()));
            ui->set_widget->addTopLevelItem(item);
            ++i;
        }
    }
}


void SpaceGroupDialog::on_spg_widget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
    std::string hm = current->data(0, Qt::UserRole).toString().toStdString();
    auto spg = gemmi::find_spacegroup_by_name(hm);

    // Do nothing if a section/organization item was clicked
    if (current->childCount() > 0){
        ui->set_widget->clear();
        return;
    }

    // Anything causing gemmi to not find a space group should never happen
    if (spg == nullptr){
        throw std::invalid_argument("null space group not allowed");
    }
    populateSettings(spg);
    selectFirstSetting();
}

void SpaceGroupDialog::selectFirstSetting(){
    if (ui->set_widget->children().size() == 0){
        return;
    }
    ui->set_widget->topLevelItem(0)->setSelected(true);
}


void SpaceGroupDialog::setSelectedSpaceGroup(gemmi::SpaceGroup const* spg){
    auto const selected_num = spg->number;
    auto const selected_xhm = spg->xhm();
    ui->spg_widget->clearSelection();

    // First select the space group
    QTreeWidgetItemIterator it(ui->spg_widget);
    while (*it){
        // Skip if item is an organizational section item
        if ((*it)->childCount() != 0){
            ++it;
            continue;
        }

        std::string current_hm = (*it)->data(0, Qt::UserRole).toString().toStdString();
        gemmi::SpaceGroup const* curr_spg = gemmi::find_spacegroup_by_name(current_hm);

        if (selected_num == curr_spg->number){
            (*it)->parent()->setExpanded(true);
            (*it)->setSelected(true);
            ui->spg_widget->scrollToItem(*it);
            break;
        }
        ++it;
    }
    populateSettings(spg);

    // Now select the space group setting
    QTreeWidgetItemIterator set_it(ui->set_widget);
    while (*set_it){
        std::string xhm = (*set_it)->data(0, Qt::UserRole).toString().toStdString();
        if (spg == gemmi::find_spacegroup_by_name(xhm)){
            (*set_it)->setSelected(true);
            return;
        }
        ++set_it;
    }
}


void SpaceGroupDialog::on_buttonBox_clicked(QAbstractButton *button){
    if (QDialogButtonBox::Reset == ui->buttonBox->standardButton(button)){
        setSelectedSpaceGroup(init_spg);
    }
}
