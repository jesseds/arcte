/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "basisnamedelegate.h"
#include <ciso646>
#include <iostream>
#include <elements.h>
#include <QColor>
#include <QPainter>

BasisNameDelegate::BasisNameDelegate() : QStyledItemDelegate() {

}

void BasisNameDelegate::paint(QPainter* painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    QStyledItemDelegate::paint(painter, option, index);

    if (index.column() == 0 and index.parent() == QModelIndex()) {
        QString curr_elem = index.data(Qt::DisplayRole).toString();
        chem::Element const* elem_item = chem::table.findByName(curr_elem.toStdString());
        QStyleOptionViewItem opts = option;
        QStyledItemDelegate::initStyleOption(&opts, index);
        painter->save();

        QFont font{};
        font.setBold(true);
        QFontMetrics font_mtr {font};
        QRect txt_width {font_mtr.boundingRect(curr_elem)};

        int padding = static_cast<int>(opts.rect.height()*0.35);

        QRect circ_rect {};
        circ_rect.setLeft(opts.rect.left() + txt_width.right() + 15);
        circ_rect.setTop(opts.rect.top() + padding/2);
        circ_rect.setHeight(opts.rect.height() - padding);
        circ_rect.setWidth(circ_rect.height());


        painter->setFont(font);
//        painter->drawText(opts.rect, curr_elem);

        QBrush fill {};
        auto rgb = elem_item->color();
        fill.setColor(QColor::fromRgbF(rgb[0], rgb[1], rgb[2]));
        fill.setStyle(Qt::BrushStyle::SolidPattern);
        painter->setBrush(fill);
        painter->setRenderHint(QPainter::Antialiasing);
        painter->drawEllipse(circ_rect);
        painter->restore();
    }
}
