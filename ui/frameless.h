#pragma once
#include <QMainWindow>
#include <QMenuBar>
#include <FramelessHelper/Widgets/framelessmainwindow.h>
#include <FramelessHelper/Widgets/framelesswidgetshelper.h>
#include <FramelessHelper/Widgets/standardtitlebar.h>
#include <framelesswidgetshelper.h>
#include <FramelessHelper/Widgets/standardsystembutton.h>
#include <FramelessHelper/Core/utils.h>

using namespace wangwenx190;

std::optional<FramelessHelper::StandardTitleBar*> setupTitleBar(QWidget* parent, QWidget* menubar, bool blend_backgrouind = false);
