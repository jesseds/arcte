/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "aboutdialog.h"
#include "ui_aboutdialog.h"
#include <QFile>
#include <QResource>
#include <QTextStream>
#include <sstream>
#include <vtkVersion.h>
#include <gemmi/version.hpp>
#include <Eigen/Core>
#include <frameless.h>

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog) {
    ui->setupUi(this);

    auto menu = new QMenuBar(this);
    auto titlebar = setupTitleBar(this, menu, true);
    if (titlebar.has_value()) {
        this->layout()->setMenuBar(titlebar.value());
    }

    QResource file_resource {":/LICENSE"};
    auto license = new QFile(file_resource.absoluteFilePath());
    license->open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream stream {license};
    ui->license_text->setText(stream.readAll());
    ui->license_text->setReadOnly(true);

    QTextBrowser* about = ui->about_text;
    about->setOpenLinks(true);
    about->setOpenExternalLinks(true);
    about->setReadOnly(true);
    about->setFrameStyle(QFrame::NoFrame);
    std::string ss {};

    about->append(QString::fromStdString("<p></p>"));

    about->setAlignment(Qt::AlignCenter);
    ss = "<b><p><font size=14 color=#434343>Arcte v" + QApplication::applicationVersion().toStdString() + "</font></p></b>";
    about->append(QString::fromStdString(ss));

    about->setAlignment(Qt::AlignCenter);
    ss = "<p>Copyright Jesse Smith</p>";
    about->append(QString::fromStdString(ss));

    ss = "<p><a href=\"https://gitlab.com/jesseds/arcte\">https://gitlab.com/jesseds/arcte</a></p>";
    about->setAlignment(Qt::AlignCenter);
    about->append(QString::fromStdString(ss));

    about->append(QString::fromStdString("<p></p>"));

    ss = "<p><b>Runtime library versions:</b></p>";
    about->setAlignment(Qt::AlignCenter);
    about->append(QString::fromStdString(ss));

    auto qvers = QString(qVersion());
    ss = "<p>Qt Version: " + qvers.toStdString() + "</p>";
    about->setAlignment(Qt::AlignCenter);
    about->append(QString::fromStdString(ss));

    auto vtkvers = std::string(vtkVersion::GetVTKVersion());
    ss = "<p>VTK Version: " + vtkvers + "</p>";
    about->setAlignment(Qt::AlignCenter);
    about->append(QString::fromStdString(ss));

    ss = "<p>Gemmi Version: " + std::string(GEMMI_VERSION) + "</p>";
    about->setAlignment(Qt::AlignCenter);
    about->append(QString::fromStdString(ss));

    ss = "<p>Eigen Version: " + std::to_string(EIGEN_MAJOR_VERSION) + "." + std::to_string(EIGEN_MINOR_VERSION) + "</p>";
    about->setAlignment(Qt::AlignCenter);
    about->append(QString::fromStdString(ss));

    about->append(QString::fromStdString("<p></p>"));
    about->append(QString::fromStdString("<p><b>Partially supported by National Science Foundation No. 0846444</b></p>"));
    about->append(QString::fromStdString("<p></p>"));

    ss = "<p>Arcte comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; see license for details.</p>";
    about->setAlignment(Qt::AlignCenter);
    about->append(QString::fromStdString(ss));
//    resize(about->size().width()*0.8, about->size().height()*1.1);
    resize(500, 610);

}

AboutDialog::~AboutDialog() {
    delete ui;
}
