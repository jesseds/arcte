/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "structurefactorsdialog.h"
#include "ui_structurefactorsdialog.h"
#include <geometry.h>
#include <scatteringfactors.h>
#include <helpers.h>
#include <qtabulate.h>
#include <ciso646>
#include <QMessageBox>

StructureFactorsDialog::StructureFactorsDialog(QWidget *parent, Document const* doc) :
    QMainWindow(parent),
    ui(new Ui::StructureFactorsDialog),
    doc(doc) {
    ui->setupUi(this);

    // Keep window on top like a dialog
    setWindowFlag(Qt::Dialog, true);
    ui->dockWidget->setTitleBarWidget(new QWidget());

    table = new QTreeWidget(this);
    auto tabulate = new QTabulate(this, table);
    table->setSortingEnabled(true);
    table->setRootIsDecorated(false);

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(tabulate);
    ui->centralwidget->setLayout(layout);
    headers = QStringList {
        "h",
        "k",
        "l",
        "Real Fg",
        "Imag Fg",
        "Norm",
        "g (1/Å)",
        "d (Å)"
    };
    table->setAlternatingRowColors(true);
    table->setHeaderLabels(headers);
    table->setIndentation(0);
    table->resizeColumnToContents(0);
    table->resizeColumnToContents(1);
    table->resizeColumnToContents(2);

    table->sortByColumn(6, Qt::AscendingOrder);

    sfactor_selection = new sfs::QScatteringFactorsCombo(this);
    sfactor_selection->setParam(sfs::Param::Xray);
    model_layout = new QFormLayout();
    ui->model_group->setLayout(model_layout);
    model_layout->addRow(QString("Scattering factors:"), sfactor_selection);

    // Make sure extents widgets are initialized
    ui->link_h->toggle();
    ui->link_h->setChecked(true);
    ui->link_k->toggle();
    ui->link_k->setChecked(true);
    ui->link_l->toggle();
    ui->link_l->setChecked(true);

    addToolBar(tabulate->toolbar);

}

void StructureFactorsDialog::show() {
    calculate();
    QMainWindow::show();
}


void StructureFactorsDialog::on_calculate_btn_clicked() {
    auto factors = sfs::factorsFromEnum(sfactor_selection->currentParam());
    for (auto const& basis: doc->basisPositions()) {
        if (not factors->exists(basis->species())) {
            QMessageBox::information(this, "Elemental species not available", "Scattering factors are not available for one or more elements.");
            table->clear();
            return;
        }
    }

    calculate();
}


void StructureFactorsDialog::calculate() {
    table->clear();

    int h_max = ui->max_h->value();
    int k_max = ui->max_k->value();
    int l_max = ui->max_l->value();
    int h_min = ui->min_h->value();
    int k_min = ui->min_k->value();
    int l_min = ui->min_l->value();
    double volts = ui->elec_voltage->value();

    MatrixXi hkls  {(h_max - h_min + 1) * (k_max - k_min + 1) * (l_max - l_min + 1), 3};

    int count = 0;
    for (int h=h_min; h<=h_max; ++h) {
        for (int k=k_min; k<=k_max; ++k) {
            for (int l=l_min; l<=l_max; ++l) {
                hkls(count, 0) = h;
                hkls(count, 1) = k;
                hkls(count, 2) = l;
                ++count;
            }
        }
    }

    Matrix3d mtensor = geom::metricTensor(doc->params(), doc->angles());

    VectorXd g = (hkls.cast<double>() * mtensor.inverse() * hkls.cast<double>().transpose()).diagonal().cwiseSqrt();

    auto param = sfactor_selection->currentParam();
    VectorXd norms {};


    // The parameters to actually show
    VectorXcd struct_params {};
    std::string struct_label_real {};
    std::string struct_label_imag {};

    // Xrays selected
    if (sfactor_selection->currentParam() == sfs::Param::Xray) {
        struct_params = geom::structureFactors(*doc, hkls, g, param);
//        norms = (struct_params.array() * struct_params.array().conjugate()).abs().real();
        norms = struct_params.array().rowwise().norm();
        struct_label_real = "Real Fg (e-)";
        struct_label_imag = "Imag Fg (e-)";
    }
    // Electrons selected
    else {
        double corr = 1;

        double vol = geom::volume(doc->params(), doc->angles());

        if (ui->rel_corr_check->isChecked()) {
            corr = geom::relElectronGamma(volts);
        }

        VectorXcd Fhkl = geom::structureFactors(*doc, hkls, g, param, corr);
        norms = Fhkl.array().rowwise().norm();

        std::vector<int> filter {};
        for (int i=0; i<Fhkl.rows(); ++ i) {
            if (not geom::isZero(norms(i))) {
                filter.push_back(i);
            }
        }

        help::rowwiseFilter(Fhkl, filter);
        help::rowwiseFilter(hkls, filter);
        help::rowwiseFilter(g, filter);
        help::rowwiseFilter(norms, filter);

        if (ui->elec_param_combo->currentText() == "Fg") {
            struct_params = Fhkl;
            struct_label_real = "Real Fg (Å)";
            struct_label_imag = "Imag Fg (Å)";
        }
        if (ui->elec_param_combo->currentText() == "Vg") {
            struct_params = geom::crystalPotential(Fhkl, vol);
            struct_label_real = "Real Vg (V)";
            struct_label_imag = "Imag Vg (V)";
        }
        if (ui->elec_param_combo->currentText() == "Ug") {
            struct_params = geom::opticalPotential(Fhkl, vol);
            struct_label_real = "Real Ug (1/Å²)";
            struct_label_imag = "Imag Ug (1/Å²)";
        }
        if (ui->elec_param_combo->currentText() == "ξg") {
            struct_params = geom::extinctionDistance(Fhkl, vol, geom::relElectronWl(volts));
            struct_label_real = "Real ξg (Å)";
            struct_label_imag = "Imag ξg (Å)";
        }
    }

//     The chosen parameter might have changed, recalculate the amplitudes
//    amplitude = (struct_params.array() * struct_params.array().conjugate()).abs().real();
    norms = struct_params.array().rowwise().norm();

    headers[3] = QString::fromStdString(struct_label_real);
    headers[4] = QString::fromStdString(struct_label_imag);
    table->setHeaderLabels(headers);


//    VectorXd struct_norm = struct_params.rowwise().norm();

    for (int i=0; i<hkls.rows(); ++i) {
        if (geom::isZero(norms(i))) {
            continue;
        }

        auto row  = new QTreeWidgetItem {};
        row->setData(0, Qt::DisplayRole, hkls(i, 0));
        row->setData(1, Qt::DisplayRole, hkls(i, 1));
        row->setData(2, Qt::DisplayRole, hkls(i, 2));

        std::stringstream struct_real {};
        if (geom::isZero(struct_params(i).real())) {
            struct_real << 0;
        }
        else {
            struct_real << std::setprecision(5) << std::fixed << struct_params(i).real();
        }

        std::stringstream struct_imag {};
        if (geom::isZero(struct_params(i).imag())) {
            struct_imag << 0;
        }
        else {
            struct_imag << std::setprecision(5) << std::fixed << struct_params(i).imag();
        }

        row->setData(3, Qt::DisplayRole, QString::fromStdString(struct_real.str()));
        row->setData(4, Qt::DisplayRole, QString::fromStdString(struct_imag.str()));
        row->setData(5, Qt::DisplayRole, QString::number(norms(i), 'f', 4));
        row->setData(5, Qt::UserRole, norms(i));

        Vector3i zero {};
        zero << 0, 0, 0;

        if (hkls.row(i).isApprox(zero.transpose())) {
            row->setData(6, Qt::DisplayRole, "");
            row->setData(7, Qt::DisplayRole, "");
        }
        else {
            row->setData(6, Qt::DisplayRole, g(i));
            row->setData(7, Qt::DisplayRole, 1/g(i));
        }

        table->addTopLevelItem(row);
    }


}


StructureFactorsDialog::~StructureFactorsDialog() {
    delete ui;
}


void StructureFactorsDialog::on_max_h_valueChanged(int val) {
    if (ui->link_h->isChecked()) {
        ui->min_h->setValue(-val);
    }
}


void StructureFactorsDialog::on_max_k_valueChanged(int val) {
    if (ui->link_k->isChecked()) {
        ui->min_k->setValue(-val);
    }
}


void StructureFactorsDialog::on_max_l_valueChanged(int val) {
    if (ui->link_l->isChecked()) {
        ui->min_l->setValue(-val);
    }
}


void StructureFactorsDialog::on_link_h_toggled(bool checked) {
    if (checked) {
        ui->min_h->setValue(-ui->max_h->value());
        ui->min_h->setEnabled(false);
    }
    else {
        ui->min_h->setEnabled(true);
    }
}


void StructureFactorsDialog::on_link_k_toggled(bool checked) {
    if (checked) {
        ui->min_k->setValue(-ui->max_k->value());
        ui->min_k->setEnabled(false);
    }
    else {
        ui->min_k->setEnabled(true);
    }
}

void StructureFactorsDialog::on_link_l_toggled(bool checked) {
    if (checked) {
        ui->min_l->setValue(-ui->max_l->value());
        ui->min_l->setEnabled(false);
    }
    else {
        ui->min_l->setEnabled(true);
    }
}
