#include <frameless.h>
#include <FramelessHelper/Widgets/framelessmainwindow.h>
#include <FramelessHelper/Widgets/framelesswidgetshelper.h>
#include <FramelessHelper/Widgets/standardtitlebar.h>
#include <framelesswidgetshelper.h>
#include <FramelessHelper/Widgets/standardsystembutton.h>
#include <FramelessHelper/Core/utils.h>
#include <QHBoxLayout>
#include <iostream>


using namespace wangwenx190;

std::optional<FramelessHelper::StandardTitleBar*> setupTitleBar(QWidget* parent, QWidget* menubar, bool blend_background) {
    auto retn = std::optional<FramelessHelper::StandardTitleBar*> {};
    std::cout << "Runn" << "\n";
    #ifdef WIN32

        auto fw_helper = FramelessHelper::FramelessWidgetsHelper::get(parent);
        fw_helper->extendsContentIntoTitleBar(true);
        auto m_titleBar = new FramelessHelper::StandardTitleBar(parent);
        m_titleBar->setWindowIconVisible(true);

        auto palette = parent->palette();
        QColor background {};
        if (blend_background == true) {
            background = palette.color(QPalette::Window);
        }
        else {
            background = QColor(245, 245, 245);
        }
        m_titleBar->chromePalette()->setTitleBarActiveBackgroundColor(background);
        m_titleBar->chromePalette()->setTitleBarInactiveBackgroundColor(background);

        m_titleBar->setTitleLabelAlignment(Qt::AlignCenter);
        fw_helper->setSystemButton(m_titleBar->closeButton(), FramelessHelper::Global::SystemButtonType::Close);
        fw_helper->setSystemButton(m_titleBar->minimizeButton(), FramelessHelper::Global::SystemButtonType::Minimize);
        fw_helper->setSystemButton(m_titleBar->maximizeButton(), FramelessHelper::Global::SystemButtonType::Maximize);

        auto spacer = new QWidget(parent);
        spacer->setFixedWidth(35);

        const auto titleBarLayout = static_cast<QHBoxLayout *>(m_titleBar->layout());
        titleBarLayout->insertWidget(0, menubar);
        titleBarLayout->insertWidget(0, spacer);
        menubar->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);

//        auto mainwindow = dynamic_cast<QMainWindow*>(parent);
//        if (mainwindow) {
//            mainwindow->setMenuWidget(m_titleBar);
//        }

        fw_helper->setTitleBarWidget(m_titleBar);

        fw_helper->setHitTestVisible(menubar);
        fw_helper->setHitTestVisible(spacer);
        retn = std::optional<FramelessHelper::StandardTitleBar*>(m_titleBar);
    #endif
        return retn;
}
