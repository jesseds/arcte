/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <QDialog>
#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <gemmi/symmetry.hpp>
#include <QStandardItemModel>
#include <map>
#include <QTreeWidgetItem>

namespace Ui {
class SpaceGroupDialog;
}

class SpaceGroupDialog : public QDialog {
    Q_OBJECT

public:
    explicit SpaceGroupDialog(QWidget* parent, gemmi::SpaceGroup const* init_spg);
    ~SpaceGroupDialog();

    std::map<char, std::string> centering_map;

    static gemmi::SpaceGroup const* getSpaceGroup(QWidget* parent, gemmi::SpaceGroup const* curr_spg);
    gemmi::SpaceGroup const* currentlySelectedSpaceGroup();

private slots:
    void on_spg_widget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);

    void on_buttonBox_clicked(QAbstractButton *button);

private:
    gemmi::SpaceGroup const* init_spg;

    Ui::SpaceGroupDialog* ui;
    void createModel();
    void populateSettings(gemmi::SpaceGroup const* spg);
    void selectFirstSetting();
    void setSelectedSpaceGroup(gemmi::SpaceGroup const* spg);
};

