/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "measuredistancedialog.h"
#include "ui_measuredistancedialog.h"
#include <math.h>
#include <vtkFollower.h>
#include <vtkVectorText.h>
#include <vtkPolyDataMapper.h>
#include <vtkCaptionActor2D.h>
#include <vtkTextProperty.h>
#include <vtkTextActor.h>
#include <vtkLeaderActor2D.h>
#include <vtkobjects.h>
#include <ciso646>

MeasureDistanceDialog::MeasureDistanceDialog(QWidget *parent, Document* doc, SceneWidget* scene):
    QDialog(parent),
    ui(new Ui::MeasureDistanceDialog), scene(scene), doc(doc) {
    ui->setupUi(this);

    connect(scene->picker_interactor->sigHandler, &InteractorSignalHandler::basisClicked, this, &MeasureDistanceDialog::actor_pick_received);
    reset();
}

MeasureDistanceDialog::~MeasureDistanceDialog() {
    delete ui;
}

void MeasureDistanceDialog::on_select_btn_clicked() {
    if (currently_selecting == true) {
        ui->select_btn->setText("Select 2 atoms");
        reset();
    }
    else {
        reset();
        scene->toPickerInteractorStyle();
        ui->select_btn->setText("Selecting... (click to cancel)");
        currently_selecting = true;
    }
}

void MeasureDistanceDialog::actor_pick_received(unsigned int basis_id, double x, double y, double z) {
    if (not isVisible()) {
        // All picking operations using the same render window interactor, do not do anything if the dialog is not visible.
        return;
    }

    if (atom1_basis != nullptr and atom2_basis != nullptr) {
        // This should not happen
        throw std::runtime_error("MeasureDistanceDialog::actor_pick_received() both positions are already picked");
    }

    if (atom1_basis == nullptr) {
        atom1_basis = doc->findBasisPosById(basis_id);
        atom1_xyz = {x, y, z};

        p1_text = vtkfactory::caption_text("P1", {x, y, z});
        scene->registerViewProp(p1_text);

        updateText();
        scene->updateView();
        return;
    }
    else {
        atom2_basis = doc->findBasisPosById(basis_id);
        atom2_xyz = {x, y, z};

        ui->select_btn->setText("Select 2 atoms");

        p2_text = vtkfactory::caption_text("P2", {x, y, z});
        scene->registerViewProp(p2_text);

        vtkNew<vtkLineSource> line_poly;
        line_poly->SetPoint1(atom1_xyz.data());
        line_poly->SetPoint2(atom2_xyz.data());
        line_poly->SetResolution(100);
        vtkNew<vtkPolyDataMapper> mapper;
        line = vtkSmartPointer<ArcteActor>::New();
        mapper->SetInputConnection(line_poly->GetOutputPort());
        line->SetMapper(mapper);
        line->GetProperty()->SetColor(0.3, 0.3, 0.3);
        line->GetProperty()->SetLineWidth(4);
        line->GetProperty()->SetPointSize(1);
        scene->registerActor(line);

        updateText();
        scene->updateView();
        currently_selecting = false;
        scene->toDefaultInteractorStyle();
    }
}

void MeasureDistanceDialog::reset() {
    removeActors();
    atom1_basis = nullptr;
    atom2_basis = nullptr;
    ui->select_btn->setText("Select 2 atoms");
    scene->toDefaultInteractorStyle();
    atom1_xyz = {0, 0, 0};
    atom2_xyz = {0, 0, 0};
    updateText();
    scene->updateView();
    currently_selecting = false;
}

void MeasureDistanceDialog::removeActors() {
    scene->removeViewProp(p1_text);
    scene->removeViewProp(p2_text);
    scene->removeActor(line);
    p1_text = vtkSmartPointer<vtkCaptionActor2D>::New();
    p2_text = vtkSmartPointer<vtkCaptionActor2D>::New();
    line = vtkSmartPointer<ArcteActor>::New();
}

void MeasureDistanceDialog::updateText() {
    ui->atom1_elem_lbl->setText("");
    ui->atom2_elem_lbl->setText("");
    ui->atom1_x_lbl->setText("");
    ui->atom1_y_lbl->setText("");
    ui->atom1_z_lbl->setText("");
    ui->atom2_x_lbl->setText("");
    ui->atom2_y_lbl->setText("");
    ui->atom2_z_lbl->setText("");
    ui->distance_lbl->setText("");

    if (atom1_basis != nullptr) {
        ui->atom1_elem_lbl->setText(QString::fromStdString(atom1_basis->species()->name));
        std::stringstream x1_ss, y1_ss, z1_ss;
        x1_ss << std::fixed << std::setprecision(4) << atom1_xyz.at(0);
        y1_ss << std::fixed << std::setprecision(4) << atom1_xyz.at(1);
        z1_ss << std::fixed << std::setprecision(4) << atom1_xyz.at(2);

        ui->atom1_x_lbl->setText(QString::fromStdString(x1_ss.str()));
        ui->atom1_y_lbl->setText(QString::fromStdString(y1_ss.str()));
        ui->atom1_z_lbl->setText(QString::fromStdString(z1_ss.str()));
    }
    if (atom2_basis != nullptr) {
        std::stringstream x2_ss, y2_ss, z2_ss;
        x2_ss << std::fixed << std::setprecision(4) << atom2_xyz.at(0);
        y2_ss << std::fixed << std::setprecision(4) << atom2_xyz.at(1);
        z2_ss << std::fixed << std::setprecision(4) << atom2_xyz.at(2);

        ui->atom2_elem_lbl->setText(QString::fromStdString(atom2_basis->species()->name));
        ui->atom2_x_lbl->setText(QString::fromStdString(x2_ss.str()));
        ui->atom2_y_lbl->setText(QString::fromStdString(y2_ss.str()));
        ui->atom2_z_lbl->setText(QString::fromStdString(z2_ss.str()));

        auto p1 = atom1_xyz;
        auto p2 = atom2_xyz;
        double dist = sqrt(pow(p2[0] - p1[0],2) + pow(p2[1] - p1[1], 2) + pow(p2[2] - p1[2], 2));
        std::stringstream dist_ss;
        dist_ss << "<font size=18>" << std::fixed << std::setprecision(4) << dist << " Å" << "</font>";
        ui->distance_lbl->setText(QString::fromStdString(dist_ss.str()));
    }
}

void MeasureDistanceDialog::closeEvent(QCloseEvent* event) {
    reset();
    QDialog::closeEvent(event);
}
