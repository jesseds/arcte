/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "measurebondangledialog.h"
#include "ui_measurebondangledialog.h"
#include <math.h>
#include <vtkFollower.h>
#include <vtkVectorText.h>
#include <vtkPolyDataMapper.h>
#include <vtkCaptionActor2D.h>
#include <vtkTextProperty.h>
#include <vtkTextActor.h>
#include <vtkLeaderActor2D.h>
#include <vtkobjects.h>
#include <ciso646>
#include <geometry.h>

MeasureBondAngleDialog::MeasureBondAngleDialog(QWidget *parent, Document* doc, SceneWidget* scene):
    QDialog(parent),
    ui(new Ui::MeasureBondAngleDialog), scene(scene), doc(doc) {
    ui->setupUi(this);

    connect(scene->picker_interactor->sigHandler, &InteractorSignalHandler::basisClicked, this, &MeasureBondAngleDialog::actor_pick_received);
    reset();
}

MeasureBondAngleDialog::~MeasureBondAngleDialog() {
    delete ui;
}

void MeasureBondAngleDialog::on_select_btn_clicked() {
    if (currently_selecting == true) {
        ui->select_btn->setText("Select 3 atoms");
        reset();
    }
    else {
        reset();
        scene->toPickerInteractorStyle();
        ui->select_btn->setText("Selecting... (click to cancel)");
        currently_selecting = true;
    }
}

void MeasureBondAngleDialog::actor_pick_received(unsigned int basis_id, double x, double y, double z) {
    if (not isVisible()) {
        // All picking operations using the same render window interactor, do not do anything if the dialog is not visible.
        return;
    }
    if (atom1_basis != nullptr and atom2_basis != nullptr and atom3_basis != nullptr) {
        // This should not happen
        throw std::runtime_error("MeasureDistanceDialog::actor_pick_received() all positions are already picked");
    }

    if (atom1_basis == nullptr) {
        atom1_basis = doc->findBasisPosById(basis_id);
        atom1_xyz = {x, y, z};

        p1_text = vtkfactory::caption_text("P1", {x, y, z});
        scene->registerViewProp(p1_text);

        updateText();
        scene->updateView();
        return;
    }
    else if (atom2_basis == nullptr) {
        atom2_basis = doc->findBasisPosById(basis_id);
        atom2_xyz = {x, y, z};

        p2_text = vtkfactory::caption_text("P2", {x, y, z});
        scene->registerViewProp(p2_text);

        vtkNew<vtkLineSource> line_poly;
        line_poly->SetPoint1(atom1_xyz.data());
        line_poly->SetPoint2(atom2_xyz.data());
        line_poly->SetResolution(100);
        vtkNew<vtkPolyDataMapper> mapper;
        line1 = vtkSmartPointer<ArcteActor>::New();
        mapper->SetInputConnection(line_poly->GetOutputPort());
        line1->SetMapper(mapper);
        line1->GetProperty()->SetColor(0.3, 0.3, 0.3);
        line1->GetProperty()->SetLineWidth(4);
        line1->GetProperty()->SetPointSize(1);
        scene->registerActor(line1);

        updateText();
        scene->updateView();
        return;
    }
    else if (atom3_basis == nullptr) {
        atom3_basis = doc->findBasisPosById(basis_id);
        atom3_xyz = {x, y, z};

        ui->select_btn->setText("Select 3 atoms");

        p3_text = vtkfactory::caption_text("P3", {x, y, z});
        scene->registerViewProp(p3_text);

        vtkNew<vtkLineSource> line_poly;
        line_poly->SetPoint1(atom2_xyz.data());
        line_poly->SetPoint2(atom3_xyz.data());
        line_poly->SetResolution(100);
        vtkNew<vtkPolyDataMapper> mapper;
        line2 = vtkSmartPointer<ArcteActor>::New();
        mapper->SetInputConnection(line_poly->GetOutputPort());
        line2->SetMapper(mapper);
        line2->GetProperty()->SetColor(0.3, 0.3, 0.3);
        line2->GetProperty()->SetLineWidth(4);
        line2->GetProperty()->SetPointSize(1);
        scene->registerActor(line2);

        updateText();
        scene->updateView();
        currently_selecting = false;
        scene->toDefaultInteractorStyle();
    }
}

void MeasureBondAngleDialog::reset() {
    removeActors();
    atom1_basis = nullptr;
    atom2_basis = nullptr;
    atom3_basis = nullptr;
    ui->select_btn->setText("Select 3 atoms");
    scene->toDefaultInteractorStyle();
    atom1_xyz = {0, 0, 0};
    atom2_xyz = {0, 0, 0};
    atom3_xyz = {0, 0, 0};
    updateText();
    scene->updateView();
    currently_selecting = false;
}

void MeasureBondAngleDialog::removeActors() {
    scene->removeViewProp(p1_text);
    scene->removeViewProp(p2_text);
    scene->removeViewProp(p3_text);
    scene->removeActor(line1);
    scene->removeActor(line2);
    p1_text = vtkSmartPointer<vtkCaptionActor2D>::New();
    p2_text = vtkSmartPointer<vtkCaptionActor2D>::New();
    p3_text = vtkSmartPointer<vtkCaptionActor2D>::New();
    line1 = vtkSmartPointer<ArcteActor>::New();
    line2 = vtkSmartPointer<ArcteActor>::New();
}

void MeasureBondAngleDialog::updateText() {
    ui->atom1_elem_lbl->setText("");
    ui->atom2_elem_lbl->setText("");
    ui->atom3_elem_lbl->setText("");
    ui->atom1_x_lbl->setText("");
    ui->atom1_y_lbl->setText("");
    ui->atom1_z_lbl->setText("");
    ui->atom2_x_lbl->setText("");
    ui->atom2_y_lbl->setText("");
    ui->atom2_z_lbl->setText("");
    ui->atom3_x_lbl->setText("");
    ui->atom3_y_lbl->setText("");
    ui->atom3_z_lbl->setText("");
    ui->distance_lbl->setText("");

    if (atom1_basis != nullptr) {
        ui->atom1_elem_lbl->setText(QString::fromStdString(atom1_basis->species()->name));
        std::stringstream x1_ss, y1_ss, z1_ss;
        x1_ss << std::fixed << std::setprecision(4) << atom1_xyz.at(0);
        y1_ss << std::fixed << std::setprecision(4) << atom1_xyz.at(1);
        z1_ss << std::fixed << std::setprecision(4) << atom1_xyz.at(2);

        ui->atom1_x_lbl->setText(QString::fromStdString(x1_ss.str()));
        ui->atom1_y_lbl->setText(QString::fromStdString(y1_ss.str()));
        ui->atom1_z_lbl->setText(QString::fromStdString(z1_ss.str()));
    }
    if (atom2_basis != nullptr) {
        std::stringstream x2_ss, y2_ss, z2_ss;
        x2_ss << std::fixed << std::setprecision(4) << atom2_xyz.at(0);
        y2_ss << std::fixed << std::setprecision(4) << atom2_xyz.at(1);
        z2_ss << std::fixed << std::setprecision(4) << atom2_xyz.at(2);

        ui->atom2_elem_lbl->setText(QString::fromStdString(atom2_basis->species()->name));
        ui->atom2_x_lbl->setText(QString::fromStdString(x2_ss.str()));
        ui->atom2_y_lbl->setText(QString::fromStdString(y2_ss.str()));
        ui->atom2_z_lbl->setText(QString::fromStdString(z2_ss.str()));
    }
    if (atom3_basis != nullptr) {
        std::stringstream x3_ss, y3_ss, z3_ss;
        x3_ss << std::fixed << std::setprecision(4) << atom3_xyz.at(0);
        y3_ss << std::fixed << std::setprecision(4) << atom3_xyz.at(1);
        z3_ss << std::fixed << std::setprecision(4) << atom3_xyz.at(2);

        ui->atom3_elem_lbl->setText(QString::fromStdString(atom3_basis->species()->name));
        ui->atom3_x_lbl->setText(QString::fromStdString(x3_ss.str()));
        ui->atom3_y_lbl->setText(QString::fromStdString(y3_ss.str()));
        ui->atom3_z_lbl->setText(QString::fromStdString(z3_ss.str()));

        auto p1 = atom1_xyz;
        auto p2 = atom2_xyz;
        auto p3 = atom3_xyz;
        std::array<double, 3> vec1 = {p2[0] - p1[0], p2[1] - p1[1], p2[2] - p1[2]};
        std::array<double, 3> vec2 = {p2[0] - p3[0], p2[1] - p3[1], p2[2] - p3[2]};
        Matrix3d deortho;
        deortho << 1, 0, 0,
                   0, 1, 0,
                   0, 0, 1;
        double angle = geom::vecAngle(deortho, vec1[0], vec1[1], vec1[2], vec2[0], vec2[1], vec2[2]);
//        if (angle > 180) {
//            angle = 180 - angle;
//        }
//        double dist = sqrt(pow(p2[0] - p1[0],2) + pow(p2[1] - p1[1], 2) + pow(p2[2] - p1[2], 2));
        std::stringstream angle_ss;
        angle_ss << "<font size=18>" << std::fixed << std::setprecision(4) << angle << " °" << "</font>";
        ui->distance_lbl->setText(QString::fromStdString(angle_ss.str()));
    }
}

void MeasureBondAngleDialog::closeEvent(QCloseEvent* event) {
    reset();
    QDialog::closeEvent(event);
}
