/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/


#include <bundledstructdialog.h>
#include <QResource>
#include <io.h>
#include <gemmi/cif.hpp>
#include <QLineEdit>
#include <QFormLayout>
#include <QLabel>
#include <QPushButton>
#include <ciso646>


QCifsResource::QCifsResource(QString const& prefix) : QResource(prefix), prefix(prefix) {
}


QStringList QCifsResource::cifList() {
    QStringList retn;

    QStringList files = children();

    for (auto const& path: files) {
        if (path.endsWith(".cif")) {
            retn.push_back(prefix + "/" + path);
        }
    }
    return retn;
}


bool StructFilterProxyModel::filterAcceptsRow(int source_row, QModelIndex const& source_parent) const {
    if (source_parent.internalPointer() == nullptr) {
        return true;
    }
    QModelIndex idx = sourceModel()->index(source_row, 0, source_parent);

    return idx.data(Qt::DisplayRole).toString().contains(filterRegularExpression());
}


BundledStructDialog::BundledStructDialog(QWidget* parent) : QDialog(parent) {
    table = new QTreeView(this);
    table->setAnimated(true);
    model = new QStandardItemModel(this);
    model_prox = new StructFilterProxyModel(this);
    btns = new QDialogButtonBox(this);
    layout = new QVBoxLayout();
    setLayout(layout);
    filter = new QLineEdit(this);
    setModal(true);

    filter->setPlaceholderText("Search structures");

    model_prox->setSourceModel(model);
    model_prox->setFilterCaseSensitivity(Qt::CaseInsensitive);
    table->setModel(model_prox);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);

    setWindowTitle("Crystal structure database");

    // Filter widgets
    auto filter_layout = new QFormLayout();
    filter_layout->addRow("Filter:", filter);

    auto btns_layout = new QHBoxLayout();

    auto collapse_btn = new QPushButton("Collapse all", this);
    collapse_btn->setIcon(QIcon(":/icons/icons/collapse-all.svg"));
    auto expand_btn = new QPushButton("Expand all", this);
    expand_btn->setIcon(QIcon(":/icons/icons/expand-all.svg"));

    btns_layout->addWidget(expand_btn);
    btns_layout->addWidget(collapse_btn);
    btns_layout->addLayout(filter_layout);

    connect(collapse_btn, &QPushButton::clicked, table, &QTreeView::collapseAll);
    connect(expand_btn, &QPushButton::clicked, table, &QTreeView::expandAll);
    connect(filter, &QLineEdit::textChanged, model_prox, &QSortFilterProxyModel::setFilterFixedString);
    connect(btns, &QDialogButtonBox::rejected, this, &BundledStructDialog::close);
    connect(btns, &QDialogButtonBox::accepted, this, &BundledStructDialog::accept);
    connect(table, &QTreeView::doubleClicked, this, &BundledStructDialog::onDoubleClicked);

    model_prox->setFilterKeyColumn(0);

    layout->addLayout(btns_layout);

    // Table
    elements = makeSectionRowItem("Elements");
    other_compounds = makeSectionRowItem("Other compounds");
    ab = makeSectionRowItem("AB");
    ab2 = makeSectionRowItem("AB₂");
    ambn = makeSectionRowItem("AₘBₙ");

    resize(1100, 700);

    layout->addWidget(table);
    layout->addWidget(btns);

    table->setAlternatingRowColors(true);

    QStringList headers {
        "Composition",
        "Space group",
        "Crystal system",
        "Description"
    };

    model->setHorizontalHeaderLabels(headers);

    model->appendRow(elements);
    table->setFirstColumnSpanned(elements->row(), QModelIndex(), true);
    model->appendRow(ab);
    table->setFirstColumnSpanned(ab->row(), QModelIndex(), true);
    model->appendRow(ab2);
    table->setFirstColumnSpanned(ab2->row(), QModelIndex(), true);
    model->appendRow(ambn);
    table->setFirstColumnSpanned(ambn->row(), QModelIndex(), true);
    model->appendRow(other_compounds);
    table->setFirstColumnSpanned(other_compounds->row(), QModelIndex(), true);

    importStructures();

    // Dialog buttons
    btns->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
}

void BundledStructDialog::onDoubleClicked(QModelIndex const& idx) {
    auto data = idx.data(Qt::UserRole);
    if (data.isValid()) {
        QFileInfo path {data.toString()};
        assert(path.exists());

        emit structureAccepted(path);
        close();
    }
}

QList<QStandardItem*> BundledStructDialog::makeStructureRow(Document const& doc, QString const& cif_url) {
    auto static constexpr flags = Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    auto comp = new QStandardItem(QString::fromStdString(doc.compStr()));
    auto spg = new QStandardItem(doc.spaceGroup()->hm);

    QString sys = doc.spaceGroup()->crystal_system_str();
    sys[0] = sys[0].toUpper();
    auto sys_item = new QStandardItem(sys);

    comp->setFlags(flags);
    spg->setFlags(flags);
    sys_item->setFlags(flags);

    QString note = QString::fromStdString(doc.descr());

    // Most of the cif files are taken from the same source so we remove some unnecessary information from the description
    if (note != "") {
        // Strip start of note
        while (true) {
            if (note.startsWith('\n') or note.startsWith(' ') or note.startsWith(';')) {
                note.remove(0, 1);
            }
            else {
                break;
            }
        }

        // Strip end of note
        while (true) {
            if (note.endsWith('\n') or note.endsWith(' ') or note.endsWith(';')) {
                note.chop(1);
            }
            else {
                break;
            }
        }


        if (note.startsWith("Second edition.")) {
            while (true) {
                if (not note.startsWith('\n') and note.size() > 0) {
                    note.remove(0, 1);
                }
                else {
                    break;
                }
            }
        }

        note = note.simplified();
        note = note.replace("Note: ", "");
        if (note != "") {
            note[0] = note[0].toUpper();
        }
    }

    auto note_item = new QStandardItem(note);
    note_item->setFlags(flags);

    comp->setData(cif_url, Qt::UserRole);
    spg->setData(cif_url, Qt::UserRole);
    sys_item->setData(cif_url, Qt::UserRole);
    note_item->setData(cif_url, Qt::UserRole);

    return {comp, spg, sys_item, note_item};
}


QStandardItem* BundledStructDialog::makeSectionRowItem(QString name) {
    auto static constexpr flags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    auto retn = new QStandardItem(name);
    retn->setFlags(flags);
    return retn;
}


void BundledStructDialog::importStructures() {
    QCifsResource cif_pref {":/cifs/cifs"};
    QStringList cifs = cif_pref.cifList();

    auto new_doc = std::make_unique<Document>();

    for (QString const& path: cifs) {
        QFile cif_file {path};
        cif_file.open(QIODevice::ReadOnly | QIODevice::Text);

        QString cif_text = cif_file.readAll();

        auto cifdoc = gemmi::cif::read_string(cif_text.toStdString());

        new_doc->toDefault();

        try {
            io::load_cif_in_place(cifdoc, new_doc.get());
        }
        catch (std::exception) {
            qDebug() << "Failed to load bundled structure " << path;
            continue;
        }

        auto item = makeStructureRow(*new_doc, path);
        auto comp = new_doc->composition();
        auto elems = new_doc->elements();

        if (comp.size() == 1) {
            elements->appendRow(item);
            continue;
        }
        else if (comp.size() == 2 and comp[elems[0]] == comp[elems[1]]) {
            ab->appendRow(item);
            continue;
        }
        else if (comp.size() == 2 and (comp[elems[0]] == comp[elems[1]]*2 or comp[elems[0]]*2 == comp[elems[1]]) ) {
            ab2->appendRow(item);
            continue;
        }
        else if (comp.size() == 2) {
            ambn->appendRow(item);
            continue;
        }

        else {
            other_compounds->appendRow(item);
            continue;
        }
    }
    table->setColumnWidth(0, 125);
    table->resizeColumnToContents(1);
    table->resizeColumnToContents(2);
    table->setSortingEnabled(true);
    table->sortByColumn(0, Qt::SortOrder::AscendingOrder);
}

void BundledStructDialog::accept() {
    QModelIndex curr = table->currentIndex();

    // Top level items are sections and do not correspond to a specific structure, do nothing
    if (curr.parent() == QModelIndex()) {
        return;
    }

    QString cif_path = curr.data(Qt::UserRole).toString();

    QResource cif_rsc {cif_path};
    QFile cif_file {cif_rsc.absoluteFilePath()};
    auto finfo = QFileInfo{cif_file};
    emit structureAccepted(finfo);
    close();
}

