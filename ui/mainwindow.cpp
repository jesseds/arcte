/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "mainwindow.h"
#include "standardtitlebar.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <gemmi/cif.hpp>
#include <io.h>
#include <spacegroupdialog.h>
#include <QColorDialog>
#include <vieworientationdialog.h>
#include <cctype>
#include <imageexportdialog.h>
#include <modelexportdialog.h>
#include <aboutdialog.h>
#include <QProgressDialog>
#include <modelexportworker.h>
#include <recentfileaction.h>
#include <ciso646>
#include <calculatordialog.h>
#include <QStyle>
#include <arcteactor.h>
#include <updatechecker.h>
#include <QDesktopServices>
#include <vtkobjects.h>
#include <QMimeData>
#include <frameless.h>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow){
    ui->setupUi(this);
    settings = new QSettings();
    greeter = new Greeter(this);
    resize(1400, 800);

    auto titlebar = setupTitleBar(this, menuBar());
    if (titlebar) {
        setMenuWidget(titlebar.value());
    }

    #ifdef WIN32
        ui->menuTools->setWindowFlag(Qt::NoDropShadowWindowHint, true);
        ui->menuFile->setWindowFlag(Qt::NoDropShadowWindowHint, true);
        ui->menuAbout->setWindowFlag(Qt::NoDropShadowWindowHint, true);
        ui->menuMeasure->setWindowFlag(Qt::NoDropShadowWindowHint, true);
        ui->menuVeiw->setWindowFlag(Qt::NoDropShadowWindowHint, true);
    #endif


    // Setup recent files menu
    recent_files_menu = new QMenu(this);
    recent_files_menu->setTitle("Recent Files");
    ui->menuFile->insertMenu(ui->actionSave, recent_files_menu);

    // Create and insert widget for controlling view rotation amount
    view_degrees = new QSpinBox(this);
    view_degrees->setRange(-180, 180);
    view_degrees->setValue(90);
    view_degrees->setSuffix(" deg");
    ui->view_toolbar->insertWidget(ui->actionYawDown, view_degrees);

    // Initialize the document
    doc = std::make_unique<Document>();

    // Signal connections
    connect(doc.get(), &Document::aModified, ui->a_edit, &QDoubleSpinBox::setValue);
    connect(doc.get(), &Document::bModified, ui->b_edit, &QDoubleSpinBox::setValue);
    connect(doc.get(), &Document::cModified, ui->c_edit, &QDoubleSpinBox::setValue);
    connect(doc.get(), &Document::nxModified, ui->nx_edit, &QSpinBox::setValue);
    connect(doc.get(), &Document::nyModified, ui->ny_edit, &QSpinBox::setValue);
    connect(doc.get(), &Document::nzModified, ui->nz_edit, &QSpinBox::setValue);
    connect(doc.get(), &Document::alphaModified, ui->alpha_edit, &QDoubleSpinBox::setValue);
    connect(doc.get(), &Document::betaModified, ui->beta_edit, &QDoubleSpinBox::setValue);
    connect(doc.get(), &Document::gammaModified, ui->gamma_edit, &QDoubleSpinBox::setValue);
    connect(doc.get(), &Document::atomsVisibleModified, ui->atoms_groupbox, &QGroupBox::setChecked);
    connect(doc.get(), &Document::bondsVisibleModified, ui->bonds_groupbox, &QGroupBox::setChecked);
    connect(doc.get(), &Document::unitCellVisibleModified, ui->unitcell_groupbox, &QGroupBox::setChecked);
    connect(doc.get(), &Document::unitCellLineWidthModified, ui->unit_cell_width, &QSpinBox::setValue);
    connect(doc.get(), &Document::spaceGroupModified, this, &MainWindow::fillSpgInfo);
    connect(doc.get(), &Document::radiusScaleModified, ui->radscale_edit, &QDoubleSpinBox::setValue);
    connect(doc.get(), &Document::radiusTypeModified, this, &MainWindow::onDocRadiiTypeChanged);
    connect(doc.get(), &Document::bondRadiusModified, ui->bond_radii_edit, &QDoubleSpinBox::setValue);
    connect(doc.get(), &Document::bondStyleModified, ui->bond_style_edit, &QComboBox::setCurrentIndex);
    connect(doc.get(), &Document::basisPosAdded, this, &MainWindow::reconstructSceneAndRender);
    connect(doc.get(), &Document::bondAdded, this, &MainWindow::reconstructSceneAndRender);

    connect(doc.get(), &Document::basesCleared, this, &MainWindow::reconstructSceneAndRender);
    connect(doc.get(), &Document::bondsCleared, this, &MainWindow::reconstructSceneAndRender);


    // Dialogs
    viewport = new SceneWidget(this, doc.get());
    uvw_win = new ViewOrientationDialog(this, viewport);
    dist_win = new MeasureDistanceDialog(this, doc.get(), viewport);
    bondangle_win = new MeasureBondAngleDialog(this, doc.get(), viewport);
    calc_win = new CalculatorDialog(this, doc.get());
    xrd_win = new XrdDialog(this, doc.get());
    zone_pattern_win = new ZoneAxisPatternDialog(this, doc.get());
    structure_factors_win = new StructureFactorsDialog(this, doc.get());
    bundled_struct_win = new BundledStructDialog(this);

    connect(bundled_struct_win, &BundledStructDialog::structureAccepted, this, &MainWindow::loadCifFile);

    doc->suppress_view_update = true;
    doc->toDefault();
    doc->suppress_view_update = false;

    showGreeter();
    setCentralWidget(greeter);
    setWindowTitleNew();
    ui->roughness->setValue(viewport->roughness);

    // Other ui setup
    createDocks();
    populateRecentFiles();
    connect(basisedit, &BasisEditor::emitReconstructView, this, &MainWindow::reconstructSceneAndRender);
    connect(bondedit, &BondEditor::emitReconstructView, this, &MainWindow::reconstructSceneAndRender);
    reconstructScene();
    viewport->resetCamera();
    showMaximized();
    unsaved_changes = false;
    setAcceptDrops(true);

    connect(doc.get(), &Document::writableModified, this, &MainWindow::onWritableModified);



}

MainWindow::~MainWindow(){
    delete ui;
}

QStringList MainWindow::recentFiles() {
    if (settings->contains("recent_files")) {
        QStringList rec_files = settings->value("recent_files").value<QStringList>();
        rec_files.removeDuplicates();
        return rec_files;
    }
    return QStringList {};
}

void MainWindow::addRecentFiles(QString str) {
    QStringList curr_files = recentFiles();
    curr_files.removeAll(str);
    curr_files.append(str);
    settings->setValue("recent_files", QVariant::fromValue(curr_files));
    populateRecentFiles();
}

void MainWindow::populateRecentFiles() {
    recent_files_menu->clear();
    QStringList files = recentFiles();

    int count = 0;
    for (int i=files.size()-1; i>=0; --i) {
        if (count == 10) {break;}
        if (not QFileInfo::exists(files.at(i))) {continue;}

        RecentFileAction* act = new RecentFileAction(this);
        act->setText(files.at(i));
        connect(act, &RecentFileAction::triggeredWithPath, this, &MainWindow::loadCifFile);
        recent_files_menu->addAction(act);
        ++count;
    }
}

void MainWindow::createDocks() {
    QVBoxLayout* basis_layout = new QVBoxLayout();
//    auto marg = basis_layout->contentsMargins();
    basis_layout->setContentsMargins(6, 6, 6, 6);
    ui->tab_2->setLayout(basis_layout);
    basisedit = new BasisEditor(this, this->doc.get());
    ui->tab_2->layout()->addWidget(basisedit);

    QVBoxLayout* bond_layout = new QVBoxLayout();
    bond_layout->setContentsMargins(6, 6, 6, 6);
    ui->tab_6->setLayout(bond_layout);
    bondedit = new BondEditor(ui->tabWidget, this->doc.get());
    ui->tab_6->layout()->addWidget(bondedit);

    ui->properties_dock->setMinimumWidth(380);
}

void MainWindow::loadCifFile(QFileInfo filepath) {
    if (! filepath.exists()) {
        std::string descr = "File " + filepath.absoluteFilePath().toStdString() + " does not exist";
        QMessageBox::warning(this, "File does not exist", QString::fromStdString(descr));
        populateRecentFiles();
        return;
    }

    // The file provided might be nae embedded QResource which we will handled specially
    bool is_resource = filepath.absoluteFilePath().startsWith(":/");

    if (unsaved_changes) {
        auto btn = QMessageBox::warning(this, "Unsaved changed",
                                        "There are unsaved changes, do you wish to continue?", QMessageBox::Discard | QMessageBox::Cancel);
        if (btn == QMessageBox::Cancel){
            return;
        }
    }

    try {
        // Here just check that loading is successful, is not loaded into window yet
        io::load_cif(filepath);
        closeAllDialogs();
        showViewport();
    }
    catch (std::exception& e) {
        std::stringstream descr {};
        descr << "Failed to read cif with error: \n\n";
        descr << e.what();
        QMessageBox::critical(this, "Failed to read cif file.", descr.str().data());
        doc->suppress_view_update = false;
        return;
    }

    // If no exception is thrown the read is successful
    doc->suppress_view_update = true;
    doc->toDefault();
    auto cifdoc = io::getCifDoc(filepath);
    io::load_cif_in_place(cifdoc, doc.get());
    doc->suppress_view_update = false;
    unsaved_changes = false;

    // Don't add to recent files if the cif file was a QResource
    if (not is_resource)
        addRecentFiles(filepath.absoluteFilePath());

    if (is_resource)
        setWindowTitleNewFile(QString::fromStdString(doc->compStr()));
    else
        setWindowTitleNewFile(filepath.absoluteFilePath());

    viewport->resetCamera();

    fillSpgInfo();
    viewport->setUvw(1, 0, 0);
    reconstructScene();
    viewport->resetCamera();
}

void MainWindow::on_actionOpen_triggered() {
    QString filepath = QFileDialog::getOpenFileName(this, "Open cif",
                                                    last_dir, "Crystallographic information file (*.cif)");
    if (QFileInfo(filepath).absoluteFilePath() != "") {
        loadCifFile(QFileInfo{filepath});
        closeAllDialogs();
    }
}

void MainWindow::fillSpgInfo() {
    auto spg = doc->spaceGroup();
    auto nops = spg->operations().sym_ops.size() * spg->operations().cen_ops.size();
    std::string system = spg->crystal_system_str();
    system[0] = std::toupper(system[0]);
    std::string centric = spg->operations().is_centric() ? "Yes" : "No";

    ui->hm_lbl->setText(QString::fromStdString(doc->spaceGroup()->xhm()));
//    ui->pg_lbl->setText(doc->spaceGroup()->point_group_hm());
    ui->num_lbl->setText(QString::fromStdString(std::to_string(doc->spaceGroup()->number)));
//    ui->order_lbl->setText(QString::fromStdString(std::to_string(nops)));
    ui->system_lbl->setText(QString::fromStdString(system));
    ui->centric_lbl->setText(QString::fromStdString(centric));
//    ui->hall_lbl->setText(QString::fromStdString(spg->hall));
    ui->laue_lbl->setText(QString::fromStdString(spg->laue_str()));
}

void MainWindow::on_actionNew_triggered(){
    if (unsaved_changes) {
        auto btn = QMessageBox::warning(this, "Unsaved changes",
                                        "There are unsaved changes, do you wish to continue?", QMessageBox::Discard | QMessageBox::Cancel);
        if (btn == QMessageBox::Cancel){
            return;
        }
    }
    doc->suppress_view_update = true;
    doc->toDefault();
    doc->suppress_view_update = false;
    setWindowTitleNew();
    unsaved_changes = false;
    reconstructScene();
    viewport->setUvw(1, 0, 0);
    viewport->resetCamera();
    closeAllDialogs();
    showViewport();
}

void MainWindow::setWindowTitleNewFile(QString const& file){
    QFileInfo fileinfo(file);
    setWindowTitle("Arcte - " + fileinfo.fileName());
}

void MainWindow::setWindowTitleNew(){
    setWindowTitle("Arcte - (Unsaved file)");
}

void MainWindow::on_actionClose_triggered(){
    close();
}

void MainWindow::closeEvent(QCloseEvent* event){
    if (unsaved_changes){
        auto btn = QMessageBox::warning(this, "Unsaved changes", "There are unsaved changes, do you wish to exit anyway?",
                                        QMessageBox::Yes | QMessageBox::Cancel);
        if (btn == QMessageBox::Cancel){
            event->ignore();
            return;
        }
    }
    QMainWindow::closeEvent(event);
}

void MainWindow::closeAllDialogs() {
    dist_win->close();
    bondangle_win->close();
    uvw_win->close();
    calc_win->close();
    xrd_win->close();
    zone_pattern_win->close();
    structure_factors_win->close();
}

void MainWindow::reconstructScene(){
    if (doc->suppress_view_update) {
        return;
    }

    viewport->clearActors();
    auto objs = vtkfactory::doc_to_vtkactors(*doc);

    for (auto& i: objs){
        viewport->registerActor(i);
    }
    viewport->updateAxesTransform(*doc);
}

void MainWindow::reconstructSceneAndRender(){
    reconstructScene();
    viewport->updateView();
}

void MainWindow::on_spg_edit_clicked(){
    gemmi::SpaceGroup const* new_spg = SpaceGroupDialog::getSpaceGroup(this, doc->spaceGroup());
    if (new_spg == nullptr){
        return;
    }
    if (new_spg != doc->spaceGroup()){
        doc->setSpaceGroup(new_spg);
        reconstructScene();
        viewport->updateView();
        unsaved_changes = true;
    }
}

void MainWindow::on_actionProjection_toggled(bool enabled){
    viewport->setProjOrthographic(enabled);
}

void MainWindow::on_atom_radii_select_currentTextChanged(QString const& text){
    std::map<std::string, AtomRadius> static type_map {
        {"Atomic", AtomRadius::Atomic},
        {"Van der Wall", AtomRadius::VanDerWall},
        {"Covalent", AtomRadius::Covalent},
        {"Metallic", AtomRadius::Metallic},
    };
    if (doc->setRadiusType(type_map[text.toStdString()])) {
        reconstructSceneAndRender();
    }
}

void MainWindow::on_a_edit_editingFinished(){
    if (doc->setA(ui->a_edit->value())) {
        reconstructScene();
        viewport->resetCamera();
        unsaved_changes = true;
    }
}

void MainWindow::on_b_edit_editingFinished(){
    if(doc->setB(ui->b_edit->value())) {
        reconstructScene();
        viewport->resetCamera();
        unsaved_changes = true;
    }
}

void MainWindow::on_c_edit_editingFinished(){
    if (doc->setC(ui->c_edit->value())) {
        reconstructScene();
        viewport->resetCamera();
        unsaved_changes = true;
    }
}

void MainWindow::on_alpha_edit_editingFinished(){
    if(doc->setAlpha(ui->alpha_edit->value())) {
        reconstructScene();
        viewport->resetCamera();
        unsaved_changes = true;
    }
}

void MainWindow::on_beta_edit_editingFinished(){
    if(doc->setBeta(ui->beta_edit->value())) {
        reconstructScene();
        viewport->resetCamera();
        unsaved_changes = true;
    }
}

void MainWindow::on_gamma_edit_editingFinished(){
    if(doc->setGamma(ui->gamma_edit->value())) {
        reconstructScene();
        viewport->resetCamera();
        unsaved_changes = true;
    }
}

void MainWindow::on_aa_edit_stateChanged(int state){
    bool checked = state == 0 ? false : true;
    viewport->setUseAA(checked);
}

void MainWindow::on_bkgcolor_edit_clicked(){
    auto init = viewport->backgroundColor();
    QColor newcol = QColorDialog::getColor(QColor::fromRgbF(init[0], init[1], init[2]), this);
    if (newcol.isValid()){
        viewport->setBackgroundColor(newcol.redF(), newcol.greenF(), newcol.blueF());
    }
}

void MainWindow::on_radscale_edit_editingFinished(){
    if (doc->setRadiusScale(ui->radscale_edit->value())){
        reconstructSceneAndRender();
        unsaved_changes = true;
    }
}

void MainWindow::on_wyc_btn_clicked(){
    QDialog* diag = new QDialog(this);
    diag->resize(300, 600);
    diag->setWindowTitle("General position");
    QTreeWidget* tree = new QTreeWidget(diag);
    QStringList lst {"Number", "Wyckoff position"};
    tree->setHeaderLabels(lst);
    QVBoxLayout* layout = new QVBoxLayout();
    diag->setLayout(layout);
    layout->addWidget(tree);
    QTreeWidgetItem* cen = new QTreeWidgetItem();
    cen->setText(0, "Centering operations");
    QTreeWidgetItem* sym = new QTreeWidgetItem();
    sym->setText(0, "Symmetry operations");
    tree->addTopLevelItem(cen);
    tree->addTopLevelItem(sym);

    int i = 1;
    for (auto& op: centeringString(doc->spaceGroup())){
        QTreeWidgetItem* item = new QTreeWidgetItem();
        item->setText(0, QString::fromStdString(std::to_string(i)));
        item->setText(1, QString::fromStdString(op));
        cen->addChild(item);
        ++i;
    }

    i = 1;
    for (auto & op: doc->spaceGroup()->operations().sym_ops){
        QTreeWidgetItem* item = new QTreeWidgetItem();
        item->setText(0, QString::fromStdString(std::to_string(i)));
        item->setText(1, QString::fromStdString(op.triplet()));
        sym->addChild(item);
        ++i;
    }
    tree->setFirstColumnSpanned(0, QModelIndex(), true);
    tree->setFirstColumnSpanned(1, QModelIndex(), true);
    tree->setAlternatingRowColors(true);
    tree->expandAll();
    diag->exec();
}

void MainWindow::on_mesh_style_currentTextChanged(QString const text) {
    viewport->setViewType(text.toStdString());
    reconstructScene();
    viewport->updateView();
}

void MainWindow::on_actionResetCamera_triggered() {
    viewport->resetCamera();
}

void MainWindow::on_unit_cell_width_editingFinished() {
    if(doc->setUnitCellLineWidth(ui->unit_cell_width->value())) {
        reconstructSceneAndRender();
    }
}

void MainWindow::on_unitcell_groupbox_toggled(bool checked) {
    if(doc->setUnitCellVisible(checked)) {
        reconstructSceneAndRender();
    }
}

void MainWindow::on_bond_radii_edit_editingFinished() {
    if(doc->setBondRadius(ui->bond_radii_edit->value())) {
        reconstructSceneAndRender();
    }
}

void MainWindow::on_bond_col_btn_clicked() {
    std::array<double, 3> col = doc->bondColor();
    QColor old = QColor::fromRgbF(col[0], col[1], col[2]);
    QColor new_col = QColorDialog::getColor(old);
    std::array<double, 3> col_ary = {new_col.redF(), new_col.greenF(), new_col.blueF()};
    if (doc->setBondColor(col_ary)) {
        reconstructSceneAndRender();
    }
}

void MainWindow::on_rendering_box_currentTextChanged(const QString &arg1) {
    viewport->rendering = ui->rendering_box->currentText().toStdString();
    reconstructSceneAndRender();
}

void MainWindow::on_roughness_valueChanged(double value) {
    viewport->roughness = value;
    reconstructSceneAndRender();
}

void MainWindow::on_actionExport_image_triggered() {
    ImageExportDialog* win = new ImageExportDialog(this, viewport);
    win->exec();
}

void MainWindow::on_actionExport_model_triggered() {
    ModelExportDialog* win = new ModelExportDialog(this, this->doc.get());
    if (win->exec()) {
        auto printable_actors = vtkfactory::stl_exportable_objects(*doc.get());
        if (win->kind ==  "union"){
            QProgressDialog* progress = new QProgressDialog(this);
            progress->setWindowTitle("Exporting STL");
            progress->setLabelText("Generating STL model (this may take a while)...");
            progress->setModal(true);
            progress->setMinimum(0);
            progress->setMaximum(printable_actors.size());
            progress->setMinimumDuration(0);
            progress->setAutoClose(false);

            progress->resize(500, progress->height());
            ModelExportWorker* worker = new ModelExportWorker(doc.get(), printable_actors, win->filepath());
            connect(worker, &ModelExportWorker::update_count, progress, &QProgressDialog::setValue);
            connect(progress, &QProgressDialog::canceled, worker, &ModelExportWorker::cancel);
            connect(worker, &ModelExportWorker::update_text, progress, &QProgressDialog::setLabelText);
            connect(worker, &ModelExportWorker::update_btn_text, progress, &QProgressDialog::setCancelButtonText);
            connect(worker, &ModelExportWorker::finished, progress, &QProgressDialog::close);

            progress->show();
            worker->start();
        }
        else if (win->kind == "separate") {
            io::export_append_stl(win->filepath(), printable_actors);
        }
    }
}

void MainWindow::hideEvent(QHideEvent* event) {
    // Manually save layout to restore when the window is minimized. manually restored when shown to force docks to be shown
    window_state = saveState();
    QMainWindow::hideEvent(event);
}

void MainWindow::showEvent(QShowEvent* event) {
    // Manually save layout to restore when the window is minimized. manually restored when shown to force docks to be shown
    QMainWindow::showEvent(event);
    restoreState(window_state);
}

void MainWindow::on_actionAbout_triggered() {
    auto win = new AboutDialog(this);
    win->exec();
}

void MainWindow::on_actionSaveAs_triggered() {
    if (doc->nPositions() == 0) {
        QMessageBox::warning(this, "Failed to save", "Must have at least 1 atom position to save file.");
        return;
    }
    auto outpath_str = QFileDialog::getSaveFileName(this, "Save as cif file", "", "Crystallographic Information File (*.cif)");
    QFileInfo outpath {outpath_str};
    if (outpath.absoluteFilePath() != "") {
        try {
            io::export_cif(outpath, *doc);
            doc->setFilePath(outpath);
            unsaved_changes = false;
            setWindowTitleNewFile(outpath.fileName());
            }
        catch (std::exception& e) {
            std::stringstream descr {};
            descr << "Failed to save cif with error: \n\n";
            descr << e.what();
            QMessageBox::critical(this, "Failed to save cif file.", descr.str().data());
        }
    }
}

void MainWindow::on_actionView_uvw_triggered() {
    uvw_win->show();
}

void MainWindow::on_actionRollUp_triggered() {
    viewport->roll(view_degrees->value());
}

void MainWindow::on_actionRollDown_triggered() {
    viewport->roll(-view_degrees->value());
}

void MainWindow::on_actionYawUp_triggered() {
    viewport->yaw(view_degrees->value());
}

void MainWindow::on_actionYawDown_triggered() {
    viewport->yaw(-view_degrees->value());
}

void MainWindow::on_actionPitchUp_triggered() {
    viewport->pitch(-view_degrees->value());
}

void MainWindow::on_actionPitchDown_triggered() {
    viewport->pitch(view_degrees->value());
}

void MainWindow::on_actionSave_triggered() {
    if (doc->nPositions() == 0) {
        QMessageBox::warning(this, "Failed to save", "Must have at least 1 atom position to save file.");
        return;
    }
    QFileInfo fpath = doc->filepath();
    bool exists = QFileInfo::exists(fpath.absoluteFilePath());
    if (fpath.absoluteFilePath() == "" or not exists) {
        on_actionSaveAs_triggered();
        return;
    }
    try {
        io::export_cif(doc->filepath(), *doc);
        unsaved_changes = false;
    }
    catch (std::exception& e) {
        std::stringstream descr {};
        descr << "Failed to save cif with error: \n\n";
        descr << e.what();
        QMessageBox::critical(this, "Failed to save cif file.", descr.str().data());
    }
}

void MainWindow::onWritableModified() {
    unsaved_changes = true;
    showViewport();
}

void MainWindow::on_actionCalculator_triggered() {
    calc_win->showWithDocumentParams(doc.get());
}

void MainWindow::on_bond_style_edit_currentIndexChanged(int index) {
    if (index == BondStyle::Solid) {
        ui->bond_col_btn->setEnabled(true);
        ui->bond_color_lbl->setEnabled(true);
        if(doc->setBondStyle(BondStyle::Solid)) {
            reconstructSceneAndRender();
        }
    }
    if (index == BondStyle::Dual) {
        ui->bond_col_btn->setEnabled(false);
        ui->bond_color_lbl->setEnabled(false);
        if(doc->setBondStyle(BondStyle::Dual)){
            reconstructSceneAndRender();
        }
    }
}

void MainWindow::on_atoms_groupbox_toggled(bool checked) {
    if (doc->setAtomsVisible(checked)) {
        reconstructSceneAndRender();
    }
}

void MainWindow::onDocRadiiTypeChanged(AtomRadius type) {
    std::map<AtomRadius, std::string> static type_map {
        {AtomRadius::Atomic,  "Atomic"},
        {AtomRadius::VanDerWall, "Van der Wall"},
        {AtomRadius::Covalent, "Covalent"},
        {AtomRadius::Metallic, "Metallic"}
    };
    int i = ui->atom_radii_select->findText(QString::fromStdString(type_map[type]));
    ui->atom_radii_select->setCurrentIndex(i);
}

void MainWindow::on_actionGo_to_webpage_triggered() {
    QDesktopServices::openUrl(QString("https://gitlab.com/jesseds/arcte"));
}

void MainWindow::on_actionCheck_for_updates_triggered() {
    UpdateChecker* updater = new UpdateChecker();
    updater->show_no_update_message = true;
    updater->show_connection_error = true;
    updater->run();
}

void MainWindow::on_bonds_groupbox_toggled(bool checked) {
    if(doc->setBondsVisible(checked)) {
        reconstructSceneAndRender();
    }
}

void MainWindow::on_actionMeasure_distance_triggered() {
    if (bondangle_win->isVisible()) {
        QMessageBox::information(this, "Close other measurement windows", "Only 1 measurement tool can be open at a time.");
        return;;
    }
    dist_win->reset();
    dist_win->show();
}

void MainWindow::on_actionMeasure_bond_angle_triggered() {
    if (dist_win->isVisible()) {
        QMessageBox::information(this, "Close other measurement windows", "Only 1 measurement tool can be open at a time.");
        return;;
    }
    bondangle_win->reset();
    bondangle_win->show();
}

void MainWindow::on_nx_edit_editingFinished() {
    if (doc->setNx(ui->nx_edit->value())) {
        reconstructScene();
        viewport->resetCamera();
    }
}

void MainWindow::on_ny_edit_editingFinished() {
    if (doc->setNy(ui->ny_edit->value())) {
        reconstructScene();
        viewport->resetCamera();
    }
}
void MainWindow::on_nz_edit_editingFinished() {
    if (doc->setNz(ui->nz_edit->value())) {
        reconstructScene();
        viewport->resetCamera();
    }
}

void MainWindow::on_actionXrd_triggered() {
    xrd_win->show();
    xrd_win->raise();
}

void MainWindow::showGreeter() {
    if (centralWidget() == greeter) {
        return;
    }
    else {
        setCentralWidget(greeter);
        viewport->setVisible(false);
        greeter->setVisible(true);
    }
}

void MainWindow::showViewport() {
    if (centralWidget() == viewport) {
        return;
    }
    else {
        setCentralWidget(viewport);
        viewport->setVisible(true);
        greeter->setVisible(false);
    }
}

void MainWindow::dragEnterEvent(QDragEnterEvent* e) {
    if (e->mimeData()->hasUrls()) {
        e->acceptProposedAction();
    }
}

void MainWindow::dropEvent(QDropEvent* e) {
    if (not e->mimeData()->hasUrls()) {
        return;
    }
    else {
        loadCifFile(QFileInfo(e->mimeData()->urls()[0].toLocalFile()));
    }
}

void MainWindow::on_actionZone_axis_diffraction_pattern_triggered() {
//    zone_pattern_win->waitReady();
    zone_pattern_win->show();
    zone_pattern_win->raise();
}


void MainWindow::on_actionStructure_factors_triggered() {
    structure_factors_win->show();

}


void MainWindow::on_actionLoad_bundled_triggered() {
    bundled_struct_win->show();
}

