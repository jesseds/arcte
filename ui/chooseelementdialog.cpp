/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "chooseelementdialog.h"
#include "ui_chooseelementdialog.h"
#include <ciso646>

ChooseElementDialog::ChooseElementDialog(QWidget* parent, std::vector<chem::Element const*> existing_elems) :
    QDialog(parent),
    ui(new Ui::ChooseElementDialog) {
    ui->setupUi(this);
    this->existing_elems = existing_elems;
    initializeElementButtons();
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &ChooseElementDialog::accepted);
    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &ChooseElementDialog::rejected);
}

ChooseElementDialog::~ChooseElementDialog(){
    delete ui;
}


std::vector<chem::Element const*> ChooseElementDialog::selectedNewElements(){
    std::vector<chem::Element const*> retn;
    for (auto& i: ui->topelements->children()){
        QPushButton* btn = dynamic_cast<QPushButton*>(i);
        if (btn){
            if (btn->isChecked() and btn->isEnabled()){
                retn.push_back(chem::table.findBySym(btn->text().toStdString()));
            }
        }
    }
    for (auto& i: ui->bottomelements->children()){
        QPushButton* btn = dynamic_cast<QPushButton*>(i);
        if (btn){
            if (btn->isChecked() and btn->isEnabled()){
                retn.push_back(chem::table.findBySym(btn->text().toStdString()));
            }
        }
    }
    return retn;
}


void ChooseElementDialog::initializeElementButtons(){
    // Add tooltips to the buttons
    for (auto& elem: ui->topelements->children()) {
        QPushButton* btn = dynamic_cast<QPushButton*>(elem);
        if (btn) {
            chem::Element const* curr_elem = chem::table.findBySym(btn->text().toStdString());
            btn->setToolTip(QString::fromStdString(curr_elem->name));
        }
    }
    for (auto& elem: ui->bottomelements->children()) {
        QPushButton* btn = dynamic_cast<QPushButton*>(elem);
        if (btn) {
            chem::Element const* curr_elem = chem::table.findBySym(btn->text().toStdString());
            btn->setToolTip(QString::fromStdString(curr_elem->name));
        }
    }

    // Disable elements that already exist in the document
    for (auto& elem: existing_elems){
        for (auto& i: ui->topelements->children()){
            QPushButton* btn = dynamic_cast<QPushButton*>(i);
            if (btn){
                if (btn->text().toStdString() == elem->symbol){
                    btn->setEnabled(false);
                    btn->setChecked(true);
                }
            }
        }
        for (auto& i: ui->bottomelements->children()){
            QPushButton* btn = dynamic_cast<QPushButton*>(i);
            if (btn){
                if (btn->text().toStdString() == elem->symbol){
                    btn->setEnabled(false);
                    btn->setChecked(true);
                }
            }
        }
    }
}


std::vector<chem::Element const*> ChooseElementDialog::getNewElements(QWidget* parent, std::vector<chem::Element const*> existing){
    ChooseElementDialog* diag = new ChooseElementDialog(parent, existing);
    diag->exec();
    auto retn = diag->selectedNewElements();
    return retn;
}


void ChooseElementDialog::accepted(){
    selectedNewElements();
    close();
}


void ChooseElementDialog::rejected(){
    close();
}



