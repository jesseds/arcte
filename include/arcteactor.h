/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include<vtkOpenGLActor.h>
#include<string>
#include<vtkProperty.h>
#include<vtkRenderer.h>
#include<vtkTexture.h>

class ArcteActor : public vtkOpenGLActor {

public:
    vtkTypeMacro(ArcteActor, vtkOpenGLActor);
    static ArcteActor* New();

    explicit ArcteActor();
    std::string kind {""};
    unsigned int kind_id = 0;
    unsigned int idx;

    virtual void ReleaseGraphicsResources(vtkWindow *window) {
                this->Device->ReleaseGraphicsResources(window);
                this->Superclass::ReleaseGraphicsResources(window);
            }

    virtual int RenderOpaqueGeometry(vtkViewport *viewport){
        if ( ! this->Mapper ) {
            return 0;
        }
        if (!this->Property) {
            this->GetProperty();
        }
        if (this->GetIsOpaque()) {
            vtkRenderer *ren = static_cast<vtkRenderer *>(viewport);
            this->Render(ren);
            return 1;
        }
        return 0;
    }

    virtual int RenderTranslucentPolygonalGeometry(vtkViewport *viewport){
        if ( ! this->Mapper ) {
          return 0;
        }
        if (!this->Property) {
          this->GetProperty();
        }
        if (!this->GetIsOpaque()) {
            vtkRenderer *ren = static_cast<vtkRenderer *>(viewport);
            this->Render(ren);
            return 1;
        }
        return 0;
    }

    virtual void Render(vtkRenderer *ren){
        this->Property->Render(this, ren);
        this->Device->SetProperty (this->Property);
        this->Property->Render(this, ren);
        if (this->BackfaceProperty) {
            this->BackfaceProperty->BackfaceRender(this, ren);
            this->Device->SetBackfaceProperty(this->BackfaceProperty);
        }
        if (this->Texture) {
            this->Texture->Render(ren);
        }
        this->ComputeMatrix();
        this->Device->SetUserMatrix(this->Matrix);
        this->Device->Render(ren,this->Mapper);
    }

    void ShallowCopy(vtkProp *prop) {
        ArcteActor *f = ArcteActor::SafeDownCast(prop);
        this->vtkOpenGLActor::ShallowCopy(prop);
    }

protected:
            vtkOpenGLActor* Device;
            ~ArcteActor() {
                this -> Device -> Delete();
            }
private:
    static unsigned int next_idx;
};

