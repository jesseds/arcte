/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once
#include <document.h>
#include <map>
#include <QTreeWidget>

using namespace Eigen;


namespace xrd {


class XrdPeak {
public:
    explicit XrdPeak(){};
    int h = 0;
    int k = 0;
    int l = 0;
    double g = 0;
    double I = 0;
    double theta = 0;
    double Fhkl_real {};
    double Fhkl_imag {};
    int m = 0;

    std::string label() const;
};


// Simulate a single wavelength xrd spectrum
class SingleSimulation : public QObject {
    Q_OBJECT

public:
    explicit SingleSimulation(Document const* doc);
//    void toDefault();

private:
    Document const* doc;

    double _wavelength = 1.5406;

    double _range_min = 10;
    double _range_max = 90;
    double _scale = 1;

    std::map<std::string, XrdPeak> _condensedPeaks {};

public:
    MatrixXi hkls {};
    Eigen::VectorXd g {};
    VectorXd intensity {};
    VectorXd theta {};
    Eigen::VectorXcd Fhkl {};

    double wavelength() const {return _wavelength;}

    double rangeMin() const {return _range_min;}
    double rangeMax() const {return _range_max;}
    double scale() const {return _scale;}

    unsigned int nHkls() const {return hkls.rows();}
    auto condensedPeaks() const {return _condensedPeaks;}

    // Compute the xrd spectrum
    void calculate();

public slots:
    bool setWavelength(double);

    bool setRangeMin(double);
    bool setRangeMax(double);
    void setScale(double val){_scale = val;}

signals:
    void recalculated();
    void wavelengthModified(double);

    void rangeMinModified(double);
    void rangeMaxModified(double);
};



// Simulate a multiple wavelength xrd spectrum
class MultiSimulation : public QObject {
    Q_OBJECT

private:
    std::vector<std::unique_ptr<SingleSimulation>> _sims {};
    double _range_min {};
    double _range_max {};
    std::vector<double> _scales {};
    Document const* doc;
    QTreeWidget* _refl_table;

    void updateTable();

public:
    explicit MultiSimulation(Document const* doc);


    SingleSimulation* addSim(double wavelength, double scale);
    auto rangeMin() const {return _range_min;}
    auto rangeMax() const {return _range_max;}

    void calculate();
    auto nSims() const {return _sims.size();}

    auto const& allSims() const {return _sims;}
    auto reflTable() const {return _refl_table;}

public slots:
    void setRangeMin(double val);
    void setRangeMax(double val);

signals:
    void calculated();
};

}
