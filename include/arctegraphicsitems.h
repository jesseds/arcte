/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <QGraphicsItem>
#include <QGraphicsEllipseItem>
#include <Eigen/Dense>
#include <QGraphicsView>
#include <plotter.h>

using Eigen::Vector3i;


class _QArcteGraphicsItem_Base : public QObject, public QGraphicsItem {
    Q_OBJECT

public:
    explicit _QArcteGraphicsItem_Base(ArcteGraphicsView* view = nullptr);
    void setScalable(bool val) {_scalable = val;}
    void setRotatable(bool val) {_rotatable = val;}
    void hoverEnterEvent(QGraphicsSceneHoverEvent* event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent* event);

    QString virtual descr() {return "";}

private:
    bool _scalable = true;
    bool _rotatable = true;

protected:
    bool _is_hovered = false;
    ArcteGraphicsView* _view;

private slots:
    void onViewScaleChanged(double scale);
    void onViewRotationChanged(double rot);

signals:
    void updateHoverText(QString);
};


class QGraphicsSpotItem : public _QArcteGraphicsItem_Base {

private:
    Eigen::Vector3i _hkl = Eigen::Vector3i::Zero();
    QString _hkl_text {};
    double _rad = 0;
    QColor _color {};
    QColor _label_color {};
    bool _show_label = false;

    int _text_width = 70;
    int _text_height = 20;

public:
    explicit QGraphicsSpotItem(ArcteGraphicsView* view);

    void setHkl(Vector3i const& hkl);
    auto hkl() const {return _hkl;}
    auto hklText() const {return _hkl_text;}

    void setColor(QColor const& col) {_color = col;}
    auto color() const {return _color;}

    void setRadius(double rad);
    auto radius() const {return _rad;}

    QRectF virtual boundingRect() const override;
    void virtual paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    QString descr() override;
};

class QGraphicsSpotLabel : public _QArcteGraphicsItem_Base {

private:
    Eigen::Vector3i _hkl = Eigen::Vector3i::Zero();
    QString _hkl_text {};
    double _rad = 0;
    QColor _color {};

    int _text_width = 70;
    int _text_height = 20;


public:
    explicit QGraphicsSpotLabel(ArcteGraphicsView* view);

    void setHkl(Vector3i const& hkl);
    auto hkl() const {return _hkl;}
    auto hklText() const {return _hkl_text;}

    void setColor(QColor const& col) {_color = col;}
    auto color() const {return _color;}

    void setRadius(double rad);
    auto radius() const {return _rad;}

    QRectF virtual boundingRect() const override;
    void virtual paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
};

class QGraphicsKikuchiItem : public _QArcteGraphicsItem_Base {

private:
    Eigen::Vector3i _hkl = Eigen::Vector3i::Zero();
    QString _hkl_text {};
    double _rad = 0;
    QColor _color {};
    QColor _label_color {};
    bool _show_label = false;


public:
    explicit QGraphicsKikuchiItem(ArcteGraphicsView* view);

    void setHkl(Vector3i const& hkl);
    auto hkl() const {return _hkl;}
    auto hklText() const {return _hkl_text;}

    void setColor(QColor const& col) {_color = col;}
    auto color() const {return _color;}

    QRectF virtual boundingRect() const override;
    void virtual paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
};
