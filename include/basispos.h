/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <elements.h>
#include <string>
#include <QMetaType>

enum AtomRadius {
    Atomic,
    VanDerWall,
    Covalent,
    Metallic,
    Manual
};

class BasisPos {

private:
    double _x {0};
    double _y {0};
    double _z {0};
    double _occ {1};
    chem::Element const* _species;
    std::string _label{""};
    std::string _type_name{""};
    double _radius {1};
    double _radius_scale {1};
    static unsigned int next_id;
    unsigned int _id {0};

    void setId();

public:
    explicit BasisPos();
    explicit BasisPos(const double& x, const double& y, const double& z, chem::Element const* species,
                      std::string const& label="", std::string const& type = "", double occupancy = 1);

    auto id() const {return _id;}
    auto x() const {return _x;}
    auto y() const {return _y;}
    auto z() const {return _z;}
    auto occupancy() const {return _occ;}
    auto const& label() const {return _label;}
    auto const& type() const {return _type_name;}
    auto radius() const {return _radius;}
    auto eff_radius() const {return _radius*_radius_scale;}
    chem::Element const* species() const {return _species;}

    void setX(double val);
    void setY(double val);
    void setZ(double val);
    void setOccupancy(double val);
    void setXYZ(double x, double y, double  z);
    void setLabel(std::string const& val);
    void setTypeName(std::string const& val);
    void setSpecies(chem::Element const* species);
    void interpretSpeciesFromString(std::string const& species);
    void setRadiusToAtomic();
    void setRadiusToCovalent();
    void setRadiusToVanDerWall();
    void setRadiusToMetallic();
    void setRadiusType(AtomRadius type);
    void setRadius(double val);
    void setRadiusScale(double val);
};

Q_DECLARE_METATYPE(BasisPos*)
