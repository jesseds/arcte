/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once
#include <QWidget>
#include <QVBoxLayout>
#include <QToolBar>
#include <QDir>
#include <QGraphicsView>


class ArcteGraphicsScene : public QGraphicsScene {
    Q_OBJECT

public:
    explicit ArcteGraphicsScene(){}

};


class ArcteGraphicsView : public QGraphicsView {
    Q_OBJECT

public:
    explicit ArcteGraphicsView(QGraphicsScene* scene);

    void scaleBy(double val);
    double currScale() const {return _scale;}
    double currRotation() const {return _rotation;}

public slots:
    void setRotation(double val);
    void setScale(double val);

protected:
    // Disable scrolling
    void wheelEvent(QWheelEvent*) override {};
    void keyPressEvent(QKeyEvent* e) override { QWidget::keyPressEvent(e);}
    void scrollContentsBy(int, int) override {}

private:
    double _rotation = 0;
    double _scale = 1;
    QTransform const _base_transform;

    void updateTransform();

signals:
    void scaleChanged(double scale);
    void rotationChanged(double rot);
};


// General plotting base class
class Plotter : public QWidget {
    Q_OBJECT

public:
    explicit Plotter(QWidget* parent,
                     bool attach_toolbar = false,
//                     Qt::ToolButtonStyle tool_btn_style = Qt::ToolButtonIconOnly
                     Qt::ToolButtonStyle tool_btn_style = Qt::ToolButtonTextUnderIcon
                     );

    QToolBar* toolbar;

private:
    QVBoxLayout* _layout;
    QDir _last_save_dir = QDir::home();

protected:
    void setPlotterWidget(QWidget* widget);
    QWidget* _plotter_widget = nullptr;

    QImage virtual getPlotAsImage();

protected slots:
    void onExportPng();
    void onCopyPng();
};


// Plotter for 2d plots
class Plotter2d : public Plotter {
    Q_OBJECT

public:
    explicit Plotter2d(QWidget* parent);

protected:
    ArcteGraphicsView* _view;
    ArcteGraphicsScene* _scene;
    QImage virtual getPlotAsImage() override;
};
