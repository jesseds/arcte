/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <QVTKOpenGLStereoWidget.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkCamera.h>
#include <QVBoxLayout>
#include <vtkOpenGLActor.h>
#include <vtkAxesActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkOrientationMarkerWidget.h>
#include <document.h>
#include <vtkCubeAxesActor.h>
#include <vtkOpenGLRenderer.h>
#include <QFrame>
#include <selectioninteractorstyle.h>
#include <defaultinteractorstyle.h>
#include <arcteactor.h>
#include <QFileInfo>
#include <vtkFollower.h>


class SceneWidget : public QWidget {
    Q_OBJECT

public:
    explicit SceneWidget(QWidget* parent, Document const* doc);
    vtkNew<DefaultInteractorStyle> default_interactor;
    vtkNew<SelectionInteractorStyle> picker_interactor;
    vtkSmartPointer<vtkCamera> camera;
    void registerActor(vtkSmartPointer<ArcteActor> actor);
    void registerFollower(vtkSmartPointer<vtkFollower> follower);
    void registerViewProp(vtkSmartPointer<vtkProp> prop);
    void removeViewProp(vtkSmartPointer<vtkProp> prop);
    void removeActor(vtkSmartPointer<ArcteActor> actor);
    void removeActorByKind(std::string const& kind, unsigned int id);
    void clearActors();
    void updateAxesTransform(Document const& strct);
    void resetCamera();
    vtkSmartPointer<vtkOpenGLRenderer> const renderer() const {return ren;}
    void setProjOrthographic(bool toggle);
    void setUseAA(bool onoff);
    void setViewType(std::string type);
    std::array<double, 3> backgroundColor();
    void setBackgroundColor(double r, double g, double b);
    void updateView();
    void pickActor();
    void toDefaultInteractorStyle();
    void toPickerInteractorStyle();
    void exportImage(QFileInfo&);
    void setUvw(int u, int v, int w);
    void roll(double);
    void yaw(double);
    void pitch(double);

    std::string rendering = "PBR";
    double roughness = 0.25;

private:
    Document const* doc;
    vtkSmartPointer<vtkGenericOpenGLRenderWindow> window;
    QVTKOpenGLStereoWidget* widget;
    vtkSmartPointer<vtkOpenGLRenderer> ren;

    vtkSmartPointer<vtkAxesActor> axesactor;
    vtkSmartPointer<vtkOrientationMarkerWidget> axes;
    std::string view_type {"Solid"};

signals:
    void actorSelected(unsigned int);
};

