/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once
#include <string>
#include <memory>
#include <vector>
#include <QMetaType>
#include <array>
#include <map>
#include <vtkLookupTable.h>
#include <QObject>


namespace chem {

enum ElementType {
    AlkaliMetal,
    AlkaliEarthMetal,
    Lanthanide,
    Actinide,
    TransitionMetal,
    PostTransitionMetal,
    Metalloid,
    NonMetal,
    NobleGas,
    Unknown
};

class Element {
    double _radii_atomic;
    double _radii_van_der_wall;
    double _radii_covalent;
    double _radii_metallic;
    std::array<double, 3> _color;
    double _elneg;
    double static constexpr default_radii {1};
    ElementType _type;

public:
    explicit Element(){};

    bool operator<(Element const& val) const {return number < val.number;}
    bool operator>(Element const& elem) const {return number > elem.number;}
    bool operator==(Element const& elem) const {return number == elem.number;}

    unsigned int number = 0;
    std::string symbol {};
    std::string name {};
    double debye_waller = 0;
    std::array<double, 3> color() const;
    std::array<double, 3> defaultColor() const {return _color;}

    auto radii_atomic() const {return _radii_atomic > 0 ? _radii_atomic : default_radii;}
    auto radii_van_der_wall() const {return _radii_van_der_wall > 0 ? _radii_van_der_wall : default_radii;}
    auto radii_covalent() const {return _radii_covalent > 0 ? _radii_covalent : default_radii;}
    auto radii_metallic() const {return _radii_metallic > 0 ? _radii_metallic : default_radii;}
    auto elneg() const {return _elneg;}
    auto elementType() const {return _type;}
    bool isMetallic() const;

    void setColor(std::array<double, 3> const& val);
    void setRadiiAtomic(double val){_radii_atomic = val;}
    void setRadiiVanDerWall(double val){_radii_van_der_wall = val;}
    void setRadiiCovalent(double val){_radii_covalent = val;}
    void setRadiiMetallic(double val){_radii_metallic = val;}
    void setElectronegativity(double val){_elneg = val;}
    void setElementType(ElementType type) {_type = type;}
};

class Table : public QObject {
    Q_OBJECT

private:
    void addElement(Element const& newelem);
    void load_data();
    std::vector<std::shared_ptr<Element const>> elements;

public:
    explicit Table();
    Element const* findBySym(std::string const& sym) const;
    Element const* findByName(std::string const& name) const;
    Element const* findByNumber(unsigned int name) const;
    std::vector<std::shared_ptr<Element const>> const& allElements() const {return elements;}
    std::vector<std::shared_ptr<Element const>>::const_iterator cbegin() const {return elements.cbegin();}
    std::vector<std::shared_ptr<Element const>>::const_iterator cend() const {return elements.cend();}
    std::vector<std::shared_ptr<Element const>>::const_iterator begin() const {return elements.cbegin();}
    std::vector<std::shared_ptr<Element const>>::const_iterator end() const {return elements.cend();}

    vtkSmartPointer<vtkLookupTable> lookupTable() const;
};

chem::Element const* str2symbol(std::string const& elem_str);
std::vector<std::string> split_element_symbols(std::string instring);

extern Table const table;
}

Q_DECLARE_METATYPE(chem::Element const*)
