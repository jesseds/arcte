/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once
#include <QTreeWidget>
#include <QVBoxLayout>
#include <QToolBar>
#include <QUrl>
#include <QStandardPaths>
#include <QDir>


class QTabulate : public QWidget {
    Q_OBJECT

public:
    explicit QTabulate(QWidget* parent = nullptr,
                       QTreeWidget* init_table = nullptr,
                       bool attach_toolbar = false,
//                       Qt::ToolButtonStyle tool_btn_style = Qt::ToolButtonIconOnly
                       Qt::ToolButtonStyle tool_btn_style = Qt::ToolButtonTextUnderIcon
                       );

    QToolBar* toolbar;

private:
    QVBoxLayout* _layout = new QVBoxLayout(this);
    QTreeWidget* _table;
    QDir _last_save_dir = QDir::home();

    QImage getTableAsImage();

private slots:
    void onExportCsv();
    void onExportPng();
    void onCopyPng();
};

