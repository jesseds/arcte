/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once

#include <vector>
#include <vtkSphereSource.h>
#include <vtkCylinderSource.h>
#include <vtkOpenGLActor.h>
#include <vtkSmartPointer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkAxesActor.h>
#include <vtkRenderer.h>
#include <array>
#include <vector>
#include <document.h>
#include <vtkCubeAxesActor.h>
#include <vtkTubeFilter.h>
#include <vtkLineSource.h>
#include <arcteactor.h>
#include <vtkCaptionActor2D.h>

typedef vtkSmartPointer<ArcteActor> actor_ptr;

namespace vtkfactory {

vtkSmartPointer<vtkPolyData> clean(vtkSmartPointer<vtkPolyData> poly);
vtkSmartPointer<vtkPolyData> poly_union (vtkSmartPointer<vtkPolyData> poly1, vtkSmartPointer<vtkPolyData> poly2, bool loop=false);
vtkSmartPointer<vtkSphereSource> sphere_source(double x, double y, double z, double radius, uint res_phi, uint res_theta);
actor_ptr sphere(double x, double y, double z, double radius, std::array<double, 3> const color,
                      uint res_phi, uint res_theta);

actor_ptr cylinder(double x1, double y1, double z1, double x2, double y2, double z2, double radius, unsigned int polys, std::array<double, 3> color={0.75, 0.75, 0.75}, bool cap=false);
actor_ptr unit_cell(Document const& doc, unsigned int width=1, std::array<unsigned int, 3> offset={0,0,0});
vtkSmartPointer<vtkTransform> cell_transform_normal(Document const& doc);
vtkSmartPointer<vtkTransform> cell_transform(Document const& doc);
std::vector<actor_ptr> doc_to_vtkactors(Document& doc);
std::vector<actor_ptr> stl_exportable_objects(Document const& doc);
vtkSmartPointer<vtkCaptionActor2D> caption_text(std::string text, std::array<double, 3> pos);
}





