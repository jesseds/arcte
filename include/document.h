/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#pragma once
#include <gemmi/symmetry.hpp>
#include <string>
#include <basispos.h>
#include <bond.h>
#include <Eigen/Dense>
#include <QObject>
#include <arcteactor.h>
#include <QFileInfo>
#include <vtkSmartPointer.h>

typedef chem::Element const* Element_ptr;

enum BondStyle {
    Solid,
    Dual
};


class Document : public QObject {
    Q_OBJECT

private:
    // Core params
    double _alpha;
    double _beta;
    double _gamma;
    double _a;
    double _b;
    double _c;
    double _radius_scale;
    unsigned int _nx;
    unsigned int _ny;
    unsigned int _nz;
    AtomRadius _radius_type;
    unsigned int _bond_polygons;
    double _bond_radius;
    unsigned int _atom_polygons;
    bool _atoms_visible;
    bool _bonds_visible;
    bool _unitcell_visible;
    std::string _descr;
    unsigned int _unitcell_linewidth;
    std::array<double, 3> _bond_color;
    gemmi::SpaceGroup const* _spg;
    std::vector<std::unique_ptr<BasisPos>> _bases {};
    std::vector<std::unique_ptr<Bond>> _bonds {};
    BondStyle _bond_style;

    QFileInfo _filepath {""};
    double bond_tol {0.325};

signals:
    void writableModified();

    // Structure params
    void aModified(double);
    void bModified(double);
    void cModified(double);
    void alphaModified(double);
    void betaModified(double);
    void gammaModified(double);
    void spaceGroupModified(QString);
    void nxModified(unsigned int);
    void nyModified(unsigned int);
    void nzModified(unsigned int);
    void descrModified(std::string);

    // Visualization params
    void atomsVisibleModified(bool);
    void bondsVisibleModified(bool);
    void unitCellVisibleModified(bool);
    void unitCellLineWidthModified(unsigned int);

    void atomPolygonsModified(unsigned int);
    void bondPolygonsModified(unsigned int);
    void bondRadiusModified(double);
    void bondColorModified(std::array<double, 3>);
    void radiusScaleModified(double);
    void radiusTypeModified(AtomRadius);
    void bondStyleModified(BondStyle);

    // Bonds, elements
    void elementRemoved(chem::Element const*);
    void basisPosAdded(BasisPos*);
    void basisPosRemovedId(unsigned int);
    void bondAdded(Bond*);
    void bondRemovedId(unsigned int);
    void basesCleared();
    void bondsCleared();

public slots:
    // Setters for document properties. Returns true if the value was changed,
    // returns false if the value was not changed (due to the new value being
    // equal to the old value).
    bool setAlpha(double val);
    bool setBeta(double val);
    bool setGamma(double val);
    bool setA(double val);
    bool setB(double val);
    bool setC(double val);
    bool setNx(unsigned int val);
    bool setNy(unsigned int val);
    bool setNz(unsigned int val);
    bool setSpaceGroup(gemmi::SpaceGroup const* spg);
    bool setSpaceGroupByName(std::string const& name);
    bool setAtomPolygons(unsigned int polys);
    bool setBondPolygons(unsigned int polys);
    bool setBondRadius(double radius);
    bool setRadiusScale(double scale);
    bool setBondColor(std::array<double, 3>);
    bool setRadiusType(AtomRadius);
    bool setAtomsVisible(bool val);
    bool setBondsVisible(bool val);
    bool setUnitCellVisible(bool val);
    bool setUnitCellLineWidth(unsigned int val);
    bool setBondStyle(BondStyle style);
    bool setDescr(std::string const& text);

    void addBasisPos(BasisPos const& newpos);
    void addBond(Bond const& newpos);
    void clearBases();
    void clearBonds();

    void toDefault();

public:
    explicit Document(){}

    bool suppress_view_update {false};

    auto alpha() const {return _alpha;}
    auto beta() const {return _beta;}
    auto gamma() const {return _gamma;}
    std::array<double, 3> params() const {return {_a, _b, _c};}
    std::array<double, 3> angles() const {return {_alpha, _beta, _gamma};}
    auto a() const {return _a;}
    auto b() const {return _b;}
    auto c() const {return _c;}
    auto nx() const {return _nx;}
    auto ny() const {return _ny;}
    auto nz() const {return _nz;}
    auto filepath() const {return _filepath;}
    auto radiusScale() const {return _radius_scale;}
    auto atomsVisible() const {return _atoms_visible;}
    auto bondsVisible() const {return _bonds_visible;}
    auto unitCellVisible() const {return _unitcell_visible;}
    auto unitCellLineWidth() const {return _unitcell_linewidth;}
    auto atomPolygons() const {return _atom_polygons;}
    auto radiusType() const {return _radius_type;}
    auto bondPolygons() const {return _bond_polygons;}
    auto bondRadius() const {return _bond_radius;}
    auto bondColor() const {return _bond_color;}
    auto itNumber() const {return _spg->number;}
    auto hermannMaugin() const {return _spg->xhm();}
    auto descr() const {return _descr;}
    auto bondStyle() const {return _bond_style;}
    auto const* spaceGroup() const {return _spg;}
    size_t nPositions() const {return _bases.size();}
    size_t nBonds() const {return _bonds.size();}
    auto const& basisPositions() const {return _bases;}
    auto const& bonds() const {return _bonds;}
    std::vector<chem::Element const*> elements() const;
    unsigned int nElemPos(chem::Element const*) const;
    auto nElements() {return elements().size();}
    std::string compStr() const;
    std::map<chem::Element const*, int> composition() const;
    unsigned int nSymEquivPos() const;

    BasisPos* findBasisPosById(unsigned int id) const;
    Bond* findBondById(unsigned int id) const;
    void removeBasisPosById(unsigned int id);
    void removeElement(chem::Element const* elem);
    void removeBondById(unsigned int id);

    std::array<double, 2> predictBondingRadius(chem::Element const* elem1, chem::Element const* elem2);
    bool basisHasWritableLabel() const;

    void setFilePath(QFileInfo const& fpath) {_filepath = fpath;}

    std::map<BasisPos const*, Eigen::MatrixXd> symEquivPos(bool include_boundary=false, bool tiled=false) const;
    std::map<BasisPos const*, Eigen::MatrixXd> centeredPos() const;

    void predictBonding();
};


std::vector<std::string> centeringString(gemmi::SpaceGroup const* spg);

