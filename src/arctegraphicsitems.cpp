/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "arctegraphicsitems.h"
#include "qstyleoption.h"
#include <QPen>
#include <QPainter>
#include <iostream>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <geometry.h>
#include <ciso646>
#include <QFontMetrics>


QColor static const HIGHLIGHT_COLOR = QColor("#3daee9");

_QArcteGraphicsItem_Base::_QArcteGraphicsItem_Base(ArcteGraphicsView* view) : QObject(), QGraphicsItem () {
    _view = view;
    setAcceptHoverEvents(true);

    if (_view != nullptr) {
        connect(_view, &ArcteGraphicsView::scaleChanged, this, &_QArcteGraphicsItem_Base::onViewScaleChanged);
        connect(_view, &ArcteGraphicsView::rotationChanged, this, &_QArcteGraphicsItem_Base::onViewRotationChanged);
    }
}


void _QArcteGraphicsItem_Base::onViewScaleChanged(double val) {
    if (not _scalable) {
        double newscale = 1 / val;
        setScale(newscale);
    }
}


void _QArcteGraphicsItem_Base::onViewRotationChanged(double rot) {
    if (not _rotatable) {
        double new_rot = -rot;
        setRotation(new_rot);
    }
}


void _QArcteGraphicsItem_Base::hoverEnterEvent(QGraphicsSceneHoverEvent* event) {
    emit updateHoverText(descr());
    QGraphicsItem::hoverEnterEvent(event);
    _is_hovered = true;
}


void _QArcteGraphicsItem_Base::hoverLeaveEvent(QGraphicsSceneHoverEvent* event) {
    emit updateHoverText("");
    QGraphicsItem::hoverLeaveEvent(event);
    _is_hovered = false;;
}


QGraphicsSpotItem::QGraphicsSpotItem(ArcteGraphicsView* view) : _QArcteGraphicsItem_Base(view) {
    setHkl(Vector3i::Zero());

    // Set to not rotate so that text labels do not rotate
    setRotatable(false);
    setRotation(-_view->currRotation());
}


void QGraphicsSpotItem::setHkl(Vector3i const& hkl) {
    _hkl = hkl;
//    auto space = "\u2009";
    auto space = " ";
    _hkl_text = QString::fromStdString(std::to_string(hkl(0)) + space + std::to_string(hkl(1)) + space + std::to_string(hkl(2)));
}


QString QGraphicsSpotItem::descr() {
    QString retn = "hkl = " + _hkl_text;
//    double x = pos().x();
//    double y = pos().y();
//    double r = std::sqrt(x*x + y*y);

//    retn += " g = " + QString::number(r, 'f', 4);
    return retn;
}

void QGraphicsSpotItem::setRadius(double val) {
    if (val < 0) {
        throw std::runtime_error("Spot radius cannot be negative");
    }

    _rad = val;
}


QRectF QGraphicsSpotItem::boundingRect() const {
    QRectF rect {-_rad, -_rad, _rad*2, _rad*2};
    return rect;
}


void QGraphicsSpotItem::paint(QPainter *painter, QStyleOptionGraphicsItem const* option, QWidget *widget) {
    painter->save();
    painter->setRenderHints(QPainter::Antialiasing);

    painter->setPen(Qt::NoPen);
    painter->setBrush(_is_hovered ? HIGHLIGHT_COLOR : _color);
    painter->drawEllipse(QPointF(0, 0), _rad, _rad);

    painter->restore();
}


QGraphicsSpotLabel::QGraphicsSpotLabel(ArcteGraphicsView* view) : _QArcteGraphicsItem_Base(view) {
    setHkl(Vector3i::Zero());

    // Set to not rotate so that text labels do not rotate
    setRotatable(false);
    setRotation(-_view->currRotation());
}

void QGraphicsSpotLabel::setHkl(Vector3i const& hkl) {
    _hkl = hkl;
//    auto space = "\u2009";
    auto space = " ";
    _hkl_text = QString::fromStdString(std::to_string(hkl(0)) + space + std::to_string(hkl(1)) + space + std::to_string(hkl(2)));
}


void QGraphicsSpotLabel::setRadius(double val) {
    if (val < 0) {
        throw std::runtime_error("Spot radius cannot be negative");
    }

    _rad = val;
}


QRectF QGraphicsSpotLabel::boundingRect() const {
//    QRectF spot_rect {-_rad, -_rad, _rad*2, _rad*2};
//    auto metrics = QFontMetrics(QFont());
//    int width = metrics.horizontalAdvance(_hkl_text);
//    int height = metrics.height();

//    QRectF =
    return QRect{};
}


void QGraphicsSpotLabel::paint(QPainter *painter, QStyleOptionGraphicsItem const* option, QWidget *widget) {
    // Draw hkl label that does not scale with zoom and stays anchored on the upper right
    // corner of the spot
    painter->save();

    QTransform trans = painter->transform();
    double scale = 1/trans.m11();

    QRectF txt_rect {};
    txt_rect.setLeft(1/scale*_rad*0.7071067811);
    txt_rect.setTop(-1/scale*_rad*0.7071067811 - _text_height);
    txt_rect.setWidth(_text_width);
    txt_rect.setHeight(_text_height);
    painter->scale(scale, scale);
    painter->setPen(_color);
    painter->drawText(txt_rect, Qt::AlignLeft | Qt::AlignBottom, hklText());
    painter->restore();
}























QGraphicsKikuchiItem::QGraphicsKikuchiItem(ArcteGraphicsView* view) : _QArcteGraphicsItem_Base(view) {
    setHkl(Vector3i::Zero());
    setScalable(false);

    // kikuchi line should not scale with zoom, set the initial line scale to the inverse of the view
    setScale(1/_view->currScale());
}


void QGraphicsKikuchiItem::setHkl(Vector3i const& hkl) {
    _hkl = hkl;
    _hkl_text = QString::fromStdString(std::to_string(hkl(0)) + std::to_string(hkl(1)) + std::to_string(hkl(2)));
}


QRectF QGraphicsKikuchiItem::boundingRect() const {
    QGraphicsView* view = scene()->views()[0];
    QRectF rect = view->mapToScene(view->viewport()->geometry()).boundingRect();
    QPointF cen = rect.center();
    double x = pos().x();
    double y = pos().y();
    rect.moveCenter(QPointF(cen.x()-x, cen.y()-y));
    return rect;
}


void QGraphicsKikuchiItem::paint(QPainter *painter, QStyleOptionGraphicsItem const* option, QWidget *widget) {
    painter->setRenderHints(QPainter::Antialiasing);

    // We do not want the line to change size with zoom, so get the size of a 1x1 square in view coordinates
    // in scene coordinates: the width or height of this box is the same as 1 px. This only works if the
    // scene is kept in 1:1 aspect ratio which it should always be
//    QPolygonF box1x1 = scene()->views()[0]->mapToScene(QRect(0, 0, 1, 1));
//    double px_width = box1x1.boundingRect().width();

    QPen pen {};
    pen.setColor(_color);
    pen.setWidthF(1);
    painter->setPen(pen);

    double y = pos().y();
    double x = pos().x();
    double b = 0;
    double angle = geom::rad2deg(std::atan(-y/x));

    // There is only 1 view in the scene
    QGraphicsView* view = scene()->views()[0];

    QRectF rect = view->mapToScene(view->viewport()->geometry()).boundingRect();

    QPointF cen = rect.center();
    rect.moveCenter(QPointF(cen.x()-x, cen.y()-y));

    // Draw length should at least span the whole view
    double draw_length {};
    if (rect.width() > rect.height()) {
        draw_length = rect.width();
    }
    else {
        draw_length = rect.height();
    }

    QLineF line {};

    // Non-vertical lines
    line.setP1(QPointF(0, 0));
    line.setP2(QPointF(1, 1));
    line.setLength(draw_length / scale());
    line.setAngle(angle+90);
    painter->drawLine(line);

    line.setAngle(angle + 270);
    painter->drawLine(line);

}
