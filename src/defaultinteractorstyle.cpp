/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <defaultinteractorstyle.h>
#include <vtkPropPicker.h>
#include <vtkRenderWindowInteractor.h>
#include <arcteactor.h>
#include <exception>
#include <vtkHardwareSelector.h>
#include <vtkMoleculeMapper.h>
#include <vtkMolecule.h>
#include <vtkSelection.h>
#include <ciso646>
#include <vtkIdTypeArray.h>


DefaultInteractorStyle::DefaultInteractorStyle() :
    vtkInteractorStyleTrackballCamera() {
    }

void DefaultInteractorStyle::OnLeftButtonUp() {
    GetDefaultRenderer()->SetUseFXAA(store_fxaa);
    vtkInteractorStyleTrackballCamera::OnLeftButtonUp();
}

void DefaultInteractorStyle::OnLeftButtonDown() {
    store_fxaa = GetDefaultRenderer()->GetUseFXAA();
    GetDefaultRenderer()->SetUseFXAA(false);
    vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
}

void DefaultInteractorStyle::OnRightButtonUp() {
    GetDefaultRenderer()->SetUseFXAA(store_fxaa);
    vtkInteractorStyleTrackballCamera::OnRightButtonUp();
}

void DefaultInteractorStyle::OnRightButtonDown() {
    GetDefaultRenderer()->SetUseFXAA(false);
    store_fxaa = GetDefaultRenderer()->GetUseFXAA();
    vtkInteractorStyleTrackballCamera::OnRightButtonDown();
}

vtkStandardNewMacro(DefaultInteractorStyle)
