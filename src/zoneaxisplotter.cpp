/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <zoneaxisplotter.h>
#include <QVBoxLayout>
#include <QGraphicsEllipseItem>
#include <QGraphicsTextItem>
#include <QPen>
#include <iostream>
#include <ciso646>
#include <helpers.h>
#include <arctegraphicsitems.h>
#include <geometry.h>


ZoneAxisPlotter::ZoneAxisPlotter(QWidget* parent, ediff::ZoneAxisPattern* pattern) : Plotter2d(parent), _pattern(pattern) {
    auto invert_col = new QAction("Invert", this);
    invert_col->setIcon(QIcon(":/icons/icons/contrast.svg"));
    invert_col->setCheckable(true);
    invert_col->setChecked(_inverted);

    _view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    connect(invert_col, &QAction::triggered, this, &ZoneAxisPlotter::onInvert);
    connect(pattern, &ediff::ZoneAxisPattern::calculationFinished, this, &ZoneAxisPlotter::rebuild);

    toolbar->addAction(invert_col);

    connect(this, &ZoneAxisPlotter::convSemiAngleChanged, this, &ZoneAxisPlotter::rebuild);
    connect(this, &ZoneAxisPlotter::gammaChanged, this, &ZoneAxisPlotter::rebuild);
    connect(this, &ZoneAxisPlotter::showSpotLabelsChanged, this, &ZoneAxisPlotter::rebuild);
    connect(this, &ZoneAxisPlotter::rotationChanged, this->_view, &ArcteGraphicsView::setRotation);
    connect(this, &ZoneAxisPlotter::hklLabelThresholdChanged, this, &ZoneAxisPlotter::rebuild);
    connect(this, &ZoneAxisPlotter::kikuchiThresholdChanged, this, &ZoneAxisPlotter::rebuild);
    connect(this, &ZoneAxisPlotter::showKikuchiLinesChanged, this, &ZoneAxisPlotter::rebuild);

    _view->setScale(_base_scale);
}

void ZoneAxisPlotter::initializeProperties() {
    setConvSemiAngle(0.02);
    setGamma(1);
    setShowSpotLabels(false);
    setShowLaueCircles(true);
    setRotation(0);
    setShowKikuchiLines(false);
    setKikuchiThreshold(0.1);
    setHklLabelThreshold(0.25);
}

void ZoneAxisPlotter::onInvert() {
    _inverted = not _inverted;
    rebuild();
}


void ZoneAxisPlotter::reCenter() {
    QRectF rect = _view->sceneRect();
    rect.moveCenter(QPointF(0, 0));
    _view->setSceneRect(rect);
}

void ZoneAxisPlotter::setCameraLength(double cl) {
    if (cl == _camera_length) {
        return;
    }
    double scale_factor = cl/_base_cl * _base_scale;
    _view->setScale(scale_factor);
}

QGraphicsSpotItem* ZoneAxisPlotter::makeSpot(Vector3i const& hkl, double gx, double gy, double I) {
    auto retn = new QGraphicsSpotItem(_view);

    double val = std::pow(I, _gamma);
    val = _inverted ? val : 1 - val;

    bool show_label = _show_spot_labels ? I > _hkl_label_thresh : false;

    retn->setHkl(hkl);
    retn->setColor(QColor::fromRgbF(val, val, val));
    double lbl_val = _inverted ? 1 : 0;
//    retn->setLabelColor(QColor::fromRgbF(lbl_val, lbl_val, lbl_val));
//    retn->setShowLabel(show_label);
    retn->setPos(gx, gy);
    retn->setRadius(_conv_semiangle);
    connect(retn, &QGraphicsSpotItem::updateHoverText, this, &ZoneAxisPlotter::hoverTextChanged);

    return retn;
}

QGraphicsSpotLabel* ZoneAxisPlotter::makeSpotLabel(Vector3i const& hkl, double gx, double gy) {
    auto retn = new QGraphicsSpotLabel(_view);
    retn->setHkl(hkl);
    retn->setColor(_inverted ? Qt::white : Qt::black);
    retn->setPos(gx, gy);
    retn->setRadius(_conv_semiangle);

    return retn;
}


QGraphicsKikuchiItem* ZoneAxisPlotter::makeKikuchiLine(Vector3i const& hkl, double gx, double gy, double I) {
    auto retn = new QGraphicsKikuchiItem(_view);

    double val = std::pow(I, _gamma);
    val = _inverted ? val : 1 - val;

    retn->setHkl(hkl);
    retn->setColor(QColor::fromRgbF(val, val, val));
    retn->setPos(gx, gy);

    return retn;
}


void ZoneAxisPlotter::setShowSpotLabels(bool val) {
    if (val == _show_spot_labels) {
        return;
    }
    _show_spot_labels = val;
    emit showSpotLabelsChanged(val);
}


void ZoneAxisPlotter::setHklLabelThreshold(double val) {
    if (geom::isZero(val - _hkl_label_thresh)) {
        return;
    }

    if (val < 0 or val > 1) {
        throw std::runtime_error("hkl threshold must be between 0 and 1");
    }

    _hkl_label_thresh = val;
    emit hklLabelThresholdChanged(val);
}


void ZoneAxisPlotter::setKikuchiThreshold(double val) {
    if (geom::isZero(val - _kikuchi_thresh)) {
        return;
    }

    if (val < 0 or val > 1) {
        throw std::runtime_error("kikuchi threshold must be between 0 and 1");
    }

    _kikuchi_thresh = val;
    emit kikuchiThresholdChanged(val);
}


void ZoneAxisPlotter::setShowLaueCircles(bool val) {
    if (val == _show_laue_circles) {
        return;
    }
    _show_laue_circles = val;
    emit showLaueCirclesChanged(val);
}


void ZoneAxisPlotter::setShowKikuchiLines(bool val) {
    if (val == _show_kikuchi_lines) {
        return;
    }
    _show_kikuchi_lines = val;
    emit showKikuchiLinesChanged(val);
}


void ZoneAxisPlotter::setRotation(double val) {
    if (help::isClose(val, _rotation)) {
        return;
    }
    _rotation = val;
    emit rotationChanged(val);
}


void ZoneAxisPlotter::rebuild() {
    clear();

    QBrush bkg {_inverted ? Qt::black : Qt::white};
    _view->setBackgroundBrush(bkg);

    // Diffraction spots and labels
    for (auto i=0; i<_pattern->nHkls(); ++i) {
        double x = _pattern->projected()(i, 0);
        double y = _pattern->projected()(i, 1);
        Vector3i hkl = _pattern->hkls().row(i).transpose();
        double I = _pattern->I()(i);

        auto spot = makeSpot(hkl, x, y, I);
        _scene->addItem(spot);

        if (_show_spot_labels and I >= _hkl_label_thresh) {
            auto label = makeSpotLabel(hkl, x, y);
            _scene->addItem(label);
        }
    }
    // Kikuchi lines

    if (_show_kikuchi_lines) {
        for (auto i=0; i<_pattern->nHkls(); ++i) {
            double x = _pattern->projected()(i, 0);
            double y = _pattern->projected()(i, 1);
            Vector3i hkl = _pattern->hkls().row(i).transpose();
            double I = _pattern->I()(i);
            int laue = _pattern->laue()(i);

            // Do not add if lower than threshold
            if (_kikuchi_thresh >= I){
                continue;
            }

            // Do not add HOLZ kikuchi lines
            if (laue > 0) {
                continue;
            }

            auto kikuchi = makeKikuchiLine(hkl, x/2, y/2, I);
            _scene->addItem(kikuchi);
        }
    }

    if (_show_spot_labels) {
        for (auto i=0; i<_pattern->nHkls(); ++i) {
            double x = _pattern->projected()(i, 0);
            double y = _pattern->projected()(i, 1);
            Vector3i hkl = _pattern->hkls().row(i).transpose();
            double I = _pattern->I()(i);

            if (I >= _hkl_label_thresh) {
                auto label = makeSpotLabel(hkl, x, y);
                _scene->addItem(label);
            }
        }
    }
    reCenter();
               }

void ZoneAxisPlotter::setConvSemiAngle(double val) {
    if (val < 0) {
        throw std::runtime_error("Convergence semi-angle cannot be negative");
    }

    if (help::isClose(val, _conv_semiangle)) {
        return;
    }
    _conv_semiangle = val;
    emit convSemiAngleChanged(val);
}

void ZoneAxisPlotter::setGamma(double val) {
    if (val < 0) {
        throw std::runtime_error("Gamma cannot be negative");
    }

    if (help::isClose(val, _gamma)) {
        return;
    }

    _gamma = val;
    emit gammaChanged(val);
}

void ZoneAxisPlotter::clear() {
    _scene->clear();
    auto spot = makeSpot(Vector3i::Zero(), 0, 0, 1);

    _scene->addItem(spot);
}
