/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <scenewidget.h>
#include <vtkOpenGLPolyDataMapper.h>
#include <vtkobjects.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkTextActor.h>
#include <vtkTextProperty.h>
#include <vtkCaptionActor2D.h>
#include <geometry.h>
#include <vtkMatrix4x4.h>
#include <vtkTransform.h>
#include <vtkProperty.h>
#include <Eigen/Dense>
#include <vtkTexture.h>
#include <vtkImageReader2Factory.h>
#include <vtkImageReader.h>
#include <QResource>
#include <ciso646>
#include <vtkWindowToImageFilter.h>
#include <vtkOpenGLTexture.h>
#include <vtkSSAOPass.h>
#include <vtkRenderStepsPass.h>
#include <vtkHDRReader.h>
#include <vtkQImageToImageSource.h>

void set_axis_label_params(vtkTextActor* actor) {
    actor->SetMinimumSize(15, 15);

    vtkSmartPointer<vtkTextProperty> txt = actor->GetTextProperty();
    txt->SetColor(0, 0, 0);
    txt->ShadowOff();
    txt->ItalicOff();
}

SceneWidget::SceneWidget(QWidget* parent, Document const* doc):
    QWidget(parent), doc(doc) {
    widget = new QVTKOpenGLStereoWidget(this);
    auto layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);

    setLayout(layout);
    auto top_widget = QWidget(this);
    auto sub_layout = new QHBoxLayout();
    sub_layout->setSpacing(0);
    sub_layout->setContentsMargins(0, 0, 0, 0);

    // Add line on the left for Windows since style overrides in QDockWidget remove the border
    #ifdef WIN32
    auto vline = new QWidget(this);
    vline->setFixedWidth(1);
    vline->setStyleSheet(QString("background-color: rgb(210, 210, 210);"));
    sub_layout->addWidget(vline);
    #endif

    sub_layout->addWidget(widget);
    layout->addLayout(sub_layout);

    // Add line on the bottom for Windows
    #ifdef WIN32
    auto hline = new QWidget(this);
    hline->setFixedHeight(1);
    hline->setStyleSheet(QString("background-color: rgb(210, 210, 210);"));
    layout->addWidget(hline);
    #endif

    window = vtkSmartPointer<vtkGenericOpenGLRenderWindow>::New();
    widget->setRenderWindow(window);

    ren = vtkSmartPointer<vtkOpenGLRenderer>::New();
    widget->renderWindow()->AddRenderer(ren);

    // Load environment image from qresources
    QResource img_res {":/images/environ_bw.jpg"};
    assert(img_res.isValid());
    auto qimg = new QImage(img_res.absoluteFilePath());
    vtkNew<vtkQImageToImageSource> vtkimg;
    vtkimg->SetQImage(qimg);
    vtkimg->Update();

    vtkNew<vtkOpenGLTexture> texture;
    texture->SetInputConnection(vtkimg->GetOutputPort());
    texture->SetQualityTo16Bit();
    texture->SetMipmap(true);
    texture->SetInterpolate(true);

    ren->UseImageBasedLightingOn();
    ren->SetEnvironmentTexture(texture);

    ren->SetBackground(1, 1, 1);
    camera = ren->MakeCamera();
    ren->SetActiveCamera(camera);
    camera->SetPosition(1, 0, 0);
    camera->SetViewUp(0, 0, 1);

    setProjOrthographic(true);
    picker_interactor->SetDefaultRenderer(ren);
    default_interactor->SetDefaultRenderer(ren);
    window->GetInteractor()->SetInteractorStyle(default_interactor);

    setUseAA(true);

    // Create the axes orientation widget
    axesactor = vtkSmartPointer<vtkAxesActor>::New();
    axes = vtkSmartPointer<vtkOrientationMarkerWidget>::New();

    axes->SetOutlineColor(0, 0, 0);
    axes->SetOrientationMarker(axesactor);
    axes->SetInteractor(window->GetInteractor());
    axes->SetDefaultRenderer(ren);
    axes->On();
    axes->SetViewport(0.0, 0, 0.15, 0.2);
    axes->InteractiveOff();
    axesactor->SetXAxisLabelText("a");
    axesactor->SetYAxisLabelText("b");
    axesactor->SetZAxisLabelText("c");
    vtkSmartPointer<vtkTextActor> x = axesactor->GetXAxisCaptionActor2D()->GetTextActor();
    vtkSmartPointer<vtkTextActor> y = axesactor->GetYAxisCaptionActor2D()->GetTextActor();
    vtkSmartPointer<vtkTextActor> z = axesactor->GetZAxisCaptionActor2D()->GetTextActor();
    set_axis_label_params(x);
    set_axis_label_params(y);
    set_axis_label_params(z);
    updateAxesTransform(*doc);
}

void SceneWidget::setViewType(std::string type) {
    view_type = type;
}

void SceneWidget::setUseAA(bool onoff){
    ren->SetUseFXAA(onoff);
    updateView();
}

void SceneWidget::registerViewProp(vtkSmartPointer<vtkProp> prop) {
    ren->AddViewProp(prop);
}

void SceneWidget::removeViewProp(vtkSmartPointer<vtkProp> prop) {
    ren->RemoveViewProp(prop);
}

void SceneWidget::registerFollower(vtkSmartPointer<vtkFollower> follower) {
    follower->SetCamera(ren->GetActiveCamera());
    ren->AddActor(follower);
}

void SceneWidget::registerActor(vtkSmartPointer<ArcteActor> actor) {
    if (view_type == "Solid"){
        actor->GetProperty()->SetRepresentationToSurface();
    }
    else if (view_type == "Solid wireframe"){
        actor->GetProperty()->SetRepresentationToSurface();
        actor->GetProperty()->EdgeVisibilityOn();
    }
    else if (view_type == "Wireframe"){
        actor->GetProperty()->SetRepresentationToWireframe();
    }
    else {
        throw std::invalid_argument("SceneWidget::registerActor() received invalid view representation");
    }

    if (rendering == "Illustrative") {
        actor->GetProperty()->SetInterpolationToPhong();
        ren->UseImageBasedLightingOff();
        actor->GetProperty()->SetAmbient(0.25);
        actor->GetProperty()->SetDiffuse(1);
        actor->GetProperty()->SetSpecular(0.1);
    }
    else if (rendering == "PBR") {
        actor->GetProperty()->SetInterpolationToPBR();
        ren->UseImageBasedLightingOn();
        actor->GetProperty()->SetMetallic(0);
        actor->GetProperty()->SetRoughness(roughness);
    }
    else {
        throw std::runtime_error("Expected \"PBR\" or \"Illustrative\" rendering modes");
    }

//    actor->GetProperty()->SetOcclusionStrength(1);
    ren->AddActor(actor);
}

void SceneWidget::removeActorByKind(const std::string &kind, unsigned int id) {
    vtkActorCollection* props = ren->GetActors();
    props->InitTraversal();
    for (int i=0; i<props->GetNumberOfItems(); i++) {
        vtkSmartPointer<vtkActor> curr_actor = props->GetNextActor();
        vtkSmartPointer<ArcteActor> actor = ArcteActor::SafeDownCast(curr_actor);
        if (! actor) {
            continue;
        }
        else if (actor->kind == kind && actor->kind_id == id) {
            removeActor(actor);
        }
    }
}

void SceneWidget::removeActor(vtkSmartPointer<ArcteActor> actor) {
    ren->RemoveActor(actor);
}

void SceneWidget::updateView(){
    camera->Render(ren);
    window->Render();
    update();
    ren->ResetCameraClippingRange();
}

std::array<double, 3> SceneWidget::backgroundColor() {
    std::array<double, 3> retn ;
    auto rgb = ren->GetBackground();
    retn = {rgb[0], rgb[1], rgb[2]};
    return retn;
}

void SceneWidget::exportImage(QFileInfo& file) {
    vtkNew<vtkWindowToImageFilter> output;
    output->SetInput(window);
}

void SceneWidget::setUvw(int u, int v, int w) {
    auto fp = camera->GetFocalPoint();
    Eigen::Vector3d fp2{};
    fp2 << fp[0], fp[1], fp[2];

    auto deortho = geom::deorthogonalization(*doc);
    Eigen::Vector3d uvw{};
    uvw << u, v, w;
    auto cam_pos = deortho*uvw;

    double cam_dist = cam_pos.norm();

    double current_cam_dist = camera->GetDistance();
    double dist_prop = current_cam_dist/cam_dist;

    auto cam_pos2 = cam_pos*dist_prop;
    auto cam_pos3 = cam_pos2 + fp2;

    if (u == 0 and v == 0 and w == 1) {
        camera->SetViewUp(1, 0, 0);
    }
    else {
        camera->SetViewUp(0, 0, 1);
    }
    camera->SetPosition(cam_pos3[0], cam_pos3[1], cam_pos3[2]);
    updateView();
}

void SceneWidget::clearActors() {
    ren->RemoveAllViewProps();
}

void SceneWidget::setBackgroundColor(double r, double g, double b) {
    ren->SetBackground(r, g, b);
    updateView();
}

void SceneWidget::setProjOrthographic(bool toggle) {
    renderer()->GetActiveCamera()->SetParallelProjection(toggle);
    window->Render();
    update();
}

void SceneWidget::resetCamera() {
    ren->ResetCamera();
    camera->OrthogonalizeViewUp();
    updateView();
}

void SceneWidget::pickActor() {
    window->GetInteractor()->SetInteractorStyle(picker_interactor);
}

void SceneWidget::toDefaultInteractorStyle() {
    window->GetInteractor()->SetInteractorStyle(default_interactor);
}

void SceneWidget::toPickerInteractorStyle() {
    window->GetInteractor()->SetInteractorStyle(picker_interactor);
}

//void SceneWidget::basisPicked(unsigned int idx, double x, double y, double z) {
//    toDefaultInteractorStyle();
//}

void SceneWidget::updateAxesTransform(Document const& doc) {
    /*
     * Update the axes indicator to transform and orient to the current crystal structure
     */
    vtkSmartPointer<vtkTransform> transform = vtkfactory::cell_transform_normal(doc);
    axesactor->SetUserTransform(transform);
}

void SceneWidget::roll(double angle) {
    camera->OrthogonalizeViewUp();
    camera->Roll(angle);
    resetCamera();
}

void SceneWidget::yaw(double angle) {
    camera->OrthogonalizeViewUp();
    camera->Azimuth(angle);
    resetCamera();
}

void SceneWidget::pitch(double angle) {
    camera->OrthogonalizeViewUp();
    camera->Pitch(angle);
    camera->OrthogonalizeViewUp();
    resetCamera();
}


