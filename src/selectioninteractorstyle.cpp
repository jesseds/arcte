/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <selectioninteractorstyle.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkPropPicker.h>
#include <vtkRenderWindowInteractor.h>
#include <arcteactor.h>
#include <exception>
#include <vtkHardwareSelector.h>
#include <vtkArcteMoleculeMapper.h>
#include <vtkMolecule.h>
#include <vtkSelection.h>
#include <ciso646>
#include <vtkIdTypeArray.h>
#include <vtkDataSetAttributes.h>


SelectionInteractorStyle::SelectionInteractorStyle() :
//    QObject(), vtkInteractorStyleTrackballActor() {
    vtkInteractorStyleTrackballActor() {
    sigHandler = new InteractorSignalHandler();

}

void SelectionInteractorStyle::OnLeftButtonUp() {
    // Forward events
    vtkInteractorStyleTrackballActor::OnLeftButtonUp();
    int* clickPos = this->GetInteractor()->GetEventPosition();

    vtkNew<vtkPropPicker> picker;

    picker->Pick(clickPos[0], clickPos[1], clickPos[2], this->GetDefaultRenderer());

    vtkSmartPointer<vtkProp3D> prop = picker->GetProp3D();

    if (not prop) {
        // Nothing selected
        return;
    }

    auto prop_actor = picker->GetActor();
    auto mol_mapper = vtkArcteMoleculeMapper::SafeDownCast(prop_actor->GetMapper());

    vtkSmartPointer<vtkActor> actor = picker->GetActor();
    vtkSmartPointer<ArcteActor>aactor = ArcteActor::SafeDownCast(actor);

    if (mol_mapper and prop)  {
        vtkNew<vtkHardwareSelector> selector;
        selector->SetRenderer(GetDefaultRenderer());
        selector->SetFieldAssociation(vtkDataObject::FIELD_ASSOCIATION_CELLS);
//        selector->SetFieldAssociation(vtkDataObject::FIELD_ASSOCIATION_POINTS | vtkDataObject::FIELD_ASSOCIATION_CELLS | vtkDataObject::FIELD_ASSOCIATION_EDGES | vtkDataObject::FIELD_ASSOCIATION_VERTICES);
//        selector->SetFieldAssociation(vtkDataObject::FIELD_ASSOCIATION_POINTS_THEN_CELLS);
        selector->SetArea(GetDefaultRenderer()->GetPickX(),
                          GetDefaultRenderer()->GetPickY(),
                          GetDefaultRenderer()->GetPickX(),
                          GetDefaultRenderer()->GetPickY());
        vtkSelection* selection = selector->Select();

        vtkNew<vtkIdTypeArray> atom_ids;
        mol_mapper->GetSelectedAtoms(selection, atom_ids);
        if (atom_ids->GetNumberOfValues() == 0) {
            return;

        }

        if (mol_mapper and prop and aactor) {
            auto atom_id = atom_ids->GetValue(0);

            vtkDataSetAttributes* mol_data = mol_mapper->GetInput()->GetAtomData();
            unsigned int basis_id = mol_data->GetArray("basis_id")->GetTuple1(atom_id);
            double x = mol_mapper->GetInput()->GetAtom(atom_id).GetPosition()[0];
            double y = mol_mapper->GetInput()->GetAtom(atom_id).GetPosition()[1];
            double z = mol_mapper->GetInput()->GetAtom(atom_id).GetPosition()[2];
//            sigHandler->emitBasisClicked(basis_id, x, y, z);
            emit sigHandler->basisClicked(basis_id, x, y, z);
        }
        selection->Delete();
    }
}

vtkStandardNewMacro(SelectionInteractorStyle)
