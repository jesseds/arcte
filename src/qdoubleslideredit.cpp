/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/
#include <qdoubleslideredit.h>
#include <QHBoxLayout>
#include <helpers.h>
#include <cmath>
#include <ciso646>


QDoubleSliderEdit::QDoubleSliderEdit(QWidget* parent) : QWidget(parent) {
    layout = new QHBoxLayout();
    layout->setContentsMargins(0, 0, 0, 0);
    setLayout(layout);
    _slider = new QSlider(this);
    _slider->setOrientation(Qt::Horizontal);

    _spin = new QDoubleSpinBox(this);
    _spin->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    _spin->setSingleStep(0);
    _spin->setButtonSymbols(QAbstractSpinBox::NoButtons);

    layout->addWidget(_slider);
    layout->addWidget(_spin);
    setMinimum(0);
    setMaximum(1);
    setDecimals(2);

    connect(_spin, &QDoubleSpinBox::editingFinished, this, &QDoubleSliderEdit::onSpinEditingFinished);
    connect(_slider, &QSlider::valueChanged, this, &QDoubleSliderEdit::onSliderValueChanged);
}

void QDoubleSliderEdit::setMaximum(double val) {
    _spin->setMaximum(val);
    updateTicks();
    syncSliderPosition();
}

void QDoubleSliderEdit::setMinimum(double val) {
    _spin->setMinimum(val);
    updateTicks();
    syncSliderPosition();
}

void QDoubleSliderEdit::setDecimals(int val) {
    _spin->setDecimals(val);
    updateTicks();
    syncSliderPosition();
}

void QDoubleSliderEdit::updateTicks() {
    int ticks = (_spin->maximum() - _spin->minimum()) * std::pow(10, _spin->decimals());
    _slider->setMaximum(ticks);
}

void QDoubleSliderEdit::syncSliderPosition() {
    setSliderByValue(_value);
}

void QDoubleSliderEdit::setSuffix(QString const& suff) {
    _spin->setSuffix(suff);
}

int QDoubleSliderEdit::nTicks() const {
    return _slider->maximum() - _slider->minimum();
}

void QDoubleSliderEdit::setValue(double val) {
    if (help::isClose(val, _value, 1.01*std::pow(10, -_spin->decimals()))) {
        return;
    }
    _value = val;

    syncSliderPosition();
    _spin->setValue(val);
    emit valueChanged(val);
}

void QDoubleSliderEdit::setSliderByValue(double val) {
    double frac = (val - _spin->minimum()) / (_spin->maximum() - _spin->minimum());
    double pos = std::floor(frac * static_cast<double>(nTicks()));
    int pos_int = static_cast<int>(pos);

    if (pos_int != _slider->value()) {
        _slider->setValue(pos_int);
    }
}

double QDoubleSliderEdit::sliderPos2Val(int pos) const {
    double spin_span = _spin->maximum() - _spin->minimum();
    double frac = static_cast<double>(pos - _slider->minimum()) / static_cast<double>(_slider->maximum() - _slider->minimum());
    return frac * spin_span + _spin->minimum();

}

double QDoubleSliderEdit::currSliderValue() const {
    return sliderPos2Val(_slider->value());
}

void QDoubleSliderEdit::onSpinEditingFinished() {
    setValue(_spin->value());
}

void QDoubleSliderEdit::onSliderValueChanged(int val) {
    auto slide_val = sliderPos2Val(val);
    setValue(slide_val);
}
