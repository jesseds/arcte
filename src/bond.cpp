/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "bond.h"

// id should never be 0, 0 is used for when actor_ptr has not set a kind_id
unsigned int Bond::next_id = 1;


Bond::Bond() {
    setId();
    setMaxRadius(1);
    setMinRadius(0.01);
}

void Bond::setMinRadius(double rad) {
    if (rad < 0.01) {
        throw std::invalid_argument("Bond::setMinRadius(): Minimum radius cannot be <= 0.");
    }
    else if (rad >= _max_rad) {
        throw std::invalid_argument("Bond::setMinRadius(): Minimum radius cannot be >= the max.");
    }
    else {
        _min_rad = rad;
    }
}

void Bond::setMaxRadius(double rad) {
    if (rad <= _min_rad) {
        throw std::invalid_argument("Bond::setMaxRadius(): Maximum radius cannot be <= the min.");
    }
    else {
        _max_rad = rad;
    }
}

void Bond::setId() {
    _id = Bond::next_id;
    Bond::next_id++;
}
