/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <io.h>
#include <gemmi/cif.hpp>
#include <gemmi/to_cif.hpp>
#include <gemmi/cifdoc.hpp>
#include <QFileInfo>
#include <iostream>
#include <document.h>
#include <vector>
#include <ciso646>
#include <vtkOpenGLPolyDataMapper.h>
#include <vtkSTLWriter.h>
#include <arcteactor.h>
#include <vtkLoopBooleanPolyDataFilter.h>
#include <vtkIntersectionPolyDataFilter.h>
#include <vtkAppendPolyData.h>
#include <vtkAlgorithmOutput.h>
#include <vtkCollisionDetectionFilter.h>
#include <vtkTransform.h>
#include <vtkCleanPolyData.h>
#include <vtkPPolyDataNormals.h>
#include <deque>
#include <vtkobjects.h>
#include <vtkFillHolesFilter.h>
#include <geometry.h>
#include <vtkMoleculeMapper.h>

using namespace gemmi;


bool io::actorIsBooleanable(vtkSmartPointer<vtkActor> actor) {
    auto aactor = ArcteActor::SafeDownCast(actor);
    if (aactor) {
        if (aactor->kind == "atom" or aactor->kind == "bond") {
            return true;
        }
    }
    return false;
}

int io::getFirstBooleanableIdx(vtkSmartPointer<vtkActorCollection> actors) {
    actors->InitTraversal();
    for (int i=0; i<actors->GetNumberOfItems(); ++i) {
        auto aactor = ArcteActor::SafeDownCast(actors->GetNextItem());
        if (aactor) {
            if (actorIsBooleanable(aactor)) {
                return i;
            }
        }
    }
    throw std::runtime_error("io::getFirstBooleanableIdx(): no booleanable actors found");
}

bool io::intersects(vtkSmartPointer<vtkPolyData> poly1, vtkSmartPointer<vtkPolyData> poly2) {

    auto intersect_filter = vtkSmartPointer<vtkCollisionDetectionFilter>::New();
    intersect_filter->SetCollisionModeToAllContacts();
    auto transform = vtkSmartPointer<vtkTransform>::New();
    auto matrix = vtkSmartPointer<vtkMatrix4x4>::New();

    intersect_filter->SetTransform(0, transform);
    intersect_filter->SetInputData(0, poly1);
    intersect_filter->SetInputData(1, poly2);
    intersect_filter->SetMatrix(1, matrix);
    intersect_filter->Update();
    return (intersect_filter->GetContactsOutput()->GetNumberOfPoints() > 0);
}

std::string filtered_hm_string(std::string hm){
    char chars[] = "' ";
    for (size_t i=0; i < strlen(chars); ++i){
        hm.erase(std::remove(hm.begin(), hm.end(), chars[i]), hm.end());
    }
    return hm;
}

void validate_pointer(std::string const * const value){
    if (value == nullptr){
        throw std::invalid_argument("io::validate_pointer(): Block does not exist in cif file");
    }
}

gemmi::SpaceGroup const* find_spacegroup(cif::Block const& block){
    // Symmetry data
    std::vector<std::string const*> names {
                block.find_value("_symmetry_space_group_name_H-M"),
                block.find_value("_space_group_name_H-M"),
                block.find_value("_symmetry_space_group_name_H-M_alt"),
                block.find_value("_space_group_name_H-M_alt"),
                block.find_value("_symmetry_space_group_name_Hall"),
                block.find_value("_space_group_name_Hall")
    };
    const std::string* it_num = block.find_value("_symmetry_space_group_IT_num");
    const std::string* it_num2 = block.find_value("_space_group_IT_num");

    for (auto& name: names){
        if (name){
            std::string name_filt = filtered_hm_string(*name);
            return gemmi::find_spacegroup_by_name(name_filt);
        }
    }

    if (it_num){
        std::string name_filt = filtered_hm_string(*it_num);
        return gemmi::find_spacegroup_by_number(std::stoi(name_filt));
    }
    else if (it_num2){
        std::string name_filt = filtered_hm_string(*it_num2);
        return gemmi::find_spacegroup_by_number(std::stoi(name_filt));
    }
    // If everything falls through use the least symmetric spacegroup
    return gemmi::find_spacegroup_by_number(1);
}


void io::load_cif_in_place(cif::Document const& cif_data, Document* doc){
//    if (! cifpath.exists()){
//        throw std::invalid_argument("io::load_cif_in_place(): The supplied file path does not exist or was deleted");
//    }
    doc->toDefault();
//    cif::Document cif_data = cif::read_file(cifpath.absoluteFilePath().toStdString());
    std::string src = cif_data.source;
//    doc->setFilePath(cifpath);
    doc->setFilePath(QFileInfo(QFile(QString::fromStdString(src))));

    // Lattice parameter and angle data
    cif::Block dat = cif_data.sole_block();
    const std::string* a = dat.find_value("_cell_length_a");
    const std::string* b = dat.find_value("_cell_length_b");
    const std::string* c = dat.find_value("_cell_length_c");
    const std::string* alpha = dat.find_value("_cell_angle_alpha");
    const std::string* beta = dat.find_value("_cell_angle_beta");
    const std::string* gamma = dat.find_value("_cell_angle_gamma");

    const std::string* descr = dat.find_value("_publ_section_title");

    // These are required
    validate_pointer(a);
    validate_pointer(b);
    validate_pointer(c);
    validate_pointer(alpha);
    validate_pointer(beta);
    validate_pointer(gamma);

    double a_num = stod(*a);
    double b_num = stod(*b);
    double c_num = stod(*c);
    double alpha_num = stod(*alpha);
    double beta_num = stod(*beta);
    double gamma_num = stod(*gamma);

    auto sg = find_spacegroup(dat);
    doc->setSpaceGroup(sg);

    // Basis positions
    cif::Table xyz = dat.find("_atom_site_", {"fract_x", "fract_y", "fract_z"});
    cif::Table labels = dat.find("_atom_site_", {"label"});
    cif::Table types = dat.find("_atom_site_", {"type_symbol"});
    cif::Table occ = dat.find("_atom_site_", {"occupancy"});

    // XYZ positions are required
    if (xyz.length() == 0){
        throw std::runtime_error("io::load_cif_in_place(): Unable to read x/y/z atom positions");
    }
    // Either atom labels or type names are required
    if (labels.length() == 0 and types.length() == 0) {
        throw std::runtime_error("io::load_cif_in_place(): Unable to read atom labels or type names");
    }

    doc->setA(a_num);
    doc->setB(b_num);
    doc->setC(c_num);
    doc->setAlpha(alpha_num);
    doc->setBeta(beta_num);
    doc->setGamma(gamma_num);
    doc->setDescr(descr == nullptr ? "" : *descr);

    for (auto i=0; i<xyz.length(); ++i){
        BasisPos newpos {};
        newpos.setX(stod(xyz[i][0]));
        newpos.setY(stod(xyz[i][1]));
        newpos.setZ(stod(xyz[i][2]));

        if (labels.length() != 0){
            newpos.setLabel(labels[i][0]);
        }

        if (types.length() != 0){
            newpos.setTypeName(types[i][0]);
        }

        if (occ.length() != 0) {
            newpos.setOccupancy(stod(occ[i][0]));
        }

        bool valid_element = false;
        std::exception exc;

        if (types.length() > 0){
            try {
                newpos.interpretSpeciesFromString(types[i][0]);
                valid_element = true;
            }
            catch (std::exception const& e) {exc = e;}
        }

        if (valid_element == false and labels.length() > 0) {
            newpos.interpretSpeciesFromString(labels[i][0]);
        }

        newpos.setRadiusType(doc->radiusType());
        doc->addBasisPos(newpos);
    }

    // Detect and add bonds
    doc->predictBonding();
}


cif::Document io::getCifDoc(QFileInfo const& path) {
    if (not path.exists()) {
        throw std::runtime_error("Cif path does not exist");
    }

    cif::Document retn;

    bool is_resource = path.absoluteFilePath().startsWith(":/");
    if (is_resource) {
        QFile file (path.absoluteFilePath());
        file.open(QIODevice::ReadOnly | QIODevice::Text);

        QString cif_data = file.readAll();
        retn = cif::read_string(cif_data.toStdString());
    }
    else {
        retn = cif::read_file(path.absoluteFilePath().toStdString());
    }

    return retn;
}


std::unique_ptr<Document> io::load_cif(QFileInfo const& cifpath){
    if (! cifpath.exists()){
        throw std::invalid_argument("io::load_cif(): The supplied file path does not exist or was deleted");
    }

    std::unique_ptr<Document> retn = std::make_unique<Document>();
    bool is_resource = cifpath.absoluteFilePath().startsWith(":/");

    cif::Document cifdoc;

    if (is_resource) {
        QFile file (cifpath.absoluteFilePath());
        file.open(QIODevice::ReadOnly | QIODevice::Text);

        QString cif_data = file.readAll();
        cifdoc = cif::read_string(cif_data.toStdString());
    }
    else {
        cifdoc = cif::read_file(cifpath.absoluteFilePath().toStdString());

    }

    io::load_cif_in_place(cifdoc, retn.get());
    return retn;
}


std::unique_ptr<Document> io::load_cif(std::string const& cifpath){
    QFileInfo fileinfo(QString::fromStdString(cifpath));
    return load_cif(fileinfo);
}


void io::export_cif(QFileInfo const& file, Document const& doc) {
     cif::Document cifdoc = cif::Document();
     cif::Block& block = cifdoc.add_new_block("");
     block.name = file.baseName().toStdString();
     block.set_pair("_cell_length_a", std::to_string(doc.a()));
     block.set_pair("_cell_length_b", std::to_string(doc.b()));
     block.set_pair("_cell_length_c", std::to_string(doc.c()));
     block.set_pair("_cell_angle_alpha", std::to_string(doc.alpha()));
     block.set_pair("_cell_angle_beta", std::to_string(doc.beta()));
     block.set_pair("_cell_angle_gamma", std::to_string(doc.gamma()));

     gemmi::SpaceGroup const* spg = doc.spaceGroup();
     block.set_pair("_space_group_IT_number", std::to_string(spg->number));
     block.set_pair("_symmetry_Int_Tables_number", std::to_string(spg->number));
     block.set_pair("_symmetry_space_group_name_H-M", "'" + spg->xhm() + "'");
     block.set_pair("_symmetry_space_group_name_Hall", "'" + std::string(spg->hall) + "'");
     block.set_pair("_cell_volume", std::to_string(geom::volume(doc)));
     block.set_pair("_chemical_formula_sum", doc.compStr());

     if (doc.nPositions() > 0) {
         if (doc.basisHasWritableLabel()) {
             auto& pos = block.init_loop("_atom_site", {"_label", "_type_symbol", "_fract_x", "_fract_y", "_fract_z", "_occupancy"});
             for (auto const& base: doc.basisPositions()) {
                 pos.add_row({base->label(),
                              base->species()->symbol,
                              std::to_string(base->x()),
                              std::to_string(base->y()),
                              std::to_string(base->z()),
                              std::to_string(base->occupancy())
                             });
             }
         }
         else {
             auto& pos = block.init_loop("_atom_site", {"_type_symbol", "_fract_x", "_fract_y", "_fract_z"});
             for (auto const& base: doc.basisPositions()) {
                 pos.add_row({//base->label(),
                              base->species()->symbol,
                              std::to_string(base->x()),
                              std::to_string(base->y()),
                              std::to_string(base->occupancy())
                             });
             }
         }
     }
     auto& wyckoff = block.init_loop("_symmetry", {"_equiv_pos_as_xyz"});
     for (auto const& op: spg->operations().sym_ops) {
         wyckoff.add_row({op.triplet()});
     }

     std::ofstream ostream {};
     ostream.open(file.absoluteFilePath().toStdString());
     cif::write_cif_to_stream(ostream, cifdoc, cif::Style::Indent35);
     ostream.close();
}

void io::export_union_stl(QFileInfo const& file, vtkSmartPointer<vtkActorCollection> actors, Document const* doc) {
    actors->InitTraversal();

    vtkSmartPointer<vtkPolyData> init1 = nullptr;
    vtkSmartPointer<vtkPolyData> init2 = nullptr;
    std::deque<vtkSmartPointer<vtkPolyData>> poly_atoms {};
    std::deque<vtkSmartPointer<vtkPolyData>> poly_other {};

    int first_idx = getFirstBooleanableIdx(actors);
    actors->InitTraversal();

    for (int i=first_idx; i<actors->GetNumberOfItems(); ++i) {
        vtkSmartPointer<vtkActor> curr_actor = actors->GetNextActor();
        auto mapper = vtkOpenGLPolyDataMapper::SafeDownCast(curr_actor->GetMapper());
        auto curr_data = mapper->GetInput();
        vtkSmartPointer<ArcteActor> aactor = ArcteActor::SafeDownCast(curr_actor);

        if (not aactor) {continue;}
        if (not actorIsBooleanable(aactor)) {continue;}

        std::string kind = aactor->kind;

        if (i == first_idx) {
            poly_atoms.push_back(curr_data);
            continue;
        }

        if (kind == "bond") {
            poly_other.push_back(curr_data);
            continue;
        }

        bool intersect_found = false;
        for (auto& i: poly_atoms) {
            if (intersects(i, curr_data)) {
                i = vtkfactory::poly_union(i, curr_data, false);
                intersect_found = true;
                break;
            }
        }

        if (intersect_found == false) {
            poly_atoms.push_back(curr_data);
        }
    }

    vtkSmartPointer<vtkPolyData> final_data = std::move(poly_atoms[0]);
    poly_atoms.erase(poly_atoms.begin());

    while (true) {
        bool intersect_found = false;
        for (size_t i=0; i<poly_atoms.size(); ++i) {
            if (intersects(final_data, poly_atoms[i])) {
                final_data = vtkfactory::poly_union(final_data, poly_atoms[i], false);
                poly_atoms.erase(poly_atoms.begin()+i);
                intersect_found = true;
                break;
            }
        }
        if (intersect_found == false) {
            break;
        }
    }

    auto retn = vtkSmartPointer<vtkPolyData>::New();
    auto append = vtkSmartPointer<vtkAppendPolyData>::New();
    append->SetInputData(0, final_data);
    for (auto& data: poly_atoms) {
        append->AddInputData(data);
    }
    append->Update();
    retn = append->GetOutput();

    for (size_t i=0; i<poly_other.size(); ++i) {
        retn = vtkfactory::poly_union(retn, poly_other[i], false);
    }

    auto fill = vtkSmartPointer<vtkFillHolesFilter>::New();
    fill->SetInputData(retn);
    fill->SetHoleSize(0.1);
    fill->Update();

    auto writer = vtkSmartPointer<vtkSTLWriter>::New();
    writer->SetFileName(file.absoluteFilePath().toStdString().data());
    writer->SetInputConnection(fill->GetOutputPort());
    writer->Write();
}


void io::export_append_stl(QFileInfo const& fileinfo, vector<vtkSmartPointer<ArcteActor>> actors) {
    auto append = vtkSmartPointer<vtkAppendPolyData>::New();
    for (auto& i: actors) {
        auto mapper = vtkOpenGLPolyDataMapper::SafeDownCast(i->GetMapper());
        append->AddInputData(mapper->GetInput());
    }
    append->Update();

    auto writer = vtkSmartPointer<vtkSTLWriter>::New();
    writer->SetFileName(fileinfo.absoluteFilePath().toStdString().data());
    writer->SetInputConnection(append->GetOutputPort());
    writer->Write();
}
