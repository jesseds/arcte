/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <xrdplotter.h>
#include <QtCharts/QLineSeries>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QValueAxis>
#include <QtCharts/QChart>
#include <QGraphicsLayout>
#include <ciso646>

XrdPlotter::XrdPlotter(QWidget* parent, xrd::MultiSimulation const* xrd_simm) :
    Plotter(parent),
    xrd_sim(xrd_simm) {
    connect(xrd_sim, &xrd::MultiSimulation::calculated, this, &XrdPlotter::onRedraw);

    _view = new ArcteChartView();
    setPlotterWidget(_view);

    chart = new QChart();
    chart->legend()->hide();
    chart->layout()->setContentsMargins(0, 0, 0, 0);
    _view->setChart(chart);
    chart->show();

    _view->setRenderHint(QPainter::Antialiasing);

    xaxis = new QValueAxis();
    yaxis = new QValueAxis();
    xaxis->setMinorTickCount(1);
    xaxis->setTickType(QValueAxis::TicksDynamic);
    yaxis->setTickType(QValueAxis::TicksDynamic);
    xaxis->setTitleText("2θ (degrees)");
    yaxis->setTitleText("Intensity");
    xaxis->setTickAnchor(0);
    yaxis->setTickAnchor(0);
    yaxis->setTickInterval(0.1);
    yaxis->setMin(0);
    yaxis->setMax(1.05);
    xaxis->setTickInterval(10);
    xaxis->setMin(0);
    xaxis->setMax(100);

    chart->addAxis(yaxis, Qt::AlignLeft);
    chart->addAxis(xaxis, Qt::AlignBottom);


    QPen pen1{};
    pen1.setColor(Qt::black);
    pen1.setWidth(2);

    QPen pen2{};
    pen2.setColor(QColor::fromRgb(239, 120, 0));
    pen2.setWidth(2);

    QPen pen3{};
    pen3.setColor(QColor::fromRgb(26, 202, 255));
    pen3.setWidth(2);

    _pens = {pen3, pen2, pen1};

    connect(_view, &ArcteChartView::resized, this, &XrdPlotter::onSetupLabels);
}

void ArcteChartView::resizeEvent(QResizeEvent *event) {
    QChartView::resizeEvent(event);
    emit resized();
}

void XrdPlotter::clear() {
    chart->removeAllSeries();
}


void XrdPlotter::updateAxes() {
    auto min = xrd_sim->rangeMin();
    auto max = xrd_sim->rangeMax();
    xaxis->setMin(min - min*0.05);
    xaxis->setMax(max + max*0.05);
}

void XrdPlotter::onRedraw() {
    clear();

    auto count = 0;
    for (auto it = xrd_sim->allSims().end()-1; it>=xrd_sim->allSims().begin(); --it) {
        auto sim = it->get();
//    for (auto const& sim: xrd_sim->allSims()) {
        if (sim->scale() < 1e-5) {
            count ++;
            continue;
        }

        for (auto const& [theta, peak]: sim->condensedPeaks()) {
            QLineSeries* series = new QLineSeries();

            series->setPen(_pens.at(count));
            if (peak.I < 1e-4) {
                continue;
            }
            series->append(peak.theta*2, 0);
            series->append(peak.theta*2, peak.I);
            chart->addSeries(series);
        }
        ++count;
    }

    updateAxes();

    for (auto& line: this->chart->series()) {
        line->attachAxis(xaxis);
        line->attachAxis(yaxis);
    }

    onSetupLabels();
}

void XrdPlotter::onShowLabelsChanged(bool checked) {
    show_labels = checked;
    onRedraw();
}

void XrdPlotter::onShowDSpacingChanged(bool checked) {
    show_d_spacing = checked;
    onRedraw();
}

void XrdPlotter::onShow2ThetaChanged(bool checked) {
    show_2theta = checked;
    onRedraw();
}

void XrdPlotter::onIntThresholdChanged(double val) {
    int_threshold = val;
    onRedraw();
}

void XrdPlotter::onSetupLabels() {
    for (auto& label: labels) {
        _view->scene()->removeItem(label);
    }
    labels.clear();

    if (not show_labels) {
        return;
    }

    auto count = 0;
    for (auto it = xrd_sim->allSims().end()-1; it>=xrd_sim->allSims().begin(); --it) {
        auto sim = it->get();
//    }
        if (sim->scale() < 1e-5) {
            ++count;
            continue;
        }
        for (auto const& [theta, peak]: sim->condensedPeaks()) {
            std::stringstream text {};
            text.clear();
            text << "<b>" << peak.label() << "</b>";

            if (peak.I < int_threshold) {
                continue;
            }

            if (show_d_spacing) {
                text << "<br>" << 1/peak.g << std::fixed << std::setprecision(3) << " Å\n";
            }

            if (show_2theta) {
                text << "<br>" << 2*peak.theta << std::fixed << std::setprecision(1) << " °";
            }

            QGraphicsTextItem* label = new QGraphicsTextItem{};
            label->setHtml(QString::fromStdString(text.str()));
            label->setDefaultTextColor(_pens[count].color());
            auto pos = chart->mapToPosition(QPointF(peak.theta*2, peak.I));

            // Fine tune position
            auto dr = 17;

            // Adjust the position if all 3 labels are being drawn to better center
            if (show_d_spacing and show_2theta) {
                dr += dr/3;
            }
            pos.setX(pos.x() - dr);
            pos.setY(pos.y() - dr);
            label->setRotation(-45);
            label->setPos(pos);
            chart->scene()->addItem(label);
            labels.push_back(label);
        }
        ++count;
    }
}
