/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/


#include <plotter.h>
#include <QPainter>
#include <QFileDialog>
#include <QApplication>
#include <QClipboard>
#include <iostream>
#include <helpers.h>


ArcteGraphicsView::ArcteGraphicsView(QGraphicsScene* scene) :
        QGraphicsView(scene),
        _base_transform(1, 0, 0, 0, 1, 0, 0, 0, 1) {
    setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    setCacheMode(QGraphicsView::CacheBackground);
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    setFrameStyle(QFrame::NoFrame);
}

void ArcteGraphicsView::setRotation(double val) {
    if (help::isClose(val, _rotation)) {
        return;
    }
    _rotation = val;
    updateTransform();

    emit rotationChanged(_rotation);
}

void ArcteGraphicsView::setScale(double val) {
    if (help::isClose(val, _scale)) {
        return;
    }

    _scale = val;
    updateTransform();
    emit scaleChanged(val);
}

void ArcteGraphicsView::updateTransform() {
    auto new_transform = QTransform(_base_transform);
    new_transform.scale(_scale, _scale);
    new_transform.rotate(_rotation);
    setTransform(new_transform);
}

Plotter::Plotter(QWidget* parent,
                 bool attach_toolbar,
                 Qt::ToolButtonStyle tool_btn_style
                 )
        : QWidget(parent) {
    _layout = new QVBoxLayout(this);
    toolbar = new QToolBar(this);
    _layout->setContentsMargins(0, 0, 0, 0);
    toolbar->setToolButtonStyle(tool_btn_style);
    toolbar->setIconSize(QSize(22, 22));
    toolbar->setMovable(false);

    if (attach_toolbar == true) {
        _layout->addWidget(toolbar);
    }

    auto export_png = new QAction("Save plot", this);
    export_png->setIcon(QIcon(":/icons/icons/camera22.svg"));
    connect(export_png, &QAction::triggered, this, &Plotter2d::onExportPng);


    auto copy_png = new QAction("Copy plot", this);
    copy_png->setIcon(QIcon(":/icons/icons/copy22.svg"));
    connect(copy_png, &QAction::triggered, this, &Plotter2d::onCopyPng);

    toolbar->addAction(export_png);
    toolbar->addAction(copy_png);
}


QImage Plotter::getPlotAsImage() {
    QSize size = _plotter_widget->size();
    QImage img {size, QImage::Format::Format_ARGB32};
    QPainter painter(&img);
    _plotter_widget->render(&painter);
    return img;
}


void Plotter::onExportPng() {
    QUrl url = QFileDialog::getSaveFileUrl(this, "Save image", QUrl::fromLocalFile(_last_save_dir.path()), "Portable network graphics (*.png)");
    if (url.isValid()) {
        QImage img = getPlotAsImage();
        img.save(url.toLocalFile());
    }
}

void Plotter::onCopyPng() {
    auto img = getPlotAsImage();
    auto clipboard = QApplication::clipboard();
    clipboard->setImage(img);
}


void Plotter::setPlotterWidget(QWidget* widget) {
    _plotter_widget = widget;
    _layout->addWidget(_plotter_widget);
}


QImage Plotter2d::getPlotAsImage() {
    QSize size = _plotter_widget->size();
    QImage img {size, QImage::Format::Format_ARGB32};
    QPainter painter(&img);
    QRectF rect {};
    rect.setWidth(size.width());
    rect.setHeight(size.height());
    rect.moveCenter(QPointF(0, 0));
    _view->render(&painter);
    return img;
}


Plotter2d::Plotter2d(QWidget* parent) : Plotter(parent) {
    _scene = new ArcteGraphicsScene();
    _view = new ArcteGraphicsView(_scene);
    setPlotterWidget(_view);
}


