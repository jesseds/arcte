/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <qtabulate.h>
#include <iostream>
#include <QFileDialog>
#include <QIODevice>
#include <QTextStream>
#include <geometry.h>
#include <QMessageBox>
#include <QScrollBar>
#include <QPainter>
#include <QApplication>
#include <QClipboard>
#include <ciso646>

QTabulate::QTabulate(QWidget* parent,
                     QTreeWidget* init_table,
                     bool attach_toolbar,
                     Qt::ToolButtonStyle tool_btn_style
                     )
        : QWidget(parent), _table(init_table) {
    _layout->setContentsMargins(0, 0, 0, 0);

    setLayout(_layout);

    // Setup toolbar
    toolbar = new QToolBar(this);
    toolbar->setToolButtonStyle(tool_btn_style);
    toolbar->setMovable(false);

    int icon_size = 22;
    toolbar->setIconSize(QSize(icon_size, icon_size));

    auto export_csv = new QAction("Save table", this);
    export_csv->setIcon(QIcon(":/icons/icons/table22.svg"));
    connect(export_csv, &QAction::triggered, this, &QTabulate::onExportCsv);

    auto export_png = new QAction("Save table image", this);
    export_png->setIcon(QIcon(":/icons/icons/camera22.svg"));
    connect(export_png, &QAction::triggered, this, &QTabulate::onExportPng);

    auto copy_png = new QAction("Copy table image", this);
    copy_png->setIcon(QIcon(":/icons/icons/copy22.svg"));
    connect(copy_png, &QAction::triggered, this, &QTabulate::onCopyPng);

    toolbar->addAction(export_csv);
    toolbar->addAction(export_png);
    toolbar->addAction(copy_png);

    if (attach_toolbar == true) {
        _layout->addWidget(toolbar);
    }

    _layout->addWidget(_table);
}

void QTabulate::onCopyPng() {
    auto img = getTableAsImage();
    auto clipboard = QApplication::clipboard();
    clipboard->setImage(img);
}


QImage QTabulate::getTableAsImage() {
    QSize size = _table->size();//{_table->verticalScrollBar()->maximum(), _table->horizontalScrollBar()->maximum()};
    QImage img {size, QImage::Format::Format_ARGB32};
    QPainter painter(&img);
    _table->render(&painter);
    return img;
}


void QTabulate::onExportPng() {
    QUrl url = QFileDialog::getSaveFileUrl(this, "Save table image", QUrl::fromLocalFile(_last_save_dir.path()), "Portable network graphics (*.png)");
    if (url.isValid()) {
        QImage img = getTableAsImage();
        img.save(url.toLocalFile());
    }
}

void QTabulate::onExportCsv() {
    try {
    QUrl url = QFileDialog::getSaveFileUrl(this, "Save table csv", QUrl::fromLocalFile(_last_save_dir.path()), "Comma separated values (*.csv)");
    if (url.isValid()) {
        QFile file = url.toLocalFile();

        if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QTextStream stream(&file);

            // Write header leabels
            for (int i=0; i<_table->model()->columnCount(); ++i) {
                char sep = i<_table->model()->columnCount()-1 ? ',' : '\n';
                stream << _table->model()->headerData(i, Qt::Horizontal).toString() << sep;
            }

            // Write table data
            for (int i=0; i<_table->topLevelItemCount(); ++ i) {
                auto row = _table->topLevelItem(i);

                for (int j=0; j<row->columnCount(); ++j) {
                    char sep = j<row->columnCount()-1 ? ',' : '\n';
                    auto disp_val = row->data(j, Qt::DisplayRole);
                    auto user_val = row->data(j, Qt::UserRole);

                    // No user data provided, write display data
                    if (not user_val.isValid()) {
                        stream << disp_val.toString();

                    }
                    // User data is provided
                    // Convert numbers close to zero to exactly zero
                    else {
                        if (user_val.canConvert<double>()) {
                            stream << (geom::isZero(user_val.toDouble()) ? "0" : user_val.toString());
                        }
                        else {
                            stream << user_val.toString();

                        }
                    }
                    stream << sep;

                }

            }
        }
        else {
            QMessageBox::warning(this, "Cannot write to file", "Could not write to file");
        }

        _last_save_dir = QDir(QFileInfo(file).dir());
    }
    }
    catch (std::exception) {
        QMessageBox::warning(this, "Writing table failed", "Writing table to file failed");
    }
}
