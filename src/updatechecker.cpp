/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include "updatechecker.h"
#include <QUrl>
#include <iostream>
#include <QStandardPaths>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
//#include <QTextCodec>
#include <QApplication>
#include <array>
#include <QMessageBox>
#include <QIcon>
#include <QPixmap>
#include <QPushButton>
#include <QDesktopServices>
#include <ciso646>


void UpdateChecker::run() {
    QUrl url {"https://gitlab.com/jesseds/arcte_meta/-/raw/master/newest_version"};


    QNetworkAccessManager* control = new QNetworkAccessManager();
    QNetworkRequest request(url);
    connect(control, &QNetworkAccessManager::finished, this, &UpdateChecker::downloadFinished);
    control->get(request);
}

void UpdateChecker::downloadFinished(QNetworkReply* reply) {
    if (reply->error() != QNetworkReply::NoError) {
    }

    data = reply->readAll();

    if (data.isNull() or data.isEmpty()) {
        if (show_connection_error) {
            std::string msg {"Failed to check for updates, check internet connection"};
            QMessageBox::warning(nullptr, "Failed to check for updates ", QString::fromStdString(msg));
        }
        return;
    }

    QString data_str = QString::fromStdString(data.toStdString());
    QStringList curr_version = QApplication::applicationVersion().split(".");
    QStringList newest_version = data_str.split(".");

    std::array<int, 3> currver = {
        std::stoi(curr_version[0].toStdString()),
        std::stoi(curr_version[1].toStdString()),
        std::stoi(curr_version[2].toStdString())
    };

    std::array<int, 3> newver = {
        std::stoi(newest_version[0].toStdString()),
        std::stoi(newest_version[1].toStdString()),
        std::stoi(newest_version[2].toStdString())
    };

    bool update_available {false};

    if (currver[0] < newver[0]) {
        update_available = true;
    }
    else if (currver[0] == newver[0]) {
        if (currver[1] < newver[1]) {
            update_available = true;
        }
        else if (currver[1] == newver[1]) {
            if (currver[2] < newver[2]) {
                update_available = true;
            }
        }
    }

    if (update_available) {
        showUpdateDialog(QApplication::applicationVersion(), data_str);
    }
    else {
        if (show_no_update_message) {
            QMessageBox::information(nullptr, "No update available", "Arcte is currently up-to-date");
        }
    }
}

void UpdateChecker::showUpdateDialog(QString const& oldver, QString const& newver) {
    QMessageBox* msg_win = new QMessageBox(nullptr);
    QPixmap img = QPixmap(":/icons/icons/update.svg");
    msg_win->setIconPixmap(QPixmap(":/icons/icons/update.png"));

    auto btn = msg_win->addButton(QMessageBox::Ok);
    msg_win->setEscapeButton(btn);

    QAbstractButton* download_btn = msg_win->addButton("Download", QMessageBox::NoRole);
    connect(download_btn, &QPushButton::clicked, this, &UpdateChecker::onDownloadBtnClicked);


    QString msg {"An update from version " + oldver + " to " + newver + " is available."};
    msg_win->setWindowTitle("Update available");
    msg_win->setText(msg);
    msg_win->resize(500, 200);

    msg_win->exec();
}

void UpdateChecker::onDownloadBtnClicked() {
    QDesktopServices::openUrl(QString("https://gitlab.com/jesseds/arcte/-/releases"));
}
