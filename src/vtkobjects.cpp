﻿/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <vtkobjects.h>
#include <vtkProperty.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkTextActor.h>
#include <vtkCaptionActor2D.h>
#include <vtkTextProperty.h>
#include <geometry.h>
#include <vtkTransform.h>
#include <vtkOutlineSource.h>
#include <vtkTransformPolyDataFilter.h>
#include <Eigen/Dense>
#include <ciso646>
#include <math.h>
#include <vtkAppendPolyData.h>
#include <vtkPolyDataNormals.h>
#include <vtkTriangleFilter.h>
#include <vtkCleanPolyData.h>
#include <vtkBooleanOperationPolyDataFilter.h>
#include <vtkLoopBooleanPolyDataFilter.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkFillHolesFilter.h>
#include <array>
#include <vtkOpenGLPolyDataMapper.h>
#include <vtkMolecule.h>
#include <vtkGlyph3D.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkLookupTable.h>
#include <elements.h>
#include <vtkArcteMoleculeMapper.h>


vtkSmartPointer<vtkPolyData> vtkfactory::clean(vtkSmartPointer<vtkPolyData> poly) {
    vtkNew<vtkCleanPolyData> clean_poly;
    clean_poly->PointMergingOn();
    clean_poly->ConvertStripsToPolysOn();
    clean_poly->SetInputData(poly);
    clean_poly->Update();

    vtkNew<vtkPolyDataNormals> normals_poly;
    normals_poly->SetInputConnection(clean_poly->GetOutputPort());
    normals_poly->Update();

    return normals_poly->GetOutput();
}

vtkSmartPointer<vtkPolyData> vtkfactory::poly_union (vtkSmartPointer<vtkPolyData> poly1, vtkSmartPointer<vtkPolyData> poly2, bool loop) {
    // Loop boolean is notably faster with less possible mesh artifacts, however, it does not work in some cases
    if (loop == true) {
        vtkNew<vtkLoopBooleanPolyDataFilter> boolean;
        boolean->NoIntersectionOutputOn();
        boolean->SetNoIntersectionOutput(3);
        boolean->SetInputData(0, poly1);
        boolean->SetInputData(1, poly2);
        boolean->Update();
        return vtkfactory::clean(boolean->GetOutput());
    }
    else {
        vtkNew<vtkBooleanOperationPolyDataFilter> boolean;
        boolean->SetOperationToUnion();
        boolean->SetInputData(0, poly1);
        boolean->SetInputData(1, poly2);
        boolean->Update();
        return vtkfactory::clean(boolean->GetOutput());
    }
}

vtkSmartPointer<vtkSphereSource> vtkfactory::sphere_source(double x, double y, double z, double radius, uint res_phi, uint res_theta) {
    vtkNew<vtkSphereSource> sphere;
    sphere->SetCenter(x, y, z);
    sphere->SetRadius(radius);
    sphere->SetPhiResolution(res_phi);
    sphere->SetThetaResolution(res_theta);
    sphere->Update();

    return sphere;
}

actor_ptr vtkfactory::sphere(double x, double y, double z, double radius, std::array<double, 3> color, uint res_phi, uint res_theta) {
    auto sphere = sphere_source(x, y, z, radius, res_phi, res_theta);

    vtkNew<vtkOpenGLPolyDataMapper> mapper;

    mapper->SetInputData(sphere->GetOutput());
    vtkNew<ArcteActor> actor;
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(color.data());

    return actor;
}

actor_ptr vtkfactory::cylinder(double x1, double y1, double z1, double x2, double y2, double z2, double radius, unsigned int polys, std::array<double, 3> color, bool cap) {
    vtkNew<vtkLineSource> line;
    line->SetPoint1(x1, y1, z1);
    line->SetPoint2(x2, y2, z2);

    vtkNew<vtkTubeFilter> tube;
    tube->SetInputConnection(line->GetOutputPort());
    tube->SetRadius(radius);
    tube->SetCapping(cap);
    tube->SetNumberOfSides(polys);
    tube->Update();

    vtkNew<vtkTriangleFilter> tris;
    tris->SetInputConnection(tube->GetOutputPort());
    tris->Update();

    vtkNew<vtkCleanPolyData> clean_poly;
    clean_poly->SetInputConnection(tris->GetOutputPort());
    clean_poly->Update();

    vtkNew<vtkPolyDataNormals> normals_poly;
    normals_poly->SetInputConnection(clean_poly->GetOutputPort());
    normals_poly->Update();

    vtkNew<vtkOpenGLPolyDataMapper> mapper;
    mapper->SetInputData(normals_poly->GetOutput());

    vtkNew<ArcteActor> actor;
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(color.data());
    return actor;
}

actor_ptr vtkfactory::unit_cell(Document const& doc, unsigned int ucell_width, std::array<unsigned int, 3> offset){
    double color[] = {0, 0, 0};
    auto transform = vtkfactory::cell_transform(doc);

    vtkNew<vtkOutlineSource> box;
    int dx = offset[0];
    int dy = offset[1];
    int dz = offset[2];
    box->SetBounds(0+dx, 1+dx, 0+dy, 1+dy, 0+dz, 1+dz);
    vtkNew<vtkTransformPolyDataFilter> poly_transform;
    poly_transform->SetTransform(transform);
    poly_transform->SetInputConnection(box->GetOutputPort());

    vtkNew<vtkOpenGLPolyDataMapper> mapper;
    vtkNew<ArcteActor> actor;

    mapper->SetInputConnection(poly_transform->GetOutputPort());
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(color);
    actor->GetProperty()->SetLineWidth(ucell_width);

    return actor;
}

vtkSmartPointer<vtkTransform> vtkfactory::cell_transform_normal(Document const& strct){
    double alpha = strct.alpha();
    double beta = strct.beta();
    double gamma = strct.gamma();
    Eigen::Matrix3d deortho = geom::deorthogonalization(1, 1, 1, alpha, beta, gamma);

    vtkNew<vtkTransform> transform;

    vtkNew<vtkMatrix4x4> matrix;
    matrix->Identity();

    for (int i=0; i<3; ++i){
        for (int j=0; j<3; ++j){
            matrix->SetElement(i, j, deortho(i, j));
        }
    }
    transform->SetMatrix(matrix);
    return transform;
}

vtkSmartPointer<vtkTransform> vtkfactory::cell_transform(Document const& strct){
    double alpha = strct.alpha();
    double beta = strct.beta();
    double gamma = strct.gamma();
    double a = strct.a();
    double b = strct.b();
    double c = strct.c();

    Eigen::Matrix3d deortho = geom::deorthogonalization(a, b, c, alpha, beta, gamma);

    vtkNew<vtkTransform> transform;

    vtkNew<vtkMatrix4x4> matrix;
    matrix->Identity();

    for (int i=0; i<3; ++i){
        for (int j=0; j<3; ++j){
            matrix->SetElement(i, j, deortho(i, j));
        }
    }
    transform->SetMatrix(matrix);
    return transform;
}

std::vector<actor_ptr> vtkfactory::doc_to_vtkactors(Document& doc) {
    std::vector<vtkSmartPointer<ArcteActor>> retn;
    double radscale = doc.radiusScale();
    std::map<BasisPos const*, Eigen::MatrixXd> sym_pos = doc.symEquivPos(true, true);
    auto deortho = geom::deorthogonalization(doc);

    double x, y, z, xt, yt, zt;

    // --------------------- Atoms -------------------------
    vtkNew<vtkMolecule> molecule;

    vtkNew<vtkArcteMoleculeMapper> mol_mapper;

    vtkNew<vtkDoubleArray> mol_radii;
    mol_radii->SetName("radii");
    mol_radii->SetNumberOfComponents(1);

    vtkNew<vtkIntArray> mol_scalar;
    mol_scalar->SetName("scalars");
    mol_scalar->SetNumberOfComponents(1);

    vtkNew<vtkDoubleArray> mol_color;
    mol_color->SetName("color");
    mol_color->SetNumberOfComponents(3);

    // Keep a reference to the basis ids for picking operations
    vtkNew<vtkIntArray> mol_basis_id;
    mol_basis_id->SetName("basis_id");
    mol_basis_id->SetNumberOfComponents(1);

    for (auto const& pair: sym_pos) {
        auto transformed = (deortho * pair.second.transpose()).transpose();
        for (long i=0; i<pair.second.rows(); ++i) {
            xt = transformed(i, 0);
            yt = transformed(i, 1);
            zt = transformed(i, 2);

            molecule->AppendAtom(pair.first->species()->number, xt, yt, zt);
            mol_radii->InsertNextTuple1(pair.first->radius()*radscale);
            mol_scalar->InsertNextTuple1(pair.first->species()->number);
            mol_basis_id->InsertNextTuple1(pair.first->id());
            mol_color->InsertNextTuple3(
                pair.first->species()->color()[0],
                pair.first->species()->color()[1],
                pair.first->species()->color()[2]
                );
        }
    }

    if (not doc.atomsVisible()) {
        mol_mapper->SetRenderAtoms(false);
    }

    molecule->GetAtomData()->AddArray(mol_color);
    mol_mapper->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_VERTICES, "color");
    molecule->GetVertexData()->AddArray(mol_radii);
    molecule->GetVertexData()->AddArray(mol_basis_id);
    mol_mapper->SetMapScalars(false);
    mol_mapper->SetAtomicRadiusArrayName("radii");
    mol_mapper->SetAtomicRadiusTypeToCustomArrayRadius();

    mol_mapper->SetInputData(molecule);
    vtkNew<ArcteActor> mol_actor;

    mol_actor->SetMapper(mol_mapper);
    mol_actor->kind = "atom";

    retn.push_back(mol_actor);

    // ------------------------- Bonds ----------------------------
    double dist;
    double dx, dy, dz;
    double x1, y1, z1, x2, y2, z2;
    size_t total_pos = 0;
    for (auto const& pair: sym_pos) {
        total_pos += pair.second.rows();
    }

    std::vector<std::pair<chem::Element const*, std::array<double, 3>>> data {total_pos};
    std::vector<double> radii{};
    radii.resize(total_pos);

    mol_mapper->SetBondRadius(doc.bondRadius());
    if (doc.bondStyle() == BondStyle::Dual) {
        mol_mapper->SetUseMultiCylindersForBonds(true);
        mol_mapper->SetBondColorModeToDiscreteByAtom();
    }
    else if (doc.bondStyle() == BondStyle::Solid) {
        mol_mapper->SetUseMultiCylindersForBonds(false);

        mol_mapper->SetBondColorModeToSingleColor();
        mol_mapper->SetBondColor(
                    doc.bondColor()[0]*255,
                    doc.bondColor()[1]*255,
                    doc.bondColor()[2]*255
                    );
    }

    unsigned short atom1_num;
    unsigned short atom2_num;
    std::vector<unsigned int> bond_ids_with_no_bonds {};
    if (doc.nBonds() > 0 and doc.bondsVisible()) {
        for (auto const& bond: doc.bonds()) {
            bool bond_found = false;
            for (int i=0; i<molecule->GetNumberOfAtoms(); ++i) {
                auto atom1 = molecule->GetAtom(i);
                atom1_num = atom1.GetAtomicNumber();

                // Skip if the atom's species is neither of the bond's species
                if ((atom1_num != bond->elem1()->number)) {continue;}

                for (int j=0; j<molecule->GetNumberOfAtoms(); ++j) {
                    auto atom2 = molecule->GetAtom(j);
                    atom2_num = atom2.GetAtomicNumber();

                    // If the bond is between 2 of the same species, avoid duplicate cylinders
                    if (atom1_num == atom2_num) {
                        if (j<=i) {continue;}
                    }
                    // Skip if the atom's species is neither of the bond's species
                    if ((atom2_num != bond->elem2()->number)) {continue;}

                    x1 = atom1.GetPosition()[0];
                    y1 = atom1.GetPosition()[1];
                    z1 = atom1.GetPosition()[2];
                    x2 = atom2.GetPosition()[0];
                    y2 = atom2.GetPosition()[1];
                    z2 = atom2.GetPosition()[2];
                    dx = x2 - x1;
                    dy = y2 - y1;
                    dz = z2 - z1;

                    dist = std::sqrt(dx*dx + dy*dy + dz*dz);
                    if (dist >= bond->minRadius() and dist <= bond->maxRadius()) {
                        molecule->AppendBond(atom1, atom2, 1);
                        if (bond_found == false) {
                            bond_found = true;
                        }
                    }
                }
            }
            // Remove the bond if not bond connections were found and the bond is flagged to be removed if
            // no connections were found
            if (not bond_found) {
                bond_ids_with_no_bonds.push_back(bond->id());
            }
        }
    }
    // Remove bonds that did not create bonded atoms, and if the bond was flagged for removal
    for (auto bond_id: bond_ids_with_no_bonds) {
        if (doc.findBondById(bond_id)->removeIfNoBonds()) {
            doc.removeBondById(bond_id);
        }
    }

    // ----------------------- Unit cell ---------------------------
    if (doc.unitCellVisible()) {
        for (unsigned int xn=1; xn<=doc.nx(); ++xn) {
            for (unsigned int yn=1; yn<=doc.ny(); ++yn) {
                for (unsigned int zn=1; zn<=doc.nz(); ++zn) {
                    std::array<unsigned int, 3> offset {xn-1, yn-1, zn-1};
                    auto cell = vtkfactory::unit_cell(doc, doc.unitCellLineWidth(), offset);
                    cell->kind = "unitcell";
                    retn.push_back(cell);
                }
            }
        }

    }

    return retn;
}

vtkSmartPointer<vtkCaptionActor2D> vtkfactory::caption_text(std::string text, std::array<double, 3> pos) {
    vtkNew<vtkCaptionActor2D> caption;
    caption->SetCaption(text.data());
    caption->SetAttachmentPoint(pos[0], pos[1], pos[2]);
    caption->GetCaptionTextProperty()->ItalicOff();
    caption->GetCaptionTextProperty()->SetColor(0, 0, 0);
    caption->GetCaptionTextProperty()->FrameOff();
    caption->GetTextActor()->SetTextScaleModeToNone();
    caption->GetCaptionTextProperty()->SetFontSize(20);
    caption->BorderOff();
    caption->LeaderOff();

    caption->GetCaptionTextProperty()->SetLineOffset(0);
    caption->GetCaptionTextProperty()->SetJustificationToCentered();

    return caption;
}

std::vector<actor_ptr> vtkfactory::stl_exportable_objects(Document const& doc) {
    /* Generate vtk actors suitable for export to 3d printable stl format */
    std::vector<vtkSmartPointer<ArcteActor>> retn;
    double radscale = doc.radiusScale();
    std::map<BasisPos const*, Eigen::MatrixXd> sym_pos = doc.symEquivPos(true, true);
    auto deortho = geom::deorthogonalization(doc);

    double x, y, z, xt, yt, zt;

    size_t npos = 0;
    for (auto const& pair: sym_pos) {
        npos += pair.second.rows();
    }

    std::vector<std::pair<chem::Element const*, std::array<double, 3>>> data{npos};

    // Create spheres for atoms
    for (auto const& pair: sym_pos) {
        vtkSmartPointer<vtkSphereSource> sphere = vtkfactory::sphere_source(0, 0, 0, pair.first->radius()*radscale, 60, 60);

        auto transformed = (deortho * pair.second.transpose()).transpose();
        for (long i=0; i<pair.second.rows(); ++i){
            xt = transformed(i, 0);
            yt = transformed(i, 1);
            zt = transformed(i, 2);

            data.push_back({pair.first->species(), {xt, yt, zt}});

            auto sphere = vtkfactory::sphere(
                         xt,
                         yt,
                         zt,
                         pair.first->radius()*radscale,
                         pair.first->species()->color(),
                         doc.atomPolygons(),
                         doc.atomPolygons());
            sphere->kind_id = pair.first->id();
            sphere->kind = "atom";
            retn.push_back(sphere);
        }
    }

    // Create cylinders for bonds
    double dist;
    double dx, dy, dz;
    double x1, y1, z1, x2, y2, z2;

    int nbonds {0};
    if (doc.nBonds() > 0) {
        for (size_t i=0; i<data.size(); ++i) {
            for (size_t j=i+1; j<data.size(); ++j) {
                for (auto const& bond: doc.bonds()) {

                    if (bond->elem1() == bond->elem2()) {
                        if (not (data.at(i).first == bond->elem1() and data.at(j).first == bond->elem2())) {
                            continue;
                        }
                    }
                    else {
                        if (not (data.at(i).first == bond->elem1() and data.at(j).first == bond->elem2()) and
                            not (data.at(i).first == bond->elem2() and data.at(j).first == bond->elem1())) {
                            continue;
                        }
                    }
                    x1 = data.at(i).second[0];
                    y1 = data.at(i).second[1];
                    z1 = data.at(i).second[2];
                    x2 = data.at(j).second[0];
                    y2 = data.at(j).second[1];
                    z2 = data.at(j).second[2];
                    dx = x2 - x1;
                    dy = y2 - y1;
                    dz = z2 - z1;

                    dist = std::sqrt(dx*dx + dy*dy + dz*dz);
                    if (dist >= bond->minRadius() and dist <= bond->maxRadius()) {
                        vtkSmartPointer<ArcteActor> cyl = vtkfactory::cylinder(
                                                              x1,
                                                              y1,
                                                              z1,
                                                              x2,
                                                              y2,
                                                              z2,
                                                              doc.bondRadius(),
                                                              doc.bondPolygons(),
                                                              doc.bondColor(),
                                                              true
                                                              );
                        cyl->kind = "bond";
                        cyl->kind_id = bond->id();
                        nbonds++;
                        retn.push_back(cyl);
                    }
                }
            }
        }
    }

    return retn;
}
