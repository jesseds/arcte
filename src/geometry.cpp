/************************************************************************
** This file is part of Arcte, a program for crystallographic
** visualization and analysis of CIF files
**
** Copyright (C) 2022 Jesse Smith - contact jesseds@protonmail.ch
**
** Arcte comes with ABSOLUTELY NO WARRANTY.
** This is free software, and you are welcome to redistribute it
** under certain conditions; see license for details.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
************************************************************************/

#include <geometry.h>
#include <cmath>
#include <iostream>
#include <array>
#include <ciso646>


using Eigen::Vector3d;

Matrix3d geom::metricTensor(double a, double b, double c,
                             double alpha, double beta, double gamma){
    double alpha_r = geom::deg2rad(alpha);
    double beta_r = geom::deg2rad(beta);
    double gamma_r = geom::deg2rad(gamma);
    Matrix3d retn {};
    retn << a*a,              a*b*cos(gamma_r), a*c*cos(beta_r),
            b*a*cos(gamma_r), b*b,              b*c*cos(alpha_r),
            c*a*cos(beta_r),  c*b*cos(alpha_r), c*c;
    return retn;
}


Matrix3d geom::deorthogonalization(double a, double b, double c,
                                   double alpha, double beta, double gamma){
    double alpha_r = geom::deg2rad(alpha);
    double beta_r = geom::deg2rad(beta);
    double gamma_r = geom::deg2rad(gamma);

    double vol = volume(a, b, c, alpha, beta, gamma);

    Matrix3d retn {};
    retn << a, b*cos(gamma_r), c*cos(beta_r),
            0, b*sin(gamma_r), c*(cos(alpha_r) - cos(beta_r) * cos(gamma_r))/sin(gamma_r),
            0, 0,              vol/(sin(gamma_r)*a*b);
    return retn;
}


Matrix3d geom::deorthogonalization(Document const& doc){
    return geom::deorthogonalization(doc.a(), doc.b(), doc.c(), doc.alpha(), doc.beta(), doc.gamma());
}


double geom::volume(double a, double b, double c, double alpha, double beta, double gamma){
    return pow((geom::metricTensor(a, b, c, alpha, beta, gamma).determinant()), 0.5);
}


Matrix3d geom::latticeVectors(double a, double b, double c, double alpha, double beta, double gamma) {
    auto deortho = geom::deorthogonalization(a, b, c, alpha, beta, gamma);
    Vector3d ai {};
    Vector3d bi {};
    Vector3d ci {};
    ai << 1, 0, 0;
    bi << 0, 1, 0;
    ci << 0, 0, 1;

    Vector3d aa = deortho * ai;
    Vector3d bb = deortho * bi;
    Vector3d cc = deortho * ci;
    double vol = geom::volume(a, b, c, alpha, beta, gamma);
    Vector3d ar = bb.cross(cc) / vol;
    Vector3d br = cc.cross(aa) / vol;
    Vector3d cr = aa.cross(bb) / vol;

    Matrix3d retn {};
    retn << ar, br, cr;

    return retn.transpose();
}


std::array<double, 6> geom::reciprocalParams(double a, double b, double c, double alpha, double beta, double gamma) {
    double alpha_r = geom::deg2rad(alpha);
    double beta_r = geom::deg2rad(beta);
    double gamma_r = geom::deg2rad(gamma);
    double vol = geom::volume(a, b, c ,alpha, beta, gamma);

    double ar = b*c*sin(alpha_r)/vol;
    double br = a*c*sin(beta_r)/vol;
    double cr = a*b*sin(gamma_r)/vol;

    // Round the operands slightly to avoid small precision errors that could cause asin(x) with x slightly greater than 1
    double alphar = asin(geom::roundFloor(vol/(a*b*c*sin(beta_r)*sin(gamma_r)), 15));
    double betar = asin(geom::roundFloor(vol/(a*b*c*sin(alpha_r)*sin(gamma_r)), 15));
    double gammar = asin(geom::roundFloor(vol/(a*b*c*sin(alpha_r)*sin(beta_r)), 15));
    return {ar, br, cr, rad2deg(alphar), rad2deg(betar), rad2deg(gammar)};
}


double geom::volume(Document const& doc) {
    return geom::volume(doc.a(), doc.b(), doc.c(), doc.alpha(), doc.beta(), doc.gamma());
}


bool geom::validCellAngles(double alpha, double beta, double gamma) {
    alpha = deg2rad(alpha);
    beta = deg2rad(beta);
    gamma = deg2rad(gamma);
    double P = 4 * sin((alpha + beta + gamma)/2) *
               sin((alpha + beta - gamma)/2) *
               sin((alpha + gamma - beta)/2) *
               sin((beta + gamma - alpha)/2);
    return P > 0;
}


MatrixXd geom::frac2cart(Eigen::MatrixXd const& coords, Eigen::Matrix3d const& deortho){
    auto retn = deortho * coords.transpose();
    return retn.transpose();
}


double geom::uvwDist(Matrix3d mtensor, double u, double v, double w) {
    Eigen::Vector3d uvw {};
    uvw << u, v, w;
    double retn = pow(uvw.transpose() * mtensor * uvw, 0.5);
    return retn;
}


double geom::hklDist(Matrix3d mtensor_r, double h, double k, double l) {
    Eigen::Vector3d hkl {};
    hkl << h, k, l;
    double retn = pow(hkl.transpose() * mtensor_r * hkl, 0.5);
    return retn;
}


double geom::vecAngle(Matrix3d deortho, double u1, double v1, double w1, double u2, double v2, double w2) {
    Eigen::Vector3d uvw1 {};
    uvw1 << u1, v1, w1;

    Eigen::Vector3d uvw2 {};
    uvw2 << u2, v2, w2;

    auto r1 = deortho * uvw1;
    auto r2 = deortho * uvw2;

    auto retn = (r1.transpose()*r2)/(r1.norm()*r2.norm());
    return rad2deg(acos(retn(0, 0)));
}


double geom::braggAngle(double wl, double dhkl, int n) {
    return rad2deg( asin(n*wl/2/dhkl) );
}


VectorXd geom::braggAngle(double wl, VectorXd const& dhkl, int n) {
    return 180/pi*(asin(n*wl/2/dhkl.array()));
}


double geom::braggWl(double angle, double dhkl, int n) {
    return 2*dhkl*sin(deg2rad(angle))/n;
}


double geom::braggDHkl(double wl, double angle, int n) {
    return wl*n/2/sin(deg2rad(angle));
}


double geom::relElectronWl(double volts) {
    return sqrt((h*h*c*c) / (ee*volts*(2*m0*c*c+ee*volts))) * 1e10;
}


double geom::relElectronGamma(double volts) {
    return 1 + (ee * volts)/(m0*c*c);
}


double geom::relElectronMass(double volts) {
    return relElectronGamma(volts) * m0;
}


void geom::applyInPlace(std::array<std::array<double, 4>, 4> const& seitz, double& x, double& y, double& z) {
    std::array<double, 3> ret;
    for (size_t i = 0; i != 3; ++i) {
        ret[i] = seitz[i][0]*x + seitz[i][1]*y + seitz[i][2]*z + seitz[i][3];
    }

    x = ret[0];
    y = ret[1];
    z = ret[2];
}


bool geom::isZero(double val, double epsilon) {
    return std::abs(val) <= epsilon;
}


std::array<int, 4> uvw2uvtw(int u, int v, int w) {
    int u2 = 1/3*(2*u-v);
    int v2 = 1/3*(2*v-u);
    int t = -(u2+v2);

    return std::array<int, 4> {u2, v2, t, w};
}


std::pair<MatrixXi, VectorXd> geom::hklsInShell (
        double a,
        double b,
        double c,
        double alpha,
        double beta,
        double gamma,
        double g_min,
        double g_max) {

    if (g_max <= g_min) {
        throw std::runtime_error("geom::hkls_in_shell(): Min g cannot be greater than max g");
    }
    auto rparams = geom::reciprocalParams(a, b, c, alpha, beta, gamma);
    double ra, rb, rc;
    double ralpha, rbeta, rgamma;

    ra = rparams.at(0);
    rb = rparams.at(1);
    rc = rparams.at(2);

    Matrix3d mtensor = geom::metricTensor(a, b, c, alpha, beta, gamma);
    Matrix3d r_mtensor = mtensor.inverse();
    MatrixXd deortho = geom::deorthogonalization(a, b, c, alpha, beta, gamma);
    Matrix3d basis = (deortho * Matrix3d::Identity()).transpose();
    Matrix3d r_basis = r_mtensor * basis;

    int nx_max = ceil(g_max/ra);
    int ny_max = ceil(g_max/rb);
    int nz_max = ceil(g_max/rc);

    // potentially large arrays so we put on the heap
    auto xn  = std::make_unique<std::vector<long>>();
    auto yn  = std::make_unique<std::vector<long>>();
    auto zn  = std::make_unique<std::vector<long>>();

    for (int i=nx_max*-1; i<=nx_max; ++i) {
        xn->push_back(i);
    }
    for (int i=ny_max*-1; i<=ny_max; ++i) {
        yn->push_back(i);
    }
    for (int i=nz_max*-1; i<=nz_max; ++i) {
        zn->push_back(i);
    }
    MatrixXi trial_hkls {xn->size() * yn->size() * zn->size(), 3};
    size_t i = 0;
    for (auto x: *xn) {
        for (auto y: *yn) {
            for (auto z: *zn) {
                trial_hkls(i, 0) = x;
                trial_hkls(i, 1) = y;
                trial_hkls(i, 2) = z;
                ++i;
            }
        }
    }

//    MatrixXd coords = (deortho * trial_hkls.cast<double>().transpose()).transpose();
    MatrixXd coords = (r_basis.transpose() * trial_hkls.cast<double>().transpose()).transpose();
    VectorXd norm = coords.rowwise().norm();

    Eigen::VectorXi pass_upper = (norm.array() < g_max).cast<int>();
    Eigen::VectorXi pass_lower = (norm.array() > g_min).cast<int>();
    Eigen::ArrayXi pass_all = ((pass_upper + pass_lower).array() == 2).cast<int>();

    std::vector<long> idx {};

    for (auto i=0; i<trial_hkls.rows(); ++i) {
        if (pass_all(i) == 1) {
            idx.push_back(i);
        }
    }

    MatrixXi hkls = trial_hkls(idx, Eigen::all);
    VectorXd g = norm(idx);

    return {hkls, g};
}


//double geom::laue_radius(int order, double g_zone, double wl, double Kt) {
//    return std::sqrt(Kt*Kt + 2*order*g_zone/wl);
//}


Vector3i geom::zoneAxis2(Vector3i const& vec1, Vector3i const& vec2) {
    long h1 = vec1(0);
    long k1 = vec1(1);
    long l1 = vec1(2);
    long h2 = vec2(0);
    long k2 = vec2(1);
    long l2 = vec2(2);

    int u = 0;
    int v = 0;
    int w = 0;
    double u_det = 0;
    double v_det = 0;
    double w_det = 0;

    MatrixXd u_mat {2, 2};
    u_mat << k1, l1,
             k2, l2;
    u_det = u_mat.determinant();
    if (geom::isZero(u_det))
        u = 0;
    else {
        u = round(u_det);
    }

    MatrixXd v_mat {2, 2};
    v_mat << l1, h1,
             l2, h2;
    v_det = v_mat.determinant();
    if (geom::isZero(v_det))
        v = 0;
    else {
        v = round(v_det);
    }

    MatrixXd w_mat {2, 2};
    w_mat << h1, k1,
             h2, k2;
    w_det = w_mat.determinant();
    if (geom::isZero(w_det))
        w = 0;
    else {
        w = round(w_det);
    }

    Vector3i retn {};
    retn << u, v, w;
    auto scale = geom::gcd(retn);

    if (scale != 0) {
        retn /= scale;
    }
    return retn;
}


bool geom::zoneAxis3(Vector3i const& vec1, Vector3i const& vec2, Vector3i const& vec3) {
    Matrix3d mat {};
    mat << vec1.cast<double>(), vec2.cast<double>(), vec3.cast<double>();
    double det = mat.transpose().determinant();
    if (geom::isZero(det)) {
        return true;
    }
    else {
        return false;
    }
}


VectorXd geom::excitationError(Vector3d const& wave_vector, MatrixXd const& hkls_g) {
    double K0 = wave_vector.norm();

    MatrixXd hkls_g_translated = hkls_g.rowwise() + wave_vector.transpose();
    VectorXd prods = (hkls_g_translated.array() * hkls_g_translated.array()).matrix().rowwise().sum();
    VectorXd retn = (K0 * K0 - prods.array())/(2 * K0);
    return retn;
}


VectorXcd geom::structureFactors(Document const& doc, Eigen::MatrixXi const& hkls, Eigen::VectorXd  const& g, sfs::Param scat_kind, double rel_corr) {
    if (hkls.rows() != g.rows()) {
        throw std::runtime_error("hkls and g must have same number of rows");
    }

    auto n_hkl = hkls.rows();

//    if (n_hkl == 0) {
//        throw std::runtime_error("Must have at least 1 hkl and 1 g");
//    }

    auto sfactors = sfs::factorsFromEnum(scat_kind);

    // All centered basis positions
    MatrixXd lattice {doc.nSymEquivPos(), 3};
    VectorXd occ {lattice.rows()};
    MatrixXd sfact {n_hkl, lattice.rows()};
    std::vector<BasisPos const*> bases {};

    Eigen::VectorXi Z {lattice.rows()};

    // Populate lattice positions
    size_t count = 0;
    auto sym_eq_pos = doc.symEquivPos();

    for (auto pair = sym_eq_pos.cbegin(); pair != sym_eq_pos.cend(); pair++) {
        auto basis = pair->first;
        auto pos = pair->second;
        for (auto row: pos.rowwise()) {
            bases.push_back(basis);

            lattice(count, 0) = row(0);
            lattice(count, 1) = row(1);
            lattice(count, 2) = row(2);
            occ(count) = basis->occupancy();
            Z(count) = basis->species()->number;
            ++count;
        }
    }

    #pragma omp parallel for collapse(2)
    for (auto i=0; i<n_hkl; ++i) {
        for (auto jj=0; jj<lattice.rows(); ++jj) {
            sfact(i, jj) = sfactors->value(bases[jj]->species(), g(i));
        }
    }

    std::complex<double> constexpr j {0, 2*geom::pi};
    Eigen::MatrixXcd summand = sfact.array() * ( j * ( hkls.cast<double>() * lattice.transpose())).array().exp();

    // Reduce to single value for each hkl
    summand.array().rowwise() *= occ.array().transpose();
    Eigen::VectorXcd Fhkl = summand.rowwise().sum();

    return Fhkl * rel_corr;
}

VectorXcd geom::crystalPotential(VectorXcd const& Fhkl, double volume) {
    static constexpr double constant = (h*h) / (2 * pi * m0) / ee * 1e20;
    return constant / volume * Fhkl;  // Volts
}

VectorXcd geom::opticalPotential(VectorXcd const& Fhkl, double volume) {
    return Fhkl / (pi*volume);  // Volts
}

VectorXd geom::extinctionDistance(VectorXcd const& Fhkl, double volume, double wl) {
    VectorXd norms = Fhkl.rowwise().norm().real();
//    VectorXd retn = (pi * volume) / (wl * norms.array());
    double constant = wl/(pi * volume);
    VectorXd retn = norms * constant;
    return retn.cwiseInverse();
}

